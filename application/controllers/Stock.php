<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct()
	{
		parent::__construct();
		//$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('stock_model');
		$this->load->model('settings_model');
		$this->load->model('reports_model');
		$this->load->library('upload');
		$this->lang->load('auth');
		$this->load->helper('language');
		$this->load->library('ion_auth');		
		$this->session->set_userdata('sel', 'user');
		if (!$this->ion_auth->logged_in())
			redirect('auth');
		//$this->session->set_userdata('sel', 'general');
		date_default_timezone_set('Asia/Dhaka');
		
	}
   function index()
   {
   	        $data['stocks']=$this->stock_model->allStock();
		
			$data['main_content']='stock/stock';
		
		    $this->load->view('includes/templates',$data);
   }
   function menufactureStock()
   {
   	         $data['menufactures']=$this->reports_model->getAllMenufacture();
	         $data['main_content']='stock/menufactureStock';
		
		    $this->load->view('includes/templates',$data);
	   
   }
   function ajax_menufactureStock()
   {
   	$post=$this->input->post();
	$data['stocks']=$this->stock_model->menufactureStock($post['menufacture_id']);
	 $this->load->view('stock/ajax_menufactureStock',$data);
   }
   function menufactureReorderLevel()
   {
   	        $data['menufactures']=$this->reports_model->getAllMenufacture();
	         $data['main_content']='stock/menufactureStockReorder';
		
		    $this->load->view('includes/templates',$data);
   }
   function ajax_menufactureStockReorder()
   {
   	    $post=$this->input->post();
	    $data['stocks']=$this->stock_model->menufactureStockReorder($post['menufacture_id']);
	    $this->load->view('stock/ajax_menufactureStockReorder',$data);
   }
   function expireDate()
   {
   	        $data['stocks']=$this->stock_model->expireDate();
			//var_dump($data['stocks']);die;
			$data['main_content']='stock/expireDate';
		
		    $this->load->view('includes/templates',$data);
   }
}