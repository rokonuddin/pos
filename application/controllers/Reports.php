<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		//$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('settings_model');
		$this->load->model('purchase_model');
		$this->load->model('reports_model');
		$this->load->library('upload');
		$this->lang->load('auth');
		$this->load->helper('language');	
		$this->load->library('ion_auth');	
		//$this->session->set_userdata('sel', 'user');
		if (!$this->ion_auth->logged_in())
			redirect('auth');
		//$this->session->set_userdata('sel', 'general');
		date_default_timezone_set('Asia/Dhaka');
		
	}
	function menufactureStatement()
	{
		    $data['menufactures']=$this->reports_model->getAllMenufacture();
			$data['main_content']='reports/menufactureStatement';
		
		    $this->load->view('includes/templates',$data);
	}
	function menufacturePurchaseReportForm()
	{
		    $data['menufactures']=$this->reports_model->getAllMenufacture();
			$data['main_content']='reports/menufactureReport';
		
		    $this->load->view('includes/templates',$data);
	}
	function menufacturePurchaseReport()
	{
			$post=$this->input->post();
		
		$data['reprots']=$this->reports_model->dateWisePurchaseStatement($post['menufacture_id'],$post['start_date'],$post['end_date']);
		
	     $this->load->view('reports/ajax_menufactureReports',$data);	
	}
	function dateRangePurchaseReports()
	{
		
		$data['main_content']='reports/dateRangePurchaseReports';
		
		    $this->load->view('includes/templates',$data);
	
	}
	function ajax_dateRangePurchaseReports()
	{
		$post=$this->input->post();
		$data['menufactures']=$this->reports_model->getAllMenufacture();
		foreach($data['menufactures'] as $menufactures)
		{
			$data['reprots'][$menufactures['menufacture_id']]=$this->reports_model->dateWisePurchaseStatement($menufactures['menufacture_id'],$post['start_date'],$post['end_date']);
		}
      $this->load->view('reports/ajax_dateRangePurchaseReports',$data);	
	}
	function inquery()
	{
		$post=$this->input->post();
		
		$data['status']=$this->reports_model->getDueAmountMenufacture($post['menufacture_id'],$post['start_date']);
		
		$data['advance']=$this->reports_model->getAdvanceAmountMenufacture($post['menufacture_id']);
		
		$data['purchases']=$this->reports_model->dateWisePurchaseStatement($post['menufacture_id'],$post['start_date'],$post['end_date']);
		$i=0;
		foreach($data['purchases'] as $date)
		{
			$data['cash'][$i]=$this->reports_model->getdateWiseAmountMenufacture($date['invoice_date'],$date['purchase_id']);
		   $i++;
		}
		$data['start']=$post['start_date'];
		//var_dump($data);die;
		 $this->load->view('reports/ajax_statement',$data);
	}
	function customerStatement()
	{
		    $data['customers']=$this->reports_model->getCustomer();
			$data['main_content']='reports/customerStatement';
		
		    $this->load->view('includes/templates',$data);
	}
	function inquery_customer()
	{
		$post=$this->input->post();
		$data['status']=$this->reports_model->getDueAmount($post['customer_id'],$post['start_date']);
		
		$data['advance']=$this->reports_model->getAdvanceAmount($post['customer_id']);
		$sales=$this->reports_model->dateWiseSalesStatement($post['customer_id'],$post['start_date'],$post['end_date']);
		$data['sales']=$sales;
		$i=0;
		foreach($sales as $date)
		{
			$data['cash'][$i]=$this->reports_model->getdateWiseAmount($date['invoice_date'],$date['sales_id']);
		   $i++;
		}
		
		$data['start']=$post['start_date'];
	    $this->load->view('reports/ajax_statement_sales',$data);
    }
   function monthlySummary()
   {
   	   
			$data['main_content']='reports/monthlySummary';
		
		    $this->load->view('includes/templates',$data);
   }
   function monthlySummaryReports()
   {
   	  $post=$this->input->post();
	  $first=date('Y').'-'.$post['month'].'-01';
	  $last=date('Y-m-t',strtotime($first));
	  $data['reports']=$this->reports_model->getSummaryReports($first,$last);
	  
	  $this->load->view('reports/ajax_summaryReports',$data);
   }
   function monthlyStatement()
   {
   	      $data['main_content']='reports/monthlyStatement';
		
		    $this->load->view('includes/templates',$data);
   }
   function monthlyStatementReports()
   {
   	   $post=$this->input->post();
	  $first=date('Y').'-'.$post['month'].'-01';
	  $last=date('Y-m-t',strtotime($first));
	  $data['reports']=$this->reports_model->getStatementReports($first,$last);
	 // var_dump($data['reports']);die;
	 $data['first']=$first;
	 $data['last']=$last;
	  $this->load->view('reports/ajax_statementReports',$data);
   }
   function dailyReportsSummary()
   {
   	   $data['main_content']='reports/dailyReportsSummary';
		
		    $this->load->view('includes/templates',$data);
   }
   function inquery_profit()
   {
   	    $post=$this->input->post();
   	    $data['end']=$post['end_date'];
   	    $data['start']=$post['start_date'];
		
		
		for($i=$post['start_date'];$i<=$post['end_date'];)
		{
			
			
			$values=$this->reports_model->getTransection($i);
			$total_receive=0;$total_profit=0;$total_expanse=0;
			foreach($values as $value)
			{
				
				if($value['transction_type']=='sales')
				{
					$total_receive=$total_receive+$value['amount'];
					//var_dump($value['amount'],$value['date']);
					$split=explode(",",$value['Description']);
					foreach($split as $sp)
					{
						if($sp!=''){
						$va=explode(":",$sp);
						//var_dump($va);
						$p_s=$this->returnProfit($va);
						//var_dump($p_s);
						$total_profit=$total_profit+$p_s;
						
						}
						
					}
					
				}else{
					$total_expanse=$total_expanse+$value['amount'];
				}
				
				
				
			}
			
			
			$data['receive'][$i][0]=$total_receive;
			$data['profit'][$i][1]=$total_profit;
			$data['expanse'][$i][2]=$total_expanse;
			//var_dump($data);die;
			$i=date("Y-m-d", strtotime("+1 day",strtotime($i)));
		}
		
		//var_dump($data);die;
	    $this->load->view('reports/ajax_profit',$data);
   }
   function returnProfit($data)
   {
   	    $produts=$this->reports_model->getByIdproduct($data['0']);
		$invoice=$this->reports_model->getByIdInvoice($data['0']);
		$profit=0;
		$purchase_price=0;$sales_price=0;
		
		foreach($produts as $produt)
		{
			$purchase_price=$purchase_price+$produt['purchase_price']*$produt['quantity'];
			$sales_price=$sales_price+$produt['total_price'];
		}
		//var_dump($invoice['discount']);
		$sales_price=$sales_price-$invoice['discount'];
		
		$is_profit=$sales_price-$purchase_price;
		//var_dump($is_profit,$data[1],$sales_price);
		$porfit= ($is_profit*$data[1])/$sales_price;
		 
		return sprintf ("%.2f", $porfit);
		
		
   }
}