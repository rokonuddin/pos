<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		//$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('settings_model');
		$this->load->model('purchase_model');
		$this->load->model('sales_model');
		$this->load->model('account_model');
		$this->load->library('upload');
		$this->lang->load('auth');
		$this->load->helper('language');	
		$this->load->library('ion_auth');	
		//$this->session->set_userdata('sel', 'user');
		if (!$this->ion_auth->logged_in())
			redirect('auth');
		//$this->session->set_userdata('sel', 'general');
		date_default_timezone_set('Asia/Dhaka');
		
	}
	
	function index()
	{
		   $data['accounts']=$this->account_model->allAccount();
		   $data['main_content']='account/account';
		
		   $this->load->view('includes/templates',$data);
	}
	function ajaxdeleteAccount()
	{
		$this->account_model->deleteAccount($this->input->post('account_id'));
	 	$data['accounts']=$this->account_model->allAccount();
		$this->load->view('account/ajaxView',$data);
	}
	function ajaxsaveAccount()
	{
		$post=$this->input->post();
		$data_array=array('AccountName'=>$post['name'],'AccountsType'=>$post['account_type']);
		//var_dump($post['account_id']);die;
		if($post['account_id']==0)
		{
			
		  $this->account_model->saveAccount($data_array);
		}else{
			$this->account_model->updateAccount($data_array,$post['account_id']);
		}
		
		$data['accounts']=$this->account_model->allAccount();
		$this->load->view('account/ajaxView',$data);
	}
	
    function expanse()
	{
		    $data['froms']=$this->account_model->getFromAccount();
			$data['tos']=$this->account_model->getToAccount();
		   $data['expances']=$this->account_model->allexpances('expanse');
		   //var_dump($data['expances']);die;
		   $data['main_content']='account/expanse';
		
		   $this->load->view('includes/templates',$data);
	}
	function ajaxsaveExpanseAccount()
	{
		$user = $this->ion_auth->user()->row();
   		$user_id = $user->id;
		 $post=$this->input->post();
		 
		  $amount=array(
			'amount'=>$this->input->post('amount'),
			'date'=>date('Y-m-d'),
			'PaymentType'=>'CREDIT',
			'payment_type'=>'expanse',
			
			'createdby'=>$user_id,
			'ToAccount'=>$post['to'],
			'FromAccount'=>$post['from'],
			'created_date'=>date('Y-m-d H:i:s'),
			'refernce_type'=>0,
			'Description'=>$post['description'],
			'transction_type'=>'none'
			);
			
		
			
		if($post['amount_tranjection_id']==0)
		{
			
		  $this->purchase_model->amount_disburse($amount);
		}else{
			$this->account_model->updateExpanse($amount,$post['amount_tranjection_id']);
		}
		
		 $data['expances']=$this->account_model->allexpances('expanse');
		$this->load->view('account/ajaxViewExpance',$data);
			
	}
    function ajaxdeleteExpanseAccount()
	{
		$this->account_model->deleteExpanse($this->input->post('amount_tranjection_id'));
		$data['expances']=$this->account_model->allexpances('expanse');
		$this->load->view('account/ajaxViewExpance',$data);
	}
	function income()
	{
		   $data['tos']=$this->account_model->getFromAccount();
			$data['froms']=$this->account_model->getToAccount1();
		   $data['expances']=$this->account_model->allexpances('income');
		   //var_dump($data['expances']);die;
		   $data['main_content']='account/income';
		
		   $this->load->view('includes/templates',$data);
	}
	function ajaxsaveIncomeAccount()
	{
		$user = $this->ion_auth->user()->row();
   		$user_id = $user->id;
		 $post=$this->input->post();
		 
		  $amount=array(
			'amount'=>$this->input->post('amount'),
			'date'=>date('Y-m-d'),
			'PaymentType'=>'DEBIT',
			'payment_type'=>'income',
			
			'createdby'=>$user_id,
			'ToAccount'=>$post['to'],
			'FromAccount'=>$post['from'],
			'created_date'=>date('Y-m-d H:i:s'),
			'refernce_type'=>0,
			'Description'=>$post['description'],
			'transction_type'=>'none'
			);
			
		
			
		if($post['amount_tranjection_id']==0)
		{
			
		  $this->purchase_model->amount_disburse($amount);
		}else{
			$this->account_model->updateExpanse($amount,$post['amount_tranjection_id']);
		}
		
		 $data['expances']=$this->account_model->allexpances('income');
		$this->load->view('account/ajaxIncome',$data);
			
	}
    function ajaxdeleteIncomeAccount()
	{
		$this->account_model->deleteExpanse($this->input->post('amount_tranjection_id'));
		$data['expances']=$this->account_model->allexpances('income');
		$this->load->view('account/ajaxIncome',$data);
	}
}