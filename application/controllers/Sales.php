<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		//$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('settings_model');
		$this->load->model('sales_model');
		$this->load->model('purchase_model');
		$this->load->library('upload');
		$this->lang->load('auth');
		$this->load->helper('language');	
		$this->load->library('ion_auth');
		//$this->lang->load('auth');
		//$this->load->helper('language');		
		//$this->session->set_userdata('sel', 'user');
		//if (!$this->ion_auth->logged_in())
		//	redirect('auth');
		//$this->session->set_userdata('sel', 'general');
		date_default_timezone_set('Asia/Dhaka');
		
	}
	function index()
	{
		$user = $this->ion_auth->user()->row();
			$data['main_content']='sales/sales';
			$data['invoice_number']=$this->purchase_model->getInvoiceNumber($user->client_code);
		    $this->load->view('includes/templates',$data);
	}
	function searchproduct(){
		$post=$this->input->get();
		$data['products']=$this->sales_model->searchProduct($post['q']);

		$products=array();
		foreach($data['products'] as $product){
			
			$products[]=$product['product_name'].':'.$product['product_code'].':'.$product['pack_size'].':'.$product['quantity'].':'.$product['sell_price'].':'.$product['catagory_name'].':'.$product['menufacture_name'].':'.$product['product_id'];
		}
		
		
		echo json_encode($products);
	}
	function items()
	{
		//$post=$this->input->post();
		$data['items']=$this->sales_model->getAllitemsByCategoryId($this->input->post('catagory_id'));
		
		 $this->load->view('sales/items',$data);
	}
	function addproduct()
	{
		$data['price']=array();
		$data['price']['count']=$this->input->post('count');
		$data['price']['product_id']=$this->input->post('items_id');
		$data['price']['product_name']=$this->input->post('product_name');
		$data['price']['store_unit']=$this->input->post('store_unit');
		$data['price']['pack_size']=$this->input->post('pack_size');
		$data['price']['exp_date']=$this->input->post('exp_date');
		$data['price']['unit']=$this->input->post('unit');
		$data['price']['sale_price']=$this->input->post('sale_price');
		$data['price']['product_amount']=$this->input->post('product_amount');
		
		 $this->load->view('sales/addSales',$data);
	}
	function check_product()
	{
		$post=$this->input->post();
		$check=$this->sales_model->check_product($post['product_id']);
		if($check['quantity']>=$post['unit'])
		{
			echo 0;
		}else{
			if(!empty($check['quantity']))
			{
				echo "Available Quentity ".$check['quantity'].' !';
			}else{
				echo "Available Quentity 0".' !';
			}
			
		}
	}


    function onlyPaid()
	{
		 $data['customers']=$this->sales_model->getCustomer();
		 $data['main_content']='sales/only_paid';
		 $this->load->view('includes/templates',$data);
	}
	function cusotmerPaid()
	{
		
		$user = $this->ion_auth->user()->row();
   		$user_id = $user->id;
		$post=$this->input->post();
		                    $dues=$this->sales_model->getTotalDue($post['customer_id']);
			                    $customer_total=$post['amount'];
								
			
			$customer=$this->sales_model->getCustomerId($post['customer_id']);
			$data['customers']=$customer;
			$customer_total=$customer_total+$customer['advance'];
					$dis='';			
				$data['due']=$this->sales_model->getDueAmount($post['customer_id']);			
								
									if(!empty($dues))
									{
										
										foreach($dues as $due_amount)
										{
											
											if($customer_total>=$due_amount['due_amount']){
												$update_due=array('status'=>'paid','due_amount'=>'0','amount'=>$due_amount['due_amount']+$due_amount['amount']);
												$this->sales_model->updateDue($update_due,$due_amount['sales_id']);
												$dis=$dis.$due_amount['sales_id'].':'.$due_amount['due_amount'].',';
												$customer_total=$customer_total-$due_amount['due_amount'];
												
											}elseif($customer_total<$due_amount['due_amount']){
												$dis=$dis.$due_amount['sales_id'].':'.$customer_total.',';
												$update_due=array('due_amount'=>$due_amount['due_amount']-$customer_total,'amount'=>$customer_total+$due_amount['amount']);
												$this->sales_model->updateDue($update_due,$due_amount['sales_id']);
												$totals=0;
												$customer_total=0;
												break;
											}
											
										}
										
										if($customer_total > 0)
										{
											
											
										    $customer_update=array('advance'=>$customer_total);
										    $this->sales_model->updateCustomer($customer_update,$post['customer_id']);
											
										}
										
									}else
									{
										
										
										
										if($customer_total > 0)
										{
											$customer_update=array('advance'=>$customer_total+$customer['advance']);
										   $this->sales_model->updateCustomer($customer_update,$post['customer_id']);
										}
										
									
									
									
									
								
									}
           if($post['amount'] > 0)
			{
			  $amount=array(
								'amount'=>$post['amount'],
								'date'=>date('Y-m-d'),
								'payment_type'=>'debit',
								'createdby'=>$user_id,
								'created_date'=>date('Y-m-d H:i:s'),
								'refernce_type'=>$post['customer_id'],
								'transction_type'=>'customer',
								'Description'=>$dis,
								);
								
				$this->sales_model->amount_receive($amount);
			}
			
			redirect('sales/printCustomerView/'.$post['customer_id']);
			
             
                    
	}
    function printCustomerView($id)
	{
		$data['customer']=$this->sales_model->getcustomerPaidAmount($id);
        $this->load->view('sales/customerPrint',$data);
	}
	function save_invoice()
	{
		
		$user = $this->ion_auth->user()->row();
   		$user_id = $user->id;
       
		$incoice_datas = json_decode($this->input->post('orders'), true);
		$post=$this->input->post();		
			
		if($post['customer_id']!=0)
		{
			$customer_id=$post['customer_id'];
		}else{
			
			$menu=array(
			'customer_name'=>$post['customer_name'],
			'address'=>$post['address'],
			'client_code'=>$user->client_code,
			'phone'=>$post['phone'],
			);
			$customer_id=$this->settings_model->ajaxsaveCustomer($menu);
		}	
		
		 if($post['invoice_id']=='0'){
	    	$invoice_number=$this->purchase_model->getInvoiceNumber($user->client_code);
	    }else{
	    	$invoice_number=$post['invoice_number'];
	    }
		
		
		
		$invoice = array(
							'ref_id'=>$customer_id,
							'invoice_number'=>$invoice_number,
							'total_amount'=>$this->input->post('subtotal'),
							'discount'=>$this->input->post('discount'),
							'invoice_date'=>$this->input->post('receive_date'),
							'grand_total'=>$this->input->post('invoicetotal'),
							'discount'=>(empty($this->input->post('discount')))?0:$this->input->post('discount'),
							'transport_cost'=>0,
							'refund_amount'=>$this->input->post('refund_amount'),
							'other_cost'=>0,
							'invoice_date'=>date('Y-m-d'),
							'invoice_type'=>'2',
							'client_code'=>$user->client_code,
							'created_by'=>$user_id,
							'created_date'=>date('Y-m-d H:i:s'),
							'paid_amount'=>$post['cash_pay'],
							'due'=>($this->input->post('invoicetotal')>$post['cash_pay'])?($this->input->post('invoicetotal')-$post['cash_pay']):0
						);
		 $id=$this->purchase_model->save_invoice($invoice);
		
		
		
		if($post['cash_pay']!=null || $post['cash_pay']!='')
			{
				$amount=array(
				'amount'=>$this->input->post('cash_pay'),
				'date'=>date('Y-m-d'),
				'payment_type'=>'2',
				'PaymentType'=>'DEBIT',
				'client_code'=>$user->client_code,
				'Description'=>'Sales amount',
				'createdby'=>$user_id,
				'ToAccount'=>3,
				'FromAccount'=>1,
				'created_date'=>date('Y-m-d H:i:s'),
				'refernce_type'=>'sales',
				'refernce_id'=>$id,
				'customer_id'=>$customer_id,
				'transction_type'=>'sales'
				);
					
								
				$this->sales_model->amount_receive($amount);
			}
		
		foreach($incoice_datas as $incoice_data)
		{
			$tranjection=array(
			'product_id'=>$incoice_data['itemid'],
			'quantity'=>$incoice_data['unit'],
			'unit_price'=>$incoice_data['sale_price'],
			'total_price'=>$incoice_data['product_amount'],
			'client_code'=>$user->client_code,
			'invoice_id'=>$id,
			'tranjection_type'=>'2',
			'date'=>date('Y-m-d'),
			'createdby'=>$user_id,
			'created_date'=>date('Y-m-d H:i:s'),
			);
			$this->purchase_model->save_purchase_tranjection($tranjection);
			
			$product=$this->purchase_model->getInvoiceProduct($incoice_data['itemid'],$user->client_code);
			$productUpdate=array(
				'quantity'=>$product['quantity']-$incoice_data['unit'],
				);
				$this->purchase_model->update_inventory($productUpdate,$product['product_id'],$user->client_code);
		}
		    
	     
      echo $id;
	}
    function salesPrint($id)
   {
   	   $data['invoiceInfo']=$this->sales_model->invoiceInfo($id);
	
   	   $this->load->view('sales/customerPrint',$data);
   }
	function salesHistory()
   {
   	   $data['main_content']='sales/sales_history';
		
       $this->load->view('includes/templates',$data);
   }
   function search()
   {
   	   $post=$this->input->post();
	   $data['status']=$post['status'];
	   if($post['status']=='DUE')
	   {
	   	   $data['searchs']=$this->sales_model->searchsDue($post['date']);
		   
	   }elseif($post['status']=='PAID'){
	   	   $data['searchs']=$this->sales_model->searchsPaid($post['date']);
		  
	   }else{
	   	  $data['searchs']=$this->sales_model->searchs($post['date']);
	   }
	   
	  
	   $this->load->view('sales/ajax_sales_history',$data);
   }
   function search_product()
   {
   	     $post=$this->input->post();
		 $data['searchs']=$this->sales_model->searchs_sales($post['invoice_number']);
	  
	     $this->load->view('sales/ajax_sales_product',$data);
   }
   function pay_amount()
   {
   	    $user = $this->ion_auth->user()->row();
   		$user_id = $user->id;
   	    $post=$this->input->post();
		$sales=$this->sales_model->getById($post['sales_id']);
		
		if($sales['due_amount']==$this->input->post('amount'))
		{
			$sales_array=array(
			'due_amount'=>0,
			'status'=>'paid',
			'amount'=>($sales['amount']+$this->input->post('amount'))
			);
		}else{
			$sales_array=array(
			'due_amount'=>($sales['due_amount']-$this->input->post('amount')),
			'status'=>'due',
			'amount'=>($sales['amount']+$this->input->post('amount'))
			);
		}
		$this->sales_model->updateSales($post['sales_id'],$sales_array);
		if(!empty($post['amount']))
		{
			
			$amount=array(
			'amount'=>$this->input->post('amount'),
			'date'=>date('Y-m-d'),
			'payment_type'=>'debit',
			'createdby'=>$user_id,
			'created_date'=>date('Y-m-d H:i:s'),
			'refernce_type'=>$post['sales_id'],
			'transction_type'=>'sales'
			);
			
			$this->sales_model->amount_receive($amount);
			
		}
		echo 0;
   }
   function search_customer()
	{
		$post=$this->input->post();
		$user = $this->ion_auth->user()->row();
		
			$customers=$this->sales_model->search_customer_phone($post['search_phone']);
		
		
		
		echo json_encode($customers);
		
	}
	
   function search_category()
	{
		$post=$this->input->post();
		
		$data['catagorys']=$this->sales_model->search_category($post['category']);
		$this->load->view('sales/ajax_search_category',$data);
		
	}
	
   function search_item()
	{
		$post=$this->input->post();
		
		$data['items']=$this->sales_model->search_item($post['item'],$post['catagoryid']);
		$this->load->view('sales/ajax_search_item',$data);
		
	}
}