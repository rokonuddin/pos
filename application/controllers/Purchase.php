<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase extends CI_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		//$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('settings_model');
		$this->load->model('purchase_model');
		$this->load->model('sales_model');
		$this->load->model('reports_model');
		$this->load->library('upload');
		$this->lang->load('auth');
		$this->load->helper('language');	
		$this->load->library('ion_auth');	
		//$this->session->set_userdata('sel', 'user');
		if (!$this->ion_auth->logged_in())
			redirect('auth');
		//$this->session->set_userdata('sel', 'general');
		date_default_timezone_set('Asia/Dhaka');
		
	}
	function index()
	{
			$user = $this->ion_auth->user()->row();
		   $data['suppliers']=$this->purchase_model->getSupplier($user->client_code);

		   $data['invoice_number']=$this->purchase_model->getInvoiceNumber($user->client_code);
			$data['main_content']='purchase/purchase';
		
		    $this->load->view('includes/templates',$data);
	}
	function search_category_menufacture()
	{
		$post=$this->input->post();
		
		$data['catagorys']=$this->purchase_model->search_category($post['menufacture_id']);
		$this->load->view('sales/ajax_search_category',$data);
	}
   function purchaseDuePaid()
   {
   	 $data['menufactures']=$this->reports_model->getAllMenufacture();
   	  $data['main_content']='purchase/only_paid';
	  $this->load->view('includes/templates',$data);
   }
   function menufacturePaid()
   {
   	$user = $this->ion_auth->user()->row();
    $user_id = $user->id;
	$post=$this->input->post();
   	  $menufacture_id=$post['menufacture_id'];
	  $menufacture=$this->purchase_model->getMenufactureId($menufacture_id);
	   $pri=$menufacture['total_due'];
	   $adv=$menufacture['advance'];
	  $menufacture_total=$post['amount']+$menufacture['advance'];
	  $dues=$this->purchase_model->getTotalDue($menufacture_id);
			                    $due=0;
								$dis='';
									if(!empty($dues))
									{
										
										
										
										
										foreach($dues as $due_amount)
										{
											
											if($menufacture_total>=$due_amount['due_amount']){
												$update_due=array('pay_status'=>'paid','due_amount'=>'0','pay_amount'=>$due_amount['due_amount']+$due_amount['pay_amount']);
												$this->purchase_model->updateDue($update_due,$due_amount['purchase_id']);
												$dis=$dis.$due_amount['purchase_id'].':'.$due_amount['due_amount'].',';
												$menufacture_total=$menufacture_total-$due_amount['due_amount'];
												
												$menufacture_update=array('total_due'=>$menufacture['total_due']-$due_amount['due_amount'],'advance'=>0);
						                         $this->purchase_model->updateMenufacture($menufacture_update,$menufacture_id);
												$due=$due+$due_amount['due_amount'];
												
											}elseif($menufacture_total<$due_amount['due_amount']){
												
												$update_due=array('due_amount'=>$due_amount['due_amount']-$menufacture_total,'pay_amount'=>$menufacture_total+$due_amount['pay_amount']);
												$this->purchase_model->updateDue($update_due,$due_amount['purchase_id']);
												$dis=$dis.$due_amount['purchase_id'].':'.$menufacture_total.',';
												$menufacture_update=array('total_due'=>$menufacture['total_due']-$menufacture_total,'advance'=>0);
												
						                        $this->purchase_model->updateMenufacture($menufacture_update,$menufacture_id);
												$due=$due+$due_amount['due_amount']-$menufacture_total;
												$totals=0;
												$menufacture_total=0;
												
												
												
												break;
											}
											
										}
										
										if($menufacture_total > 0)
										{
											
											
										    $menufacture_update=array('advance'=>$menufacture_total,'total_due'=>0);
										    $this->purchase_model->updateMenufacture($menufacture_update,$menufacture_id);
											
										}
										
									}else
									{
										
										
										
										
										$menufacture_update=array('advance'=>$menufacture_total+$menufacture['advance'],'total_due'=>0);
										$this->purchase_model->updateMenufacture($menufacture_update,$menufacture_id);
									
									    
									
									
								
									}
									if(!empty($post['amount']))
									{
										
										$amount=array(
										'amount'=>$this->input->post('amount'),
										'date'=>date('Y-m-d'),
										'PaymentType'=>'CREDIT',
										
										'Description'=>$dis,
										'createdby'=>$user_id,
										'ToAccount'=>2,
										'FromAccount'=>3,
										'created_date'=>date('Y-m-d H:i:s'),
										'refernce_type'=>$menufacture_id,
										'transction_type'=>'menufacture'
										);
										
										$this->purchase_model->amount_disburse($amount);
										
									}
									
									
									
							
			$menufacture1=$this->purchase_model->getMenufactureId($menufacture_id);
			
           //$this->printMenufacture($menufacture['advance'],$data['menufacture'],$due,$post['amount'],date('Y-m-d'));
		   
		   redirect('purchase/printMenufacture/'.$adv.'/'.$menufacture1['name'].'/'.$menufacture1['address'].'/'.$menufacture1['phone'].'/'.$menufacture1['advance'].'/'.$menufacture1['total_due'].'/'.$pri.'/'.$post['amount']);
									
   }
   function printMenufacture($advance,$name,$address,$phone,$advance1,$total_due,$due,$amount)
   {
   	         $data['advance']=$advance;				
			$data['name']=$name;
			$data['address']=$address;
			$data['phone']=$phone;
			$data['advance1']=$advance1;
			$data['total_due']=$total_due;
			
			$data['due']=$due;
			$data['paid']=$amount;
			$data['date']=date('Y-m-d');
   	    $this->load->view('purchase/menufacturePrint',$data);
   }
   
   function ajax_menufactureDue()
   {
   	  $post=$this->input->post();
	  $due=$this->purchase_model->getDueAmount($post['menufacture_id']);
	  
	  echo $due['total_due'];
   }
	function items()
	{
		//$post=$this->input->post();
		$data['items']=$this->sales_model->getAllitemsByCategoryId($this->input->post('catagory_id'));
		 $this->load->view('purchase/items',$data);
	}
	function searchproduct(){
		$post=$this->input->get();
		$data['products']=$this->purchase_model->searchProduct($post['q']);

		$products=array();
		foreach($data['products'] as $product){
			
			$products[]=$product['product_name'].':'.$product['product_code'].':'.$product['pack_size'].':'.$product['purchase_price'].':'.$product['sell_price'].':'.$product['catagory_name'].':'.$product['menufacture_name'].':'.$product['product_id'];
		}
		
		
		echo json_encode($products);
	}
	function addproduct()
	{
		$data['price']=array();
		$data['price']['count']=$this->input->post('count');
		$data['price']['product_id']=$this->input->post('items_id');
		$data['price']['product_name']=$this->input->post('product_name');
		$data['price']['product_code']=$this->input->post('product_code');
		$data['price']['pack_size']=$this->input->post('pack_size');
		$data['price']['exp_date']=$this->input->post('exp_date');
		$data['price']['unit']=$this->input->post('unit');
		$data['price']['purchase_price']=$this->input->post('purchase_price');
		$data['price']['product_amount']=$this->input->post('product_amount');
		$this->load->view('purchase/addPurchase',$data);
	}
    function search_phone()
	{
		$post=$this->input->post();
		
		$data['menufactures']=$this->purchase_model->search_mennfacture($post['search_phone']);
		
		$this->load->view('purchase/ajax_search_menufacture',$data);
		
	}
	function save_invoice()
	{
		$user = $this->ion_auth->user()->row();
   		$user_id = $user->id;
        $year=date('Y');
        $month=date('m');
        
		$post=$this->input->post();
		
	    if($post['invoice_id']=='0'){
	    	$invoice_number=$this->purchase_model->getInvoiceNumber($user->client_code);
	    }else{
	    	$invoice_number=$post['invoice_number'];
	    }
		
		
		$invoice = array(
							'ref_id'=>$post['supplier_id'],
							'invoice_number'=>$invoice_number,
							'total_amount'=>$this->input->post('subtotal'),
							'discount'=>$this->input->post('discount'),
							'invoice_date'=>$this->input->post('receive_date'),
							'grand_total'=>$this->input->post('invoicetotal'),
							'discount'=>(empty($this->input->post('discount')))?0:$this->input->post('discount'),
							'transport_cost'=>(empty($this->input->post('transport_cost')))?0:$this->input->post('transport_cost'),
							'other_cost'=>(empty($this->input->post('other_cost')))?0:$this->input->post('other_cost'),
							'invoice_date'=>date('Y-m-d'),
							'invoice_type'=>'1',
							'client_code'=>$user->client_code,
							'created_by'=>$user_id,
							'created_date'=>date('Y-m-d H:i:s'),
							'paid_amount'=>$post['cash_pay'],
							'due'=>($this->input->post('invoicetotal')>$post['cash_pay'])?($this->input->post('invoicetotal')-$post['cash_pay']):0
						);
		 $id=$this->purchase_model->save_invoice($invoice);
		
		
		
		if(!empty($post['cash_pay']))
		{
			
			$amount=array(
			'amount'=>$this->input->post('cash_pay'),
			'date'=>date('Y-m-d'),
			'payment_type'=>'1',
			'PaymentType'=>'CREDIT',
			'client_code'=>$user->client_code,
			'Description'=>'Purchase amount',
			'createdby'=>$user_id,
			'ToAccount'=>2,
			'FromAccount'=>3,
			'created_date'=>date('Y-m-d H:i:s'),
			'refernce_type'=>'purchase',
			'refernce_id'=>$id,
			'supplier_id'=>$post['supplier_id'],
			'transction_type'=>'purchase'
			);
			
			$this->purchase_model->amount_disburse($amount);
			
		}
		
		$incoice_datas = json_decode($this->input->post('orders'), true);
		
		foreach($incoice_datas as $incoice_data)
		{
			
			$tranjection=array(
			'product_id'=>$incoice_data['itemid'],
			'quantity'=>$incoice_data['unit'],
			'unit_price'=>$incoice_data['unit_price'],
			'total_price'=>$incoice_data['product_amount'],
			'expire_date'=>$incoice_data['exp_date'],
			'client_code'=>$user->client_code,
			'invoice_id'=>$id,
			'tranjection_type'=>'1',
			'date'=>date('Y-m-d'),
			'createdby'=>$user_id,
			'created_date'=>date('Y-m-d H:i:s'),
			);
			$this->purchase_model->save_purchase_tranjection($tranjection);
			
			$product=$this->purchase_model->getInvoiceProduct($incoice_data['itemid'],$user->client_code);
			if(!empty($product))
			{
				$productUpdate=array(
				'client_code'=>$user->client_code,
				'quantity'=>$product['quantity']+$incoice_data['unit'],
				);
				$this->purchase_model->update_inventory($productUpdate,$product['product_id'],$user->client_code);
			}else{
				$product=array(
				'product_id'=>$incoice_data['itemid'],
				'quantity'=>$incoice_data['unit'],
				'client_code'=>$user->client_code
				);
				$this->purchase_model->save_inventory($product);
			}
		}
      
	 echo $id;
	}
   function purchasePrint($id)
   {
   	   $data['invoiceInfo']=$this->purchase_model->invoiceInfo($id);
	   
	   //var_dump($data['invoiceInfo']);die;
   	   $this->load->view('purchase/purchaseInvoicePrint',$data);
   }
   function purchaseHistory()
   {
   	   
   	   $data['main_content']='purchase/purchase_history';
		
       $this->load->view('includes/templates',$data);
   }
   function search()
   {
   	   $post=$this->input->post();
	   $data['status']=$post['status'];
	   if($post['status']=='DUE')
	   {
	   	   $data['searchs']=$this->purchase_model->searchsDue($post['date']);
		   
	   }elseif($post['status']=='PAID'){
	   	   $data['searchs']=$this->purchase_model->searchsPaid($post['date']);
		  
	   }else{
	   	  $data['searchs']=$this->purchase_model->searchs($post['date']);
	   }
	   
	  
	   $this->load->view('purchase/ajax_purchase_history',$data);
   }
   function search_product()
   {
   	     $post=$this->input->post();
		 
		 $data['searchs']=$this->purchase_model->searchs_product($post['invoice_number']);
	  
	     $this->load->view('purchase/ajax_purchase_product',$data);
   }
   function pay_amount()
   {
   	   $user = $this->ion_auth->user()->row();
   		$user_id = $user->id;
   	    $post=$this->input->post();
		$purchase=$this->purchase_model->getById($post['purchase_id']);
		
		if($purchase['due_amount']==$this->input->post('amount'))
		{
			$purchase_array=array(
			'due_amount'=>0,
			'pay_status'=>'cash',
			'pay_amount'=>($purchase['pay_amount']+$this->input->post('amount'))
			);
		}else{
			$purchase_array=array(
			'due_amount'=>($purchase['due_amount']-$this->input->post('amount')),
			'pay_status'=>'due',
			'pay_amount'=>($purchase['pay_amount']+$this->input->post('amount'))
			);
		}
		$this->purchase_model->updatePurchase($post['purchase_id'],$purchase_array);
		if(!empty($post['amount']))
		{
			
			$amount=array(
			'amount'=>$this->input->post('amount'),
			'date'=>date('Y-m-d'),
			'payment_type'=>'credit',
			'createdby'=>$user_id,
			'created_date'=>date('Y-m-d H:i:s'),
			'refernce_type'=>$post['purchase_id'],
			'transction_type'=>'purchase'
			);
			
			$this->sales_model->amount_receive($amount);
			
		}
		echo 0;
   }
	
}