<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct()
	{
		parent::__construct();
		//$this->load->library('ion_auth');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('settings_model');
		$this->load->model('purchase_model');
		//$this->load->model('attendance_model');
		$this->load->library('upload');
		
		$this->lang->load('auth');
		$this->load->helper('language');	
		$this->load->library('ion_auth');	
		//$this->session->set_userdata('sel', 'user');
		if (!$this->ion_auth->logged_in())
			redirect('auth');
		//$this->load->helper('language');		
		//$this->session->set_userdata('sel', 'user');
		//if (!$this->ion_auth->logged_in())
		//	redirect('auth');
		//$this->session->set_userdata('sel', 'general');
		date_default_timezone_set('Asia/Dhaka');
		
	}
	function csv(){
		$this->load->library('csvimport');
		$data=$this->csvimport->get_array('FINAL ALL PRODUCT-1 - Sheet2.csv');
		foreach ($data as $value) {
			if(!empty($value['Product Name'])){
				$result=array(
				'product_name'=>$value['Product Name'],
				'manufacture'=>$value['Company Name'],
				'category'=>$value['Category'],
				'purchase_price'=>$value['Purchase Price'],
				'sell_price'=>$value['Sale Price'],
				're_order_level'=>$value['Reorder Label'],
				);
				$this->settings_model->saveAll('product',$result);
			}
			
		}
	}
	function edit_user(){
        $data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name'  => $this->input->post('last_name'),
					'email'  => $this->input->post('email'),
					'client_code' => $this->input->post('config_id'),
					'phone'      => $this->input->post('phone'),
				);
        $this->settings_model->updateAll('users','id',$this->input->post('id'),$data);

        redirect('settings/userList');
	}
	function query()
	{
	 	$customers=$this->settings_model->getAllCustomer();
		$i=1;
		foreach($customers as $customer)
		{
			$i++;
			$sales=$this->settings_model->getCustomerSales($customer['customer_id']);
			$paid=$this->settings_model->getCustomerpaid($customer['customer_id']);
			$due_paid=$this->settings_model->getCustomerDuepaid($customer['customer_id']);
			
			
			if($sales['sales']>($paid['sales']+$due_paid['sales']))
			{
				$menu=array(
					'due_total'=>$sales['sales']-($paid['sales']+$due_paid['sales']),
					'advance'=>0
					);
		      
		      $this->settings_model->ajaxUpdateCustomer($menu,$customer['customer_id']);
				
			}else{
				$menu=array(
					'advance'=>($paid['sales']+$due_paid['sales'])-$sales['sales'],
					'due_total'=>0,
					);
		     
		      $this->settings_model->ajaxUpdateCustomer($menu,$customer['customer_id']);
			}
			
		}
		

	 }
	 function queryPurchase()
	 {
	 	$menufactures=$this->settings_model->getManufacture();
		$i=1;
		foreach($menufactures as $menufacture)
		{
			$i++;
			$sales=$this->settings_model->getmenufacture_idSales($menufacture['menufacture_id']);
			$paid=$this->settings_model->getmenufacture_idpaid($menufacture['menufacture_id']);
			$due_paid=$this->settings_model->getmenufacture_idDuepaid($menufacture['menufacture_id']);
			
			
			if($sales['purchase']>($paid['purchase']+$due_paid['purchase']))
			{
				$menu=array(
					'total_due'=>$sales['purchase']-($paid['purchase']+$due_paid['purchase']),
					'advance'=>0
					);
		      
		     $this->settings_model->updateMenufacture($menu,$menufacture['menufacture_id']);
				
			}else{
				$menu=array(
					'advance'=>($paid['purchase']+$due_paid['purchase'])-$sales['purchase'],
					'total_due'=>0,
					);
		     
		      $this->settings_model->updateMenufacture($menu,$menufacture['menufacture_id']);
			}
			
		}
	 }
	 function config($id=0)
	 {
	 	if($id==0)
		{
			$data['main_content']='config/config';
		
		    $this->load->view('includes/templates',$data);
		}else{
			
			$data['configList']=$this->settings_model->getById($id);
			$data['main_content']='config/config';
		
		    $this->load->view('includes/templates',$data);
		}
	 	
	 }
	 function saveConfig($id)
	 {
	 	$this -> load -> library('upload');
	 	$post=$this->input->post();
		//var_dump($_FILES['company_logo']['name']);
		//var_dump($post);
		$data=array(
		'client_code' => $post['client_code'],
		'company_name' => $post['company_name'],
		'short_form' => $post['shot_form'],
		'branch_name' => $post['branch_name'],
		'branch_location' => $post['branch_location'],
		'currency_name' => $post['currency_name'],
		'address_line_1' => $post['address_line_1'],
		'address_line_2' => $post['address_line_2'],
		'phone' => $post['phone'],
		'vat_rag_no' => $post['vat_rag_no'],
		'tax_rate' => $post['tax_rate'],
		);
		
		
		
		if (!empty($_FILES['company_logo']['name'])) {
				
				$config['upload_path'] = './companyLogo/';

				$config['allowed_types'] = 'JPG|jpg|png|gif|bmp';

				//$config['max_size'] = '10000000000000000';

				$this -> upload -> initialize($config);
                 
				if ($this -> upload -> do_upload('company_logo')) {

					$image_data = $this -> upload -> data();
                  
					
				}
				
				
					$data['image'] = $image_data['file_name'];
						
				  unset($config);
				  
					$upload_data = $this->upload->data();
					$raw_name = $upload_data['raw_name'];
					$ext = $upload_data['file_ext'];
					/* for 85x85 px */
					$config['image_library'] = 'gd2';
					$config['source_image'] = $upload_data['full_path'];
					$config['quality'] = '70%';
					$config['master_dim'] = 'auto';
					$config['maintain_ratio'] = TRUE;
					$config['new_image'] = './companyLogo/15x15/'.$raw_name.$ext;
					$config['width']     = 10;
					$config['height']    = 10;
					$this->load->library('image_lib');
					$this->image_lib->initialize($config);
					if (!$this->image_lib->resize()) {
						echo $this->image_lib->display_errors();
					}		
					unset($config);
					
					$config['image_library'] = 'gd2';
					$config['source_image'] = $upload_data['full_path'];
					$config['quality'] = '70%';
					$config['master_dim'] = 'auto';
					$config['maintain_ratio'] = TRUE;
					$config['new_image'] = './companyLogo/200x200/'.$raw_name.$ext;
					$config['width']     = 200;
					$config['height']    = 200;
					$this->load->library('image_lib');
					$this->image_lib->initialize($config);
					if (!$this->image_lib->resize()) {
						echo $this->image_lib->display_errors();
					}
            
			
				} 
		if($id==0){
			$this -> settings_model -> saveConfig($data);
		}else{
			
			   $con=$this->settings_model->getById($id);
			unlink('./companyLogo/'.$con['image']);
			unlink('./companyLogo/15x15/'.$con['image']);
			unlink('./companyLogo/200x200/'.$con['image']);
			$this -> settings_model -> updateConfig($data,$id);
		}
		
		redirect('settings/configList');
	 }
	 
	 function configList()
	 {
	 	$data['configLists']=$this->settings_model->getAllData('config');
		
		$data['main_content']='config/configList';
		
		$this->load->view('includes/templates',$data);
		
	 }
	 function configview($id)
	 {
	 	
		$data['configList']=$this->settings_model->getById($id);
			$data['main_content']='config/configView';
		
		    $this->load->view('includes/templates',$data);
	 	
	 }
	 function configDelete($id)
	 {
	 	$this->settings_model->deleteConfig($id);
	 	
		redirect('settings/configList');
	 	
	 }
	 
	 function manufacture($id)
	 {
			$data['menufactures']=$this->settings_model->getManufacture();
			$data['main_content']='menufacture/menufacture';
		    $this->load->view('includes/templates',$data);
		
	 }
	 
	 function supplier($id)
	 {
	 		$user = $this->ion_auth->user()->row();
			$data['number']=$this->settings_model->getsuppliernumber($user->client_code);

			$data['suppliers']=$this->settings_model->getSupplier($user->client_code);
			$data['main_content']='supplier/supplier';
		    $this->load->view('includes/templates',$data);
		
	 }
	 function getNumber(){
	 	$user = $this->ion_auth->user()->row();
		echo $this->settings_model->getsuppliernumber($user->client_code);
	 }
	 function ajaxsavesupplier()
	 {
	 	$user = $this->ion_auth->user()->row();
	 	$post=$this->input->post();
	 	$supplier=array(
		'supplier_name'=>$post['name'],
		'supplier_no'=>$post['number'],
		'phone'=>$post['phone'],
		'address'=>$post['address'],
		'client_code'=>$user->client_code
		);
		if($post['id']=='0'){
			$this->settings_model->saveAll('supplier',$supplier);
		}else{
			$this->settings_model->updateAll('supplier','id',$post['id'],$supplier);
		}
		
		
		$data['suppliers']=$this->settings_model->getSupplier($user->client_code);
		$this->load->view('supplier/ajaxView',$data);
	 }
	 function catagory()
	 {
			$data['catagorys']=$this->settings_model->getcatagory();
			$data['main_content']='catagory/catagory';
		    $this->load->view('includes/templates',$data);
		
	 }
	 function ajaxsave()
	 {
	 	
	 	$post=$this->input->post();
		
		$menu=array(
		'name'=>$post['name'],
		'contact_person'=>$post['contact_person'],
		'phone'=>$post['phone'],
		'description'=>$post['description'],
		'address'=>$post['address']
		);
		$this->settings_model->saveMenufacture($menu);
		$data['menufactures']=$this->settings_model->getManufacture();
		$this->load->view('menufacture/ajaxView',$data);
	 }
	 function ajaxedit()
	 {
	 	$post=$this->input->post();
		
		$menu=array(
		'name'=>$post['name'],
		'contact_person'=>$post['contact_person'],
		'phone'=>$post['phone'],
		'description'=>$post['description'],
		'address'=>$post['address']
		);
		$this->settings_model->updateMenufacture($menu,$post['menufacture_id']);
		$data['menufactures']=$this->settings_model->getManufacture();
		$this->load->view('menufacture/ajaxView',$data);
	 }
	
	 
	 function ajaxdelete()
	 {
	 	$this->settings_model->deletemenufacture($this->input->post('menufacture_id'));
	 	$data['menufactures']=$this->settings_model->getManufacture();
		$this->load->view('menufacture/ajaxView',$data);
	 }
	 
	 function ajaxsaveCatagory()
	 {
	 	$post=$this->input->post();
		
		$menu=array(
		'catagory_name'=>$post['name'],
		
		'description'=>$post['description'],
		
		);
		$this->settings_model->ajaxsaveCatagory($menu);
		$data['catagorys']=$this->settings_model->getcatagory();
		$this->load->view('catagory/ajaxView',$data);
	 }
	 
	 function ajaxeditcatagory()
	 {
	 	$post=$this->input->post();
		
		$menu=array(
		'catagory_name'=>$post['name'],
		
		'description'=>$post['description'],
		
		);
		$this->settings_model->ajaxupdateCatagory($menu,$post['catagory_id']);
		$data['catagorys']=$this->settings_model->getcatagory();
		$this->load->view('catagory/ajaxView',$data);
	 }
	 
	 function ajaxdeleteCatagory()
	 {
	 	$this->settings_model->deleteCatagory($this->input->post('catagory_id'));
	 	$data['catagorys']=$this->settings_model->getcatagory();
		$this->load->view('catagory/ajaxView',$data);
	 }
	 
	 function ajaxeditCustomer()
	 {
	 	
		
		$post=$this->input->post();
		$user = $this->ion_auth->user()->row();
		
		$menu=array(
		'customer_code'=>$post['customer_code'],
		'client_code'=>$user->client_code,
		'customer_name'=>$post['customer_name'],
		'address'=>$post['address'],
		'phone'=>$post['phone'],
		'balance'=>$post['balance'],
		);
		
		$id=$this->settings_model->ajaxUpdateCustomer($menu,$post['customer_id']);
		//var_dump($id);die;
		$data['customers']=$this->settings_model->getAllCustomer();
		$this->load->view('customer/ajaxView',$data);
		
		
	 }
	 
	 
	 
	public function userList()
	{
		
		$data['main_content']='auth/userList';
		
		$this->load->view('includes/templates',$data);
	}
	
	public function getUser()
	{
		$data['users']=$this->settings_model->getAllUsers();
		echo json_encode($data['users']);
	}
    function customer()
	{
		$user = $this->ion_auth->user()->row();
		$data['number']=$this->settings_model->getlastCustomerNumber($user->client_code);

		$data['customers']=$this->settings_model->getAllCustomer($user->client_code);
		$data['main_content']='customer/customer';
		
		$this->load->view('includes/templates',$data);
	}
	function ajaxsaveCustomer()
	{
		$post=$this->input->post();

		//$number=$this->settings_model->getlastCustomerNumber();
		$user = $this->ion_auth->user()->row();
		
		
		$menu=array(
		'client_code'=>$user->client_code,
		'customer_code'=>$post['customer_code'],
		'customer_name'=>$post['customer_name'],
		'address'=>$post['address'],
		'phone'=>$post['phone'],
		'balance'=>$post['balance'],
		);
		
		$id=$this->settings_model->ajaxsaveCustomer($menu);
		
		$data['number']=$this->settings_model->getlastCustomerNumber($user->client_code);

		$data['customers']=$this->settings_model->getAllCustomer($user->client_code);
		$this->load->view('customer/ajaxView',$data);
	}
	function ajaxdeleteCustomer()
	{
		$this->settings_model->deleteCustomer($this->input->post('customer_id'));
	 	$data['customers']=$this->settings_model->getAllCustomer();
		$this->load->view('customer/ajaxView',$data);
	}
	function product()
	{
		$user = $this->ion_auth->user()->row();
		$data['menufactures']=$this->settings_model->getManufacture();
		$data['catagorys']=$this->settings_model->getcatagory();
		$data['products']=$this->settings_model->getAllproduct();
		$data['number']=$this->settings_model->getlastProductNumber();
		$data['main_content']='product/product';
		
		$this->load->view('includes/templates',$data);
	}
	function ajaxsaveproduct()
	{
		$post=$this->input->post();
		$number=$this->settings_model->getlastProductNumber();
		var_dump($number);die;
		$menu=array(
		'catagory_id'=>$post['catagory_id'],
		'manufacture_id'=>$post['manufacture_id'],
		'pack_size'=>$post['pack_size'],
		're_order_level'=>$post['re_order_level'],
		'purchase_price'=>$post['purchase_price'],
		'product_name'=>$post['product_name'],
		'product_code'=>$number+1,
		'due_sale_price'=>$post['due_sale_price'],
		'sell_price'=>$post['sell_price'],
		'whole_sale_price'=>$post['whole_sale_price'],
		);
		
		$id=$this->settings_model->ajaxsaveproduct($menu);
		//var_dump($id);die;
		$data['products']=$this->settings_model->getAllproduct();
		$this->load->view('product/ajaxView',$data);
	}
	function ajaxeditProduct()
	{
		$post=$this->input->post();
		
		$menu=array(
		'catagory_id'=>$post['catagory_id'],
		'manufacture_id'=>$post['manufacture_id'],
		'pack_size'=>$post['pack_size'],
		're_order_level'=>$post['re_order_level'],
		'purchase_price'=>$post['purchase_price'],
		'product_name'=>$post['product_name'],
		'product_code'=>$post['product_code'],
		'due_sale_price'=>$post['due_sale_price'],
		'sell_price'=>$post['sell_price'],
		'whole_sale_price'=>$post['whole_sale_price'],
		);
		
		$this->settings_model->ajaxupdateProduct($menu,$post['product_id']);
		//var_dump($id);die;
		$data['products']=$this->settings_model->getAllproduct();
		$this->load->view('product/ajaxView',$data);
	}
	function ajaxdeleteProduct()
	{
		$this->settings_model->deleteProduct($this->input->post('product_id'));
	 	$data['products']=$this->settings_model->getAllproduct();
		$this->load->view('product/ajaxView',$data);
	}
	function productOption()
	{
		$data['products']=$this->settings_model->getAllproduct();
		$data['productOptions']=$this->settings_model->getAllproductOption();
		$data['main_content']='product/productOption';
		
		$this->load->view('includes/templates',$data);
	}
    function ajaxsaveproductOption()
	{
		$post=$this->input->post();
		$user = $this->ion_auth->user()->row();
   		$user_id = $user->id;
		
			$product=$this->purchase_model->getInvoiceProduct($post['product_id']);
			if(!empty($product))
			{
				$productUpdate=array(
				
				'quantity'=>$product['quantity']+$post['quantity'],
				'update_by'=>$user_id,
			    'update_date'=>date('Y-m-d H:i:s'),
				);
				$this->purchase_model->update_inventory($productUpdate,$product['product_id']);
			}else{
				$product=array(
				'product_id'=>$post['product_id'],
				'quantity'=>$post['quantity'],
				'insert_by'=>$user_id,
			    'insert_date'=>date('Y-m-d H:i:s'),
				);
				$this->purchase_model->save_inventory($product);
			}
		   $tranjection=array(
			'product_id'=>$post['product_id'],
			'quantity'=>$post['quantity'],
			'status'=>'openingbalance',
			'date'=>date('Y-m-d'),
			'createdby'=>$user_id,
			'created_date'=>date('Y-m-d H:i:s'),
			);
			$this->purchase_model->save_purchase_tranjection($tranjection);
		
		
		
		
		$menu=array(
		'product_id'=>$post['product_id'],
		'quantity'=>$post['quantity'],
		'expire_date'=>date('Y-m-d',strtotime($post['expire_date'])),
		
		);
		
		$this->settings_model->ajaxsaveProductOption($menu);
		$data['productOptions']=$this->settings_model->getAllproductOption();
		$data['products']=$this->settings_model->getAllproduct();
		$this->load->view('product/ajaxViewOption',$data);
	}
	function ajaxeditProductOption()
	{
		
		$post=$this->input->post();
		
		
		$post=$this->input->post();
		$user = $this->ion_auth->user()->row();
   		$user_id = $user->id;
		$product_option=$this->settings_model->getooption($this->input->post('product_option_id'));
		
			$product=$this->purchase_model->getInvoiceProduct($post['product_id']);
			$total=$product['quantity']+$post['quantity']-$product_option['quantity'];
				$productUpdate=array(
				
				'quantity'=>$total,
				'update_by'=>$user_id,
			    'update_date'=>date('Y-m-d H:i:s'),
				);
				$this->purchase_model->update_inventory($productUpdate,$product['product_id']);
		
		
		$menu=array(
		'product_id'=>$post['product_id'],
		'quantity'=>$post['quantity'],
		'expire_date'=>date('Y-m-d',strtotime($post['expire_date'])),
		
		);
		
		$this->settings_model->ajaxupdateProductOption($menu,$this->input->post('product_option_id'));
		$data['productOptions']=$this->settings_model->getAllproductOption();
		$data['products']=$this->settings_model->getAllproduct();
		$this->load->view('product/ajaxViewOption',$data);
		
	}
	function ajaxdeleteProductOption()
	{
		$post=$this->input->post();
		$user = $this->ion_auth->user()->row();
		$user_id = $user->id;
		$product_option=$this->settings_model->getooption($this->input->post('product_option_id'));
		
			$product=$this->purchase_model->getInvoiceProduct($product_option['product_id']);
			$total=$product['quantity']-$product_option['quantity'];
				$productUpdate=array(
				
				'quantity'=>$total,
				'update_by'=>$user_id,
			    'update_date'=>date('Y-m-d H:i:s'),
				);
				$this->purchase_model->update_inventory($productUpdate,$product['product_id']);
		
		
		$this->settings_model->deleteOption($this->input->post('product_option_id'));
		$data['productOptions']=$this->settings_model->getAllproductOption();
		$data['products']=$this->settings_model->getAllproduct();
		$this->load->view('product/ajaxViewOption',$data);
	}
	function autoproduct_number()
	{
		 $number=$this->settings_model->getlastProductNumber();
		 echo $number['product_code']+1;
		
	}
	
	function role()
	{
		$data['roles']=$this->settings_model->getAllRole();
		
		$data['main_content']='role/role';
		
		$this->load->view('includes/templates',$data);
	}
	function ajaxsaveRole()
	{
		$post=$this->input->post();
		
		$data_array=array('name'=>$post['role'],'description'=>$post['role']);
	
		if($post['id']=='')
		{
			$this->settings_model->saveRole($data_array);
		}else{
			$this->settings_model->updateRole($data_array,$post['id']);
		}
		$data['roles']=$this->settings_model->getAllRole();
		$this->load->view('role/ajaxRoleView',$data);
	}
	function ajaxDeleteRole()
	{
		
		$this->settings_model->deleteRole($this->input->post('id'));
		$data['roles']=$this->settings_model->getAllRole();
		$this->load->view('role/ajaxRoleView',$data);
	}


    function menu()
	{
		$data['menus']=$this->settings_model->getAllMenu();
		$data['menu1s']=$this->settings_model->getAllMenuOnly();
		$data['main_content']='role/menu';
		
		$this->load->view('includes/templates',$data);
	}
    function ajaxsaveMenu()
	{
		$post=$this->input->post();
		
			$data_array=array('menu_name'=>$post['menu_name'],'menu_order'=>$post['menu_order'],'menu_url'=>$post['menu_url'],'parent_id'=>$post['parent_id']);
			//var_dump($data_array);die;
			if($post['menu_id_edit']=='')
			{
				$this->settings_model->saveMenu($data_array);
				
			}else{
				$this->settings_model->updateMenu($data_array,$post['menu_id_edit']);
			}
			
		$data['menus']=$this->settings_model->getAllMenu();
		$this->load->view('role/ajaxMenuView',$data);
		
		
	}
    function ajaxDeleteMenu()
	{
		$this->settings_model->deleteMenu($this->input->post('menu_id'));
		$data['menus']=$this->settings_model->getAllMenu();
		$this->load->view('role/ajaxMenuView',$data);
	}
    function assignRole()
	{
		$data['assignLists']=$this->settings_model->getAssignList();
		$data['roles']=$this->settings_model->getAllRole();
		$data['menu1s']=$this->settings_model->getAllMenuOnly();
		$data['main_content']='role/assignRole';
		
		$this->load->view('includes/templates',$data);
	}
	function ajaxSaveRoleAssign()
	{
		$post=$this->input->post();
		
		$data_array=array('menu_id'=>$post['menu_id'],'role_id'=>$post['role_id']);
			
			if($post['check']=='1')
			{
				$this->settings_model->SaveRoleAssign($data_array);
				
			}else{
				$this->settings_model->deleteAssignMenu($post['menu_id'],$post['role_id']);
			}
	}
	function supplyList()
	{
		
		$data['main_content']='supply/supplyList';
		
		$this->load->view('includes/templates',$data);
	}
	function supply()
	{
		
		$data['main_content']='supply/supply';
		
		$this->load->view('includes/templates',$data);
	}
}
