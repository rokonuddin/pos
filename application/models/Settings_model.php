<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Settings_model extends  CI_Model {
	
	function saveConfig($data)
	{
		$this->db->insert('config',$data);
		
		return $this->db->insert_id();
		
	}
	function saveAll($table,$data){
		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}
	function updateAll($table,$con,$id,$data)
	{
	   return	$this->db->where($con,$id)->update($table,$data);
	}
	function getCustomerSales($id)
	{
		return $this->db->select('sum(amount) as sales')->where('PaymentType','CREDIT')->where('ToAccount','3')->where('FromAccount','1')->where('transction_type','Sale')->where('refernce_id',$id)->get('amount_tranjection')->row_array();
	}
	function getCustomerpaid($id)
	{
		return $this->db->select('sum(amount) as sales')->where('PaymentType','DEBIT')->where('ToAccount','3')->where('FromAccount','1')->where('transction_type','CashPayment')->where('refernce_id',$id)->get('amount_tranjection')->row_array();
	}
	function getCustomerDuepaid($id)
	{
		return $this->db->select('sum(amount) as sales')->where('PaymentType','DEBIT')->where('ToAccount','1')->where('FromAccount','3')->where('transction_type','SaleDue')->where('refernce_id',$id)->get('amount_tranjection')->row_array();
	}
    function getsuppliernumber($code){

    	$number= $this->db->select_sum('id')->where('client_code',$code)->order_by('id','desc')->get('supplier')->row_array();

    	if($number['id']==null){
    		return $code.'001';
    	}else{
    		if($number['id']<10){
    			return $code.'00'.($number['id']+1);
    		}else if($number['id']>=10 && $number['id']<100){
    			return $code.'0'.($number['id']+1);
    		}else{
    			return $code.($number['id']+1);
    		}
    		
    	}
    }

     function getAllData($table){
     	return $this->db->get($table)->result_array();
     }

    function getmenufacture_idSales($id)
	{
		return $this->db->select('sum(amount) as purchase')->where('PaymentType','DEBIT')->where('ToAccount','2')->where('FromAccount','3')->where('transction_type','Purchase')->where('refernce_id',$id)->get('amount_tranjection')->row_array();
	}
	function getmenufacture_idpaid($id)
	{
		return $this->db->select('sum(amount) as purchase')->where('PaymentType','CREDIT')->where('ToAccount','2')->where('FromAccount','3')->where('transction_type','CashPayment')->where('refernce_id',$id)->get('amount_tranjection')->row_array();
	}
	function getmenufacture_idDuepaid($id)
	{
		return $this->db->select('sum(amount) as purchase')->where('PaymentType','CREDIT')->where('ToAccount','3')->where('FromAccount','1')->where('transction_type','PurchaseDue')->where('refernce_id',$id)->get('amount_tranjection')->row_array();
	}


    public function getSupplier($code){
    	return $this->db->where('client_code',$code)->get('supplier')->result_array();
    }


	function getAll()
	{
		return $this->db->get('config')->result_array();
	}
	function getooption($id)
	{
		return $this->db->where('product_option_id',$id)->get('product_option')->row_array();
	}
	function getconfig()
	{
		return $this->db->get('config')->row_array();
	}
	function getById($id)
	{
		return $this->db->where('config_id',$id)->get('config')->row_array();
	}
	function updateConfig($data,$id)
	{
	   return	$this->db->where('config_id',$id)->update('config',$data);
	}
	function ajaxUpdateCustomer($data,$id)
	{
		return	$this->db->where('customer_id',$id)->update('customer',$data);
	}
	function deleteConfig($id)
	{
		$this->db->where('config_id',$id)->delete('config');
	}
	function getManufacture()
	{
		return $this->db->get('menufacture')->result_array();
	}
	function saveMenufacture($data)
	{
		$this->db->insert('menufacture',$data);
		
		return $this->db->insert_id();
	}
	function updateMenufacture($data,$id)
	{
		return	$this->db->where('menufacture_id',$id)->update('menufacture',$data);
	}
	function deletemenufacture($id)
	{
		$this->db->where('menufacture_id',$id)->delete('menufacture');
	}
	function ajaxsaveCatagory($data)
	{
		
		$this->db->insert('catagory',$data);
		
		return $this->db->insert_id();
		
	}
	function getcatagory()
	{
		
		return $this->db->get('catagory')->result_array();
		
	}
	function ajaxupdateCatagory($data,$id)
	{
		return	$this->db->where('catagory_id',$id)->update('catagory',$data);
	}
	function deleteCatagory($id)
	{
		$this->db->where('catagory_id',$id)->delete('catagory');
	}
	function getAllUsers()
	{
		return $this->db->get('users')->result_array();
	}
	function getAllCustomer($code)
	{
		return $this->db->where('client_code',$code)->get('customer')->result_array(); 
	}
	function ajaxsaveCustomer($data)
	{
		
		$this->db->insert('customer',$data);
		
		return $this->db->insert_id();
	}
	function deleteCustomer($id)
	{
		$this->db->where('customer_id',$id)->delete('customer');
	}
    function getAllproduct()
	{
		return $this->db->order_by('product_id','desc')->get('product')->result_array();
	}
	function ajaxsaveproduct($data)
	{
		$this->db->insert('product',$data);
		
		return $this->db->insert_id();
	}
	function ajaxupdateProduct($data,$id)
	{
		return	$this->db->where('product_id',$id)->update('product',$data);
	}
	function deleteProduct($id)
	{
		$this->db->where('product_id',$id)->delete('product');
	}
	function getAllproductOption()
	{
		return $this->db->select('product.*,product_option.*')->join('product','product.product_id=product_option.product_id')->get('product_option')->result_array();
	}
	function ajaxsaveProductOption($data)
	{
		$this->db->insert('product_option',$data);
		
		return $this->db->insert_id();
	}
	function ajaxupdateProductOption($data,$id)
	{
		return	$this->db->where('product_option_id',$id)->update('product_option',$data);
	}
	function deleteOption($id)
	{
		$this->db->where('product_option_id',$id)->delete('product_option');
	}
	function getlastCustomerNumber($code)
	{
		 $data= $this->db->select_max('customer_code')->where('client_code',$code)->get('customer')->row_array();
		 if($data['customer_code']==null){
		 	return $code.'00001';
		 }else if($data['customer_code']<10){
		 	return $code.'0000'.($data['customer_code']+1);
		 }else if($data['customer_code']>=10 && $data['customer_code']<100){
		 	return $code.'000'.($data['customer_code']+1);
		 }else if($data['customer_code']>=100 && $data['customer_code']<1000){
		 	return $code.'00'.($data['customer_code']+1);
		 }else if($data['customer_code']>=1000 && $data['customer_code']<10000){
			return $code.'0'.($data['customer_code']+1);
		 }else{
		 	return $code.($data['customer_code']+1);
		 }
	}
	function getlastProductNumber()
	{
		 $number= $this->db->count_all_results('product');
		 if($number<10){
		 	$number='00000'.($number+1);
		 }else if($number>=10 && $number<100){
		 	$number='0000'.($number+1);

		 }else if($number>=100 && $number<1000){
		 	$number='000'.($number+1);
		 }else if($number>=1000 && $number<10000){
		 	$number='00'.($number+1);
		 }else if($number>=10000 && $number<100000){
			$number='0'.($number+1);
		 }else{
		 	$number=($number+1);
		 }
		 return $number;
	}
	function saveRole($data)
	{
		$this->db->insert('groups',$data);
		
		return $this->db->insert_id();
	}
	function getAllRole()
	{
		return $this->db->get('groups')->result_array();
	}
	function updateRole($data,$id)
	{
		return	$this->db->where('id',$id)->update('groups',$data);
	}
    function deleteRole($id)
	{
		$this->db->where('id',$id)->delete('groups');
	}
	function getAllMenu()
	{
		return  $this->db->get('menu')->result_array();
	}
	function getAllMenuOnly()
	{
		return  $this->db->where('parent_id','0')->get('menu')->result_array();
	}
	function saveMenu($data)
	{
		$this->db->insert('menu',$data);
		
		return $this->db->insert_id();
	}
	function updateMenu($data,$id)
	{
		return	$this->db->where('menu_id',$id)->update('menu',$data);
	}
	function deleteMenu($id)
	{
		$this->db->where('menu_id',$id)->delete('menu');
	}
	function getChild($id)
	{
		return  $this->db->where('parent_id',$id)->get('menu')->result_array();
	}
	function getAssignList()
	{
		return $this->db->get('role_menu_assign')->result_array();
	}
	function SaveRoleAssign($data)
	{
		$this->db->insert('role_menu_assign',$data);
		
		return $this->db->insert_id();
	}
	function deleteAssignMenu($menu_id,$role_id)
	{
		$this->db->where('menu_id',$menu_id)->where('role_id',$role_id)->delete('role_menu_assign');
	}
	function getRoleById($id)
	{
		return $this->db->select('groups.*,users_groups.*')->join('groups','groups.id=users_groups.group_id')->where('users_groups.user_id',$id)->get('users_groups')->row_array();
	}
	function getAllMenuHeader($id)
	{
		return $this->db->select('menu.*,role_menu_assign.*')
		                ->join('role_menu_assign','role_menu_assign.menu_id=menu.menu_id')
						->where('menu.parent_id','0')
						->where('role_menu_assign.role_id',$id)
						->order_by('menu.menu_order','asc')
						->get('menu')->result_array();
		
	}
	function getAllMenuprent($id,$role_id)
	{
		return $this->db->select('menu.*,role_menu_assign.*')
		                ->join('role_menu_assign','role_menu_assign.menu_id=menu.menu_id')
						->where('menu.parent_id',$id)
						->where('role_menu_assign.role_id',$role_id)
						->order_by('menu.menu_order','asc')
						->get('menu')->result_array();
	}
}