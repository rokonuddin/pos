<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Account_model extends  CI_Model {
	
	
	function allAccount()
	{
		return $this->db->get('account')->result_array();
	}
	function deleteAccount($id)
	{
		$this->db->where('account_id',$id)->delete('account');
	}
	function saveAccount($data)
	{
		return $this->db->insert('account',$data);
	}
	function updateAccount($data,$id)
	{
		return $this->db->where('account_id',$id)->update('account',$data);
	}
	function allexpances($type)
	{
		return $this->db->select('amount_tranjection.*,f.AccountName as fromname,t.AccountName as toname')->join('account as f','f.account_id=amount_tranjection.FromAccount')->join('account as t','t.account_id=amount_tranjection.ToAccount')->where('payment_type',$type)->get('amount_tranjection')->result_array();
	}
	function getFromAccount()
	{
		return $this->db->where('AccountsType','SuperCash')->or_where('AccountsType','SuperBank')->get('account')->result_array();
	}
	function getToAccount()
	{
		return $this->db->where('AccountsType','Expense')->get('account')->result_array();
	}
	function getToAccount1()
	{
		return $this->db->where('AccountsType','Income')->get('account')->result_array();
	}
	function updateExpanse($data,$id)
	{
		return $this->db->where('amount_tranjection_id',$id)->update('amount_tranjection',$data);
	}
    function deleteExpanse($id)
	{
		return $this->db->where('amount_tranjection_id',$id)->delete('amount_tranjection');
	}
}