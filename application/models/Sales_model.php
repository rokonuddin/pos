<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Sales_model extends  CI_Model {
	
	
	function getitems($id)
	{
		return $this->db->join('inventory','inventory.product_id=product.product_id')->where('inventory.quantity >','0')->where('product.catagory_id',$id)->get('product')->result_array();
	}
	function searchProduct($search){
		$data= $this->db->select('product.product_id,product.product_code,quantity,product.product_name,catagory_name,pack_size,purchase_price,sell_price,whole_sale_price,menufacture.name as menufacture_name')
		->join('menufacture','menufacture.menufacture_id=product.manufacture_id')
		->join('catagory','catagory.catagory_id=product.catagory_id')
		->join('store','store.product_id=product.product_id')
		->like('product_name',$search)
		->get('product')->result_array();

		$data1= $this->db->select('product.product_id,product.product_code,quantity,product.product_name,catagory_name,pack_size,purchase_price,sell_price,whole_sale_price,menufacture.name as menufacture_name')
		->join('menufacture','menufacture.menufacture_id=product.manufacture_id')
		->join('catagory','catagory.catagory_id=product.catagory_id')
		->join('store','store.product_id=product.product_id')
		->like('product_code',$search)
		->get('product')->result_array();

		$data2= $this->db->select('product.product_id,product.product_code,quantity,product.product_name,catagory_name,pack_size,purchase_price,sell_price,whole_sale_price,menufacture.name as menufacture_name')
		->join('menufacture','menufacture.menufacture_id=product.manufacture_id')
		->join('catagory','catagory.catagory_id=product.catagory_id')
		->join('store','store.product_id=product.product_id')
		->like('menufacture.name',$search)
		->get('product')->result_array();

		$data3= $this->db->select('product.product_id,product.product_code,quantity,product.product_name,catagory_name,pack_size,purchase_price,sell_price,whole_sale_price,menufacture.name as menufacture_name')
		->join('menufacture','menufacture.menufacture_id=product.manufacture_id')
		->join('catagory','catagory.catagory_id=product.catagory_id')
		->join('store','store.product_id=product.product_id')
		->like('catagory_name',$search)
		->get('product')->result_array();

		return array_merge($data,$data1,$data2,$data3);
	}
	function getAllitemsByCategoryId($id)
	{
		return $this->db->where('catagory_id',$id)->get('product',5, 0)->result_array();
	}
	function getitemsProduct($id)
	{
		return $this->db->where('product_id',$id)->get('product')->row_array();
	}
	function getCustomer()
	{
		return $this->db->get('customer', 5, 0)->result_array();
	}
	function getcatagory()
	{
		
		return $this->db->get('catagory', 5, 0)->result_array();
		
	}
	function check_product($id)
	{
		return $this->db->where('product_id',$id)->get('inventory')->row_array();
	}
	function save_invoice($data)
	{
		$this->db->insert('sales',$data);
		return $this->db->insert_id();
	}
	function amount_receive($data)
	{
		$this->db->insert('amount_tranjection',$data);
		return $this->db->insert_id();
	}
	function save_sales($data)
	{
		$this->db->insert('sales_product',$data);
		return $this->db->insert_id();
	}
	
	
	function searchs($date)
	{
		return $this->db->select('customer.*,sales.*')->join('customer','customer.customer_id=sales.customer_id')->where('invoice_date',$date)->get('sales')->result_array();
	}
	function searchs_sales($invoice_number)
	{
		return $this->db->select('product.*,sales_product.*')->join('product','product.product_id=sales_product.product_id')->where('sales_product.invoice_number',$invoice_number)->get('sales_product')->result_array();
	}
	function searchsDue($date)
	{
		return $this->db->select('customer.*,sales.*')->join('customer','customer.customer_id=sales.customer_id')->where('status','due')->where('invoice_date',$date)->get('sales')->result_array();
	}
	function searchsPaid($date)
	{
		return $this->db->select('customer.*,sales.*')->join('customer','customer.customer_id=sales.customer_id')->where('status','paid')->where('invoice_date',$date)->get('sales')->result_array();
	}
	function getById($id)
	{
		return $this->db->where('sales_id',$id)->get('sales')->row_array();
	}
	function updateSales($id,$data)
	{
		return $this->db->where('sales_id',$id)->update('sales',$data);
	}
	function search_customer($code)
	{
		return $this->db->like('customer_code',$code)->get('customer')->result_array();
	}
	function search_customer_phone($phone)
	{
		return $this->db->like('phone',$phone)->get('customer')->row_array();
	}
	function search_category($name)
	{
		return $this->db->like('catagory_name',$name)->get('catagory')->result_array();
	}
	function search_item($name,$id)
	{
		return $this->db->like('product_code',$name)->where('catagory_id',$id)->get('product')->result_array();
	}
    function invoiceInfo($id)
	{
		$user = $this->ion_auth->user()->row();
		$data= $this->db->select('customer.*,invoice.*,config.company_name,config.address_line_1,config.image,config.phone as cphone')
			->join('customer','customer.customer_id=invoice.ref_id')
			->join('config','config.code_number=invoice.client_code')
			->where('invoice_type','2')
			->where('invoice.id',$id)->get('invoice')->row_array();
		$data['sales']=array();
		$data['sales']=$this->db->select('product_tranjection.*,product.*')
									->join('product','product.product_id=product_tranjection.product_id')
									->where('invoice_id',$id)
									->where('product_tranjection.client_code',$user->client_code)
									->get('product_tranjection')->result_array();
		return $data;
	}
	function invoiceInfoProduct($id)
	{
		return $this->db->select('product.*,sales_product.*')->join('product','product.product_id=sales_product.product_id')->where('sales_product.invoice_number',$id)->get('sales_product')->result_array();
	}
	function invoicepaid($id)
	{
		return $this->db->where('refernce_type',$id)->where('transction_type','sales')->get('amount_tranjection')->row_array();
	}
	function invoicedue($id)
	{
		return $this->db->select('sum(due_amount) as total')->where('customer_id',$id)->get('sales')->row_array();
	}
	function invoicedue1($id)
	{
		return $this->db->where('customer_id',$id)->order_by('sales_id','desc')->get('sales')->result_array();
	}
	function get_total_due($id)
	{
		return $this->db->select('sum(due_amount) as total')->where('customer_id',$id)->get('sales')->row_array();
	}
	function getTotalDue($id)
	{
		return $this->db->where('status','due')->where('customer_id',$id)->get('sales')->result_array();
	}
	
	function updateDue($data,$id)
	{
		return $this->db->where('sales_id',$id)->update('sales',$data);
	}
	function getCustomerId($id)
	{
		return $this->db->where('customer_id',$id)->get('customer')->row_array();
	}
	function updateCustomer($data,$id)
	{
		return $this->db->where('customer_id',$id)->update('customer',$data);
	}
    function getcustomerPaidAmount($id)
	{
		return $this->db->select('customer.*,amount_tranjection.*')->join('amount_tranjection','amount_tranjection.refernce_type=customer.customer_id')->where('amount_tranjection.transction_type','customer')->order_by('amount_tranjection.amount_tranjection_id','DESC')->where('customer.customer_id',$id)->get('customer')->row_array();
	}
	function getDueAmount($id)
	{
		return $this->db->select('sum(due_amount) as due')->where('status','due')->get('sales')->row_array();
	}
	function getitemsProductExp($id)
	{
		return $this->db->where('product_id',$id)->where('quantity >','0')->get('purchase_product')->result_array();
	}
	function check_purchase_product($id)
	{
		return $this->db->where('quantity >','0')->where('product_id',$id)->get('purchase_product')->result_array();
		
	}
	function updateProduct($data,$id)
	{
		return $this->db->where('purchase_product_id',$id)->update('purchase_product',$data);
	}
}