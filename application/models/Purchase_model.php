<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Purchase_model extends  CI_Model {
	
	
	function getitems($id)
	{
		return $this->db->where('catagory_id',$id)->get('product')->result_array();
	}
	function getitemsProduct($id)
	{
		return $this->db->where('product_id',$id)->get('product')->row_array();
	}
	function searchProduct($search){
		$data= $this->db->select('product.product_id,product.product_code,product.product_name,catagory_name,pack_size,purchase_price,sell_price,whole_sale_price,menufacture.name as menufacture_name')
		->join('menufacture','menufacture.menufacture_id=product.manufacture_id')
		->join('catagory','catagory.catagory_id=product.catagory_id')
		->like('product_name',$search)
		->get('product')->result_array();

		$data1= $this->db->select('product.product_id,product.product_code,product.product_name,catagory_name,pack_size,purchase_price,sell_price,whole_sale_price,menufacture.name as menufacture_name')
		->join('menufacture','menufacture.menufacture_id=product.manufacture_id')
		->join('catagory','catagory.catagory_id=product.catagory_id')
		->like('product_code',$search)
		->get('product')->result_array();

		$data2= $this->db->select('product.product_id,product.product_code,product.product_name,catagory_name,pack_size,purchase_price,sell_price,whole_sale_price,menufacture.name as menufacture_name')
		->join('menufacture','menufacture.menufacture_id=product.manufacture_id')
		->join('catagory','catagory.catagory_id=product.catagory_id')
		->like('menufacture.name',$search)
		->get('product')->result_array();

		$data3= $this->db->select('product.product_id,product.product_code,product.product_name,catagory_name,pack_size,purchase_price,sell_price,whole_sale_price,menufacture.name as menufacture_name')
		->join('menufacture','menufacture.menufacture_id=product.manufacture_id')
		->join('catagory','catagory.catagory_id=product.catagory_id')
		->like('catagory_name',$search)
		->get('product')->result_array();

		return array_merge($data,$data1,$data2,$data3);
	}
	function save_invoice($data)
	{
		$this->db->insert('invoice',$data);

		return $this->db->insert_id();
	}
	function getSupplier($code){
		return $this->db->where('client_code',$code)->get('supplier')->result_array();
	}
	function getInvoiceNumber($code){
		$number= $this->db->select_sum('id')->where('client_code',$code)->get('invoice')->row_array();

    	if($number['id']==null){
    		return $code.'00001';
    	}else{
    		if($number['id']<10){
    			return $code.'000'.($number['id']+1);
    		}else if($number['id']>=10 && $number['id']<100){
    			return $code.'00'.($number['id']+1);
    		}else if($number['id']>=100 && $number['id']<1000){
    			return $code.'0'.($number['id']+1);
    		}else{
    			return $code.($number['id']+1);
    		}
    		
    	}

		
	}
	function getManufacture()
	{
		return $this->db->get('menufacture',5,0)->result_array();
	}
	function amount_disburse($data)
	{
		$this->db->insert('amount_tranjection',$data);
		return $this->db->insert_id();
	}
	function save_purchase($data)
	{
		$this->db->insert('purchase_product',$data);
		return $this->db->insert_id();
	}
	function search_mennfacture($phone)
	{
		return $this->db->like('phone',$phone)->get('menufacture')->result_array();
	}
	function search_category($id)
	{
		return $this->db->join('product','product.catagory_id=catagory.catagory_id')->where('manufacture_id',$id)->get('catagory')->result_array();
	}
	function save_purchase_tranjection($data)
	{
		$this->db->insert('product_tranjection',$data);
		return $this->db->insert_id();
	}
	function getDueAmount($id)
	{
		return $this->db->where('menufacture_id',$id)->get('menufacture')->row_array();
	}
	function getInvoiceProduct($id,$code)
	{
		return $this->db->where('client_code',$code)->where('product_id',$id)->get('store')->row_array();
	}
	function save_inventory($data)
	{
		$this->db->insert('store',$data);
		return $this->db->insert_id();
	}
	function update_inventory($data,$id,$code)
	{
		$this->db->where('product_id',$id)->where('client_code',$code)->update('store',$data);
	}
	function searchs($date)
	{
		return $this->db->select('menufacture.*,purchase.*')->join('menufacture','menufacture.menufacture_id=purchase.menufacture_id')->where('invoice_date',$date)->get('purchase')->result_array();
	}
	function searchs_product($invoice_number)
	{
		return $this->db->select('product.*,purchase_product.*')->join('product','product.product_id=purchase_product.product_id')->where('purchase_product.invoice_number',$invoice_number)->get('purchase_product')->result_array();
	}
	function searchsDue($date)
	{
		return $this->db->select('menufacture.*,purchase.*')->join('menufacture','menufacture.menufacture_id=purchase.menufacture_id')->where('purchase.pay_status','due')->where('purchase.invoice_date',$date)->get('purchase')->result_array();
	}
	function searchsPaid($date)
	{
		return $this->db->select('menufacture.*,purchase.*')->join('menufacture','menufacture.menufacture_id=purchase.menufacture_id')->where('purchase.pay_status','cash')->where('purchase.invoice_date',$date)->get('purchase')->result_array();
	}
	function getById($id)
	{
		return $this->db->where('purchase_id',$id)->get('purchase')->row_array();
	}
	function updatePurchase($id,$data)
	{
		return $this->db->where('purchase_id',$id)->update('purchase',$data);
	}
	//////////////////rules///////////////////////////////////////////////
	function getCustomerId($id)
	{
		return $this->db->where('menufacture_id',$id)->get('menufacture')->row_array();
	}
    function getMenufactureDue($id)
	{
		return $this->db->select('sum(due_amount) as due')->where('menufacture_id',$id)->get('purchase')->row_array();
	}
    function invoiceInfo($id)
	{
		$user = $this->ion_auth->user()->row();
		$data=array();
		$data= $this->db->select('supplier.supplier_name,supplier.address as saddress,supplier.phone as sphone,
			supplier.advance,supplier.due as due_total,invoice.*,config.company_name,config.address_line_1,config.image,config.phone as cphone')
			->join('supplier','supplier.id=invoice.ref_id')
			->join('config','config.code_number=invoice.client_code')
			->where('invoice.id',$id)
			->where('invoice_type','1')
			->where('invoice.client_code',$user->client_code)
			->get('invoice')->row_array();
		$data['purchases']=array();
		$data['purchases']=$this->db->select('product_tranjection.*,product.*')
									->join('product','product.product_id=product_tranjection.product_id')
									->where('invoice_id',$id)
									->where('product_tranjection.client_code',$user->client_code)
									->get('product_tranjection')->result_array();
		return $data;
	}
    function invoicepaid($id)
	{
		return $this->db->where('refernce_type',$id)->where('transction_type','purchase')->get('amount_tranjection')->row_array();
	}
    function invoicedue($id)
	{
		return $this->db->select('sum(due_amount) as total')->where('menufacture_id',$id)->get('purchase')->row_array();
	}
	function getMenufactureId($id)
	{
		return $this->db->where('menufacture_id',$id)->get('menufacture')->row_array();
	}
	function invoiceInfoProduct($id)
	{
		return $this->db->select('product.*,purchase_product.*')->join('product','product.product_id=purchase_product.product_id')->where('purchase_product.invoice_number',$id)->get('purchase_product')->result_array();
	}
   function get_total_due($id)
	{
		return $this->db->select('sum(due_amount) as total')->where('menufacture_id',$id)->get('purchase')->row_array();
	}
	function getTotalDue($id)
	{
		return $this->db->where('pay_status','due')->where('menufacture_id',$id)->get('purchase')->result_array();
	}
	function updateDue($data,$id)
	{
		return $this->db->where('purchase_id',$id)->update('purchase',$data);
	}
    function updateMenufacture($data,$id)
	{
		return $this->db->where('menufacture_id',$id)->update('menufacture',$data);
	}
}