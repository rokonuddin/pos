<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Reports_model extends  CI_Model {
	
	function dateWisePurchaseStatement($id,$s_date,$e_date)
	{
		 return $this->db
		                 
						 ->where('menufacture_id',$id)
						  ->where('invoice_date BETWEEN "'.$s_date. '" and "'.$e_date.'"')
						 ->get('purchase')->result_array();
	}
	function dateWiseAmountStatement($name,$s_date,$e_date)
	{
		return $this->db->select('menufacture.*,amount_tranjection.*')
		                 ->join('menufacture','menufacture.menufacture_id=purchase.menufacture_id')
						 ->join('amount_tranjection','amount_tranjection.refernce_type=purchase.purchase_id')
						 ->where('menufacture.name',$name)
						  ->where('amount_tranjection.date BETWEEN "'.$s_date. '" and "'.$e_date.'"')
						 ->get('purchase')->result_array();
	}
	
	
	function getdateWiseAmountMenufacture($date,$id)
	{
		return $this->db->where('date',$date)->where('transction_type','purchase')->where('refernce_type',$id)->get('amount_tranjection')->row_array();
	}
	function getCustomer()
	{
		return $this->db->get('customer')->result_array();
	}
    function getDueAmount($id,$start_date)
	{
		
		return $this->db->where('invoice_date < ',$start_date)->where('customer_id',$id)->order_by('sales_id','desc')->get('sales')->row_array();
	}
    function getAllMenufacture()
	{
		return $this->db->get('menufacture')->result_array();
	}
	function getAdvanceAmount($id)
	{
		return $this->db->where('customer_id',$id)->get('customer')->row_array();
	}
    function dateWiseSalesStatement($customer_id,$s_date,$e_date)
	{
		 return $this->db->where('sales.customer_id',$customer_id)
						 ->where('sales.invoice_date BETWEEN "'.$s_date. '" and "'.$e_date.'"')
						 ->group_by('sales_id')->get('sales')->result_array();
	}
    function getDueAmountMenufacture($id,$date)
	{
		return $this->db->where('menufacture_id',$id)->where('invoice_date < ',$date)->order_by('purchase_id','desc')->get('purchase')->row_array();
	}
	function getAdvanceAmountMenufacture($id)
	{
		return $this->db->where('menufacture_id',$id)->get('menufacture')->row_array();
	}
	function getdateWiseAmount($date,$id)
	{
		return $this->db->where('date',$date)->where('transction_type','sales')->where('refernce_type',$id)->get('amount_tranjection')->row_array();
	}
	function dateWiseSalesAmountStatement($customer_id,$s_date,$e_date)
	{
		return $this->db->select('customer.*,amount_tranjection.*')
		                 ->join('customer','customer.customer_id=sales.customer_id')
						 ->join('amount_tranjection','amount_tranjection.refernce_type=sales.sales_id')
						 ->where('sales.customer_id',$customer_id)
						  ->where('amount_tranjection.date BETWEEN "'.$s_date. '" and "'.$e_date.'"')
						 ->get('sales')->result_array(); 
	}
	function getSummaryReports($first,$last)
	{
		  $data1=$this->db->select('sum(invoice_total) as total')
		                  ->where('invoice_date BETWEEN "'.$first. '" and "'.$last.'"')
						  ->get('purchase')->row_array();
		  $data2=$this->db->select('sum(due_amount) as total')
		                  ->where('invoice_date BETWEEN "'.$first. '" and "'.$last.'"')
						  ->get('purchase')->row_array();
						  
		 $data3=$this->db->select('sum(invoice_total) as total')
		                  ->where('invoice_date BETWEEN "'.$first. '" and "'.$last.'"')
						  ->get('sales')->row_array();
		  $data4=$this->db->select('sum(due_amount) as total')
		                  ->where('invoice_date BETWEEN "'.$first. '" and "'.$last.'"')
						  ->get('sales')->row_array();
		 $data5=$this->db->select('sum(amount) as total')
		                  ->where('transction_type','purchase')
		                  ->where('date BETWEEN "'.$first. '" and "'.$last.'"')
						  ->get('amount_tranjection')->row_array();
		 $data6=$this->db->select('sum(amount) as total')
		                  ->where('transction_type','sales')
		                  ->where('date BETWEEN "'.$first. '" and "'.$last.'"')
						  ->get('amount_tranjection')->row_array();
		$data7=$this->db->select('sum(amount) as total')
		                  ->where('payment_type','expanse')
		                  ->where('date BETWEEN "'.$first. '" and "'.$last.'"')
						  ->get('amount_tranjection')->row_array();
		$data8=$this->db->select('sum(amount) as total')
		                  ->where('payment_type','income')
		                  ->where('date BETWEEN "'.$first. '" and "'.$last.'"')
						  ->get('amount_tranjection')->row_array();
		return array($data1['total'],$data2['total'],$data3['total'],$data4['total'],$data5['total'],$data6['total'],$data7['total'],$data8['total']);
	}
    function getStatementReports($first,$last)
	{
		for($i=$first;$i<=$last;)
		{
			
			$data= $this->db->select('amount_tranjection.*,menufacture.name')
		                  ->join('purchase','purchase.purchase_id=amount_tranjection.refernce_type')
						  ->join('menufacture','menufacture.menufacture_id=purchase.menufacture_id')
		                  ->where('transction_type','purchase')
						  
		                  ->where('date',$i)
						  ->get('amount_tranjection')->result_array();
				
			$data1= $this->db->select('amount_tranjection.*,customer.customer_name as name')
		                  ->join('sales','sales.sales_id=amount_tranjection.refernce_type')
						  ->join('customer','customer.customer_id=sales.customer_id')
		                  ->where('transction_type','sales')
						  
		                  ->where('date',$i)
						  ->get('amount_tranjection')->result_array();
			$data2= $this->db->select('amount_tranjection.*,t.AccountName as name')->join('account as f','f.account_id=amount_tranjection.FromAccount')->join('account as t','t.account_id=amount_tranjection.ToAccount')->where('date',$i)->where('payment_type','expanse')->get('amount_tranjection')->result_array();
			$data3= $this->db->select('amount_tranjection.*,f.AccountName as name')->join('account as f','f.account_id=amount_tranjection.FromAccount')->join('account as t','t.account_id=amount_tranjection.ToAccount')->where('date',$i)->where('payment_type','income')->get('amount_tranjection')->result_array();
			$result[$i]=array_merge($data,$data1,$data2,$data3);
			$i=date("Y-m-d", strtotime("+1 day",strtotime($i)));
		
		}
		return $result;
	}
    function getTransection($date)
	{
		$data= $this->db->select('amount_tranjection.*,menufacture.name')
		                  ->join('purchase','purchase.purchase_id=amount_tranjection.refernce_type')
						  ->join('menufacture','menufacture.menufacture_id=purchase.menufacture_id')
		                  ->where('transction_type','purchase')
						  
		                  ->where('date',$date)
						  ->get('amount_tranjection')->result_array();
				
			$data1= $this->db->select('amount_tranjection.*,customer.customer_name as name')
		                  ->join('sales','sales.sales_id=amount_tranjection.refernce_type')
						  ->join('customer','customer.customer_id=sales.customer_id')
		                  ->where('transction_type','sales')
						  
		                  ->where('date',$date)
						  ->get('amount_tranjection')->result_array();
			$data2= $this->db->select('amount_tranjection.*,t.AccountName as name')->join('account as f','f.account_id=amount_tranjection.FromAccount')->join('account as t','t.account_id=amount_tranjection.ToAccount')->where('date',$date)->where('payment_type','expanse')->get('amount_tranjection')->result_array();
			$data3= $this->db->select('amount_tranjection.*,f.AccountName as name')->join('account as f','f.account_id=amount_tranjection.FromAccount')->join('account as t','t.account_id=amount_tranjection.ToAccount')->where('date',$date)->where('payment_type','income')->get('amount_tranjection')->result_array();
			return array_merge($data,$data1,$data2,$data3);
	}
    function getByIdproduct($id)
	{
		   return $this->db->select('product.*,sales_product.*')->join('product','product.product_id=sales_product.product_id')->where('sales_product.invoice_number',$id)->get('sales_product')->result_array();
	}
	function getByIdInvoice($id)
	{
		return $this->db->where('sales_id',$id)->get('sales')->row_array();
	}
}