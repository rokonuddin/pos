<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');

class Stock_model extends  CI_Model {
	function allStock()
	{
		return $this->db
				         ->select('product.*,sum(inventory.quantity) as quantity,menufacture.*,catagory.*')
						 ->join('product','product.product_id=inventory.product_id','left')
						 ->join('menufacture','menufacture.menufacture_id=product.manufacture_id','left')
						 ->join('catagory','catagory.catagory_id=product.catagory_id','left')
						 ->group_by('inventory.product_id')
						 ->get('inventory')->result_array();
						 
						 
	}
	function menufactureStock($id)
	{
		return $this->db
				         ->select('product.*,sum(inventory.quantity) as quantity,menufacture.*,catagory.*')
						 ->join('product','product.product_id=inventory.product_id','left')
						 ->join('menufacture','menufacture.menufacture_id=product.manufacture_id','left')
						 ->join('catagory','catagory.catagory_id=product.catagory_id','left')
						 ->where('menufacture.menufacture_id',$id)
						 ->group_by('inventory.product_id')
						 ->get('inventory')->result_array();
	}
	function menufactureStockReorder($id)
	{
		return $this->db
				         ->select('product.*,sum(inventory.quantity) as quantity,menufacture.*,catagory.*')
						 ->join('product','product.product_id=inventory.product_id','left')
						 ->join('menufacture','menufacture.menufacture_id=product.manufacture_id','left')
						 ->join('catagory','catagory.catagory_id=product.catagory_id','left')
						 ->where('menufacture.menufacture_id',$id)
						 
						 ->group_by('inventory.product_id')
						 ->get('inventory')->result_array();
	}
	function expireDate()
	{
		return $this->db
				         ->select('product.*,inventory.*,menufacture.*,catagory.*')
						 ->join('inventory','product.product_id=inventory.product_id')
						 ->join('menufacture','menufacture.menufacture_id=product.manufacture_id')
						 ->join('catagory','catagory.catagory_id=product.catagory_id')
						 ->where('')
						 ->get('product')->result_array();
	}
}