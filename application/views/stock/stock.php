<div class="col-md-12" style="margin-top: 5%;width: 95%;margin-left: 1%; margin-bottom: 1%;">
			<div class="col-md-12">
				<h3 class="text-center text-success" style="margin-top: -1%;background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;height: 50px;">
						 <b style="margin-top: 50px;">Stock Balance</b> 
					</h3>
			</div>
            <form class="form-horizontal" role="form">
                <div class="col-md-12" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 97%;margin-left: 15px;">
                    

                        <div class="form-group" style="margin-top: 15px;">
                            <label for="inputEmail3" class="col-sm-2 control-label">
                                Search :
                            </label>
                            <div class="col-sm-3">
                                <input style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="" type="text" placeholder="Search By Product Code">
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-success" style="float: right; margin-right: 86%;">
                                    Print
                                </button>
                            </div>
                        </div>
                        <table class="table table-bordered table-condensed" style="margin-top: 1%;">
                            <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Product Category
                                    </th>
                                    <th>
                                        Product Name
                                    </th>
                                    <th>
                                        Product Code
                                    </th>
                                    <th>
                                        Pack Size
                                    </th>
                                    <th>
                                       Manufacture
                                    </th>
                                    
                                    <th>
                                        Quantity
                                    </th>
                                   
                                    <th>
                                        Re-order Level
                                    </th>
                                    <th>
                                       Purchase Price
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php $total=0; $i=1; foreach($stocks as $stock):
                            		if($stock['re_order_level']<$stock['quantity']){
                            		?>
                            		
                                <tr>
                                    <th>
                                        <?php echo $i++;?>
                                    </th>
                                    <th>
                                      <?= $stock['catagory_name']?>
                                    </th>
                                    <th>
                                      <?= $stock['product_name']?>
                                    </th>
                                    <th>
                                      <?= $stock['product_code']?>
                                    </th>
                                    <th>
                                          <?= $stock['pack_size']?>
                                    </th>
                                    <th>
                                         <?= $stock['name']?>
                                    </th>
                                    
                                    <th>
                                        <?= $stock['quantity']?>
                                    </th>
                                    <th>
                                       <?= $stock['re_order_level']?>
                                    </th>
                                    <th>
                                       <?php $total=$total+ $stock['quantity']*$stock['purchase_price'];echo $stock['quantity']*$stock['purchase_price'];?>
                                    </th>
                                    
                                </tr>
                                <?php }else{?>
                                	<tr class="btn-danger">
                                    <th>
                                        <?php echo $i++;?>
                                    </th>
                                    <th>
                                      <?= $stock['catagory_name']?>
                                    </th>
                                    <th>
                                      <?= $stock['product_name']?>
                                    </th>
                                    <th>
                                      <?= $stock['product_code']?>
                                    </th>
                                    <th>
                                          <?= $stock['pack_size']?>
                                    </th>
                                    <th>
                                         <?= $stock['name']?>
                                    </th>
                                    <?php if($stock['quantity']<0){?>
                                    <th>
                                        <?php echo '('.-($stock['quantity']).')'?>
                                    </th>
                                    <?php }else{?>
                                    	<th>
                                        <?= $stock['quantity']?>
                                    </th>
                                    	<?php }?>
                                    <th>
                                       <?= $stock['re_order_level']?>
                                    </th>
                                    <th>
                                       <?php $total=$total+ $stock['quantity']*$stock['purchase_price'];echo $stock['quantity']*$stock['purchase_price'];?>
                                    </th>
                                    
                                </tr>
                                <?php } endforeach;?>
                                <tr>
                                	<td style="text-align: center;color:red" colspan="8"> Total:</td>
                                	<td style="text-align: center;color:red" > <?= $total?></td>
                                </tr>
                            </tbody>
                        </table>
                   
                </div>

            </form>		
			</div>	
<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script> 	
	<script type="text/javascript">
             $(function () {
                $('#ExpDate').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('#datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
     </script>