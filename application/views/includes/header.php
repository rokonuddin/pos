<?php $title=$this->settings_model->getconfig();?>
<?php 
$user = $this->ion_auth->user()->row();
$role_id=$this->settings_model->getRoleById($user->id);
$menus=$this->settings_model->getAllMenuHeader($role_id['group_id']);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title['company_name'];?></title>
    <link rel="shortcut icon"   href="<?= base_url('./companyLogo/15x15/'.$title['image'])?>"  type="image/x-icon" >

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
     
    <link href="<?= base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
     <link href="<?= base_url('assets/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/style.css');?>" rel="stylesheet"> 
    
    <link rel="stylesheet" href="<?php echo base_url('dist/css/formValidation.css');?>"/>
  </head>
  
  <body>
    <div class="container-fluid">
	<div class="row">
		<div class="col-md-1">
		</div>
		<div class="col-md-10">
			<nav class="navbar navbar-success navbar-fixed-top background" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">						 
						<!--a class="navbar-brand" href="#">Inventory</a-->
					</div>					
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
						
						<?php foreach($menus as $menu){
							
							if($menu['menu_url']!=''){
							
							?>
							
							<li class="dropdown">
							 <a href="<?php echo site_url($menu['menu_url'])?>" class="dropdown-toggle" data-toggle="dropdown"><?=$menu['menu_name']?><strong class="caret"></strong></a>
							
						</li>
							
						   <?php }else{
						   	
							$prentmenus=$this->settings_model->getAllMenuprent($menu['menu_id'],$menu['role_id']);
						   	
						   	?>
						   	
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$menu['menu_name']?><strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<?php foreach($prentmenus as $prentmenu){?>
								<li>
									<a href="<?php echo site_url($prentmenu['menu_url'])?>"><?=$prentmenu['menu_name']?></a>
									
								</li>
								<li class="divider">
								</li>
								<?php }?>
								<!--li>
									<a href="<?php echo site_url('settings/role')?>">User Role</a>
									
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="<?php echo site_url('settings/menu')?>">Menu</a>
									
								</li>
								<li class="divider">
								</li>
								<li >
									<a href="<?php echo site_url('settings/assignRole')?>">Role Permission</a>
									
								</li>
								<li class="divider">
								</li>
								<li >
									<a href="#">User Fuction Permission</a>
									
								</li-->
								
							</ul>
						</li>
						<?php } }?>
						<!--li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="<?php echo site_url('settings/configview/2')?>">Config</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="<?php echo site_url('settings/manufacture/0')?>">Manufacture</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="<?php echo site_url('settings/catagory')?>">Product Category</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="<?php echo site_url('settings/customer')?>">Customer</a>
								</li>
							</ul>
						</li>						
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sales<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="<?= site_url('sales')?>">Sale</a>
								</li>
								<li class="divider">
								</li>
								<!--li>
									<a href="<?= site_url('settings/query')?>">Sale Query</a>
								</li>
								<li class="divider">
								</li-->
								<!--li>
									<a href="<?= site_url('sales/salesHistory')?>">Sales History</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="<?= site_url('sales/onlyPaid')?>">Payment</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Sales Return</a>
								</li>
							</ul>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Purcahse<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="<?php echo site_url('purchase')?>">Purchase</a>
								</li>
								<li class="divider">
								</li>
								<!--li>
									<a href="<?= site_url('settings/queryPurchase')?>">Purchase Query</a>
								</li>
								<li class="divider">
								</li-->
								<!--li>
									<a href="<?= site_url('purchase/purchaseHistory')?>">Purchase History</a>
								</li>
								
							</ul>
						</li>	
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Product<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="<?= site_url('settings/product')?>">Product Entry</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="<?=site_url('settings/productOption')?>">Opening Product</a>
								</li>
								
							</ul>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Stock<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Product Exchange</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="<?php echo site_url('stock')?>">Stock Balance</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="<?php echo site_url('stock/expireDate')?>">Stock With Expire Date</a>
								</li>
							</ul>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<!--li>
									<a href="#">Sale Reports</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Purcahse Reports</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Client Satement</a>
								</li>
								<li class="divider">
								</li-->
								<!--li>
									<a href="#">Monthly Summary</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Monthly Total Expense Income</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Monthly Satement</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="<?php echo site_url('reports/customerStatement')?>">Customer Statement</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="<?php echo site_url('reports/menufactureStatement')?>">Menufacture Statement</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Daily Profit Summary</a>
								</li>
							</ul>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Accounts<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="<?= site_url('account')?>">Accounts Heads</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="<?= site_url('account/expanse')?>">Expanse</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Income</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Cash Bank</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Withdraw</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Bank Satement</a>
								</li>
							</ul>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">SMS<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Sent Message</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Send Message</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Income</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Cash Bank</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Withdraw</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Bank Satement</a>
								</li>
							</ul>
						</li-->
					</ul>
						

						<ul class="nav navbar-nav navbar-right" style="margin-right: 2%;">
							<li class="dropdown">
								 <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $user->first_name.' '.$user->last_name?><strong class="caret"></strong></a>
								<ul class="dropdown-menu">
									<li>
										<a href="#">Change Password</a>
									</li>
									<li class="divider">
									</li>
									<li>
										<a href="<?php echo site_url('auth/logout')?>">Log Out</a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>				
			</nav>
		</div>	
	</div>