<style>
	.alert-success{
		display: none;
	}
	.alert-default{
		display: none;
	}
	.alert-danger{
		display: none;
	}
	
</style>

<div class="col-md-12" style="margin-top: 5%;width: 95%;margin-left: 1%; margin-bottom: 1%;">
			<div class="bs-example">
				    <!-- Modal HTML -->
				   
				    <div id="myModal1" class="modal fade">
				        <div class="modal-dialog">
				            <div class="modal-content" style="width: 70%;">
				            	<div class="modal-header">
				                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                    <h3 class="modal-title title" style="text-align: center;color:green;">Product Option</h3>
				                </div>
				                <div class="modal-header">
				                	<input id="productoptionid" type="hidden" />
				                	<h4 class="modal-title title" style="text-align: center;color:green;">Are You Sure delete this?</h4><br />
				                 <a  style="margin-left: 130px;" class="btn btn-danger yes" data-dismiss="modal">Yes</a>
				                 <a  style="" class="btn btn-success cancel" data-dismiss="modal">Cancel</a>
				                </div>
				                
				         
				            </div>
				        </div>
				    </div>
				</div>
			
			<div class="col-md-12">
				<h3 class="text-center text-success" style="margin-top: -1%;background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;height: 50px;">
						 <b style="margin-top: 50px;">Product Option</b> 
					</h3>
			</div><br /><br />
			<div class="alert alert-success save_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been added to the list..
			</div>
			<div class="alert alert-default  edit_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been edit to the list..
			</div>
			<div class="alert alert-danger delete_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been delete to the list..
			</div>
			<div class="col-md-12">	
				<div class="col-md-6" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 40%;">
					<h3 class="text-center text-success">
						ADD Product Option
					</h3>
					<form class="form-horizontal" role="form" id="product_option">
						<input id="productoption_id" type="hidden" />
					
						
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Product:
							</label>
							<div class="col-sm-8">
								<select class="form-control" name="product_id" id="product_id">
								<option>---select--- </option>
								<?php foreach($products as $product):?> 
								<option value="<?= $product['product_id']?>"><?= $product['product_name']?></option>
								<?php endforeach;?>
								
								</select>
							</div>
						</div>
						
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Quantity :
							</label>
							<div class="col-sm-8">
								<input id="quantity" name="quantity" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Quantity">
							</div>
						</div>		
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Expire Date :
							</label>
							<div class="col-sm-8">
								<input id="expire_date" name="expire_date" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="expire date">
							</div>
						</div>		
						
						
						<div class="form-group save">								 
							<button  class="btn btn-success" style="float: right; margin-right: 40%;">
								Save
							</button>
						</div>
						<div class="form-group edit_button" >								 
							<a  class="btn btn-success editform" style="float: right; margin-right: 40%;">
								edit
							</a>
						</div>	
					</form>						
				</div>			
				<div class="col-md-6" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 58%;margin-left: 15px;">
					<h3 class="text-center text-success">
						Product Option List
					</h3>
					<table class="table table-bordered table-condensed subject_table" style="margin-top: 5%;">
						<thead>
							<tr>
								
								<th>
									Product Name
								</th>
								
								<th>
									Quantity
								</th>
								<th>
									Expire Date
								</th>
								
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($productOptions as $productOption):?>
							<tr>
								<td>
									<?= $productOption['product_name']?>
								</td>
								<td>
									<?= $productOption['quantity']?>
								</td>
								
								<td>
									<?= $productOption['expire_date']?>
								</td>
								
								<td>
									<a href="#" data-product_option_id="<?php echo $productOption['product_option_id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a>
									<a href="#" data-product_id ="<?php echo $productOption['product_id']?>" data-product_option_id ="<?php echo $productOption['product_option_id']?>" data-quantity ="<?php echo $productOption['quantity']?>" data-expire_date ="<?php echo $productOption['expire_date']?>"  class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
							
						</tbody>
					</table>						
				</div>
			</div>	
								
				
			</div>	
			<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
<script src="<?= base_url('dist/js/formValidation.js')?>"></script>
 <script src="<?= base_url('dist/js/framework/bootstrap.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>
<script type="text/javascript">
            $(function () {
                $('#expire_date').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('#datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
				$('.edit_button').hide();
				$('.delete_success').hide();
				$('.save_success').hide();
				$('.edit_success').hide();
				$('#product_option').formValidation({
				message: 'This value is not valid',
		        icon: {
		            valid: 'glyphicon glyphicon-ok',
		            invalid: 'glyphicon glyphicon-remove',
		            validating: 'glyphicon glyphicon-refresh'
		        },
		        fields: {
		        	product_id: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The Product filed  is required'
		                    }
		                }
		        	},
		        	quantity: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The quantity filed  is required'
		                    }
		                }
		        	}
		        }
		        }).on('success.form.fv', function(e) {
		        	
		        	
		        	var product_id=$('#product_id').val();
					var expire_date=$('#expire_date').val();
					var quantity=$('#quantity').val();
					
					//alert(manufacture_id);
					$.ajax({
					
					type:'post',
					data:{'product_id':product_id,'expire_date':expire_date,'quantity':quantity},
					url:'<?= site_url('settings/ajaxsaveproductOption')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					
					$(".alert-success").slideDown("slow");
					$(".alert-success").delay(1500);
					$(".alert-success").slideUp("slow");
					 $('#product_id').val('');
					$('#expire_date').val('');
					$('#quantity').val('');
				     }
					
					});
		        	
		        	
		        });
				
				$('body').on('click','.add',function(){
					
					var product_id=$('#product_id').val();
					var expire_date=$('#expire_date').val();
					var quantity=$('#quantity').val();
					
					//alert(manufacture_id);
					$.ajax({
					
					type:'post',
					data:{'product_id':product_id,'expire_date':expire_date,'quantity':quantity},
					url:'<?= site_url('settings/ajaxsaveproductOption')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					
					 
				     }
					
					});
				});
				
				
				
				$('body').on('click','.edit',function(){
					$('.edit_button').show();
					$('.save').hide();
					
					
					
					var productoption_id=$(this).data('product_option_id');
					var product_id=$(this).data('product_id');
					var expire_date=$(this).data('expire_date');
					var quantity=$(this).data('quantity');
					
					alert(product_id);
					$('#product_id').val(product_id);
					
					$('#expire_date').val(expire_date);
					$('#quantity').val(quantity);
					$('#productoption_id').val(productoption_id);
					
				//var menufacture=$(this).data('menufacture');
				 // var name=$(this).data('name');
					//var contact_person=$(this).data('contact_person');
					//var address=$(this).data('address');
					//var phone=$(this).data('phone');
					//var description=$(this).data('description');
				});
					
				
				
				$('body').on('click','.delete',function(){
				    
				    $('#myModal1').modal('show');
					var product_option_id=$(this).data('product_option_id');
					
					$('#productoptionid').val(product_option_id);
				});
				
				$('body').on('click','.yes',function(){
					var product_option_id=$('#productoptionid').val();
					
					$.ajax({
					
					type:'post',
					data:{'product_option_id':product_option_id},
					url:'<?= site_url('settings/ajaxdeleteProductOption')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$(".alert-danger").slideDown("slow");
					$(".alert-danger").delay(1500);
					$(".alert-danger").slideUp("slow");
					 
				     }
				});
					
					
				});
				
				
				
				
				$('body').on('click','.editform',function(){
					
					
					var product_id=$('#product_id').val();
					var expire_date=$('#expire_date').val();
					var quantity=$('#quantity').val();
					var product_option_id=$('#productoption_id').val();
					$.ajax({
					
					type:'post',
					data:{'product_option_id':product_option_id,'product_id':product_id,'expire_date':expire_date,'quantity':quantity},
					url:'<?= site_url('settings/ajaxeditProductOption')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$('#product_id').val('');
					
					$(".alert-default").slideDown("slow");
					$(".alert-default").delay(1000);
					$(".alert-default").slideUp("slow");
					$('#product_id').val('');
					$('#expire_date').val('');
					$('#quantity').val('');
					
					$('.edit_button').hide();
					$('.save').show();
					 
				     }
					
					});
				
					
				});
			});
     </script>