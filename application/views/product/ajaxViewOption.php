<thead>
							<tr>
								
								<th>
									Product Name
								</th>
								
								<th>
									Quantity
								</th>
								<th>
									Expire Date
								</th>
								
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($productOptions as $productOption):?>
							<tr>
								<td>
									<?= $productOption['product_name']?>
								</td>
								<td>
									<?= $productOption['quantity']?>
								</td>
								
								<td>
									<?= $productOption['expire_date']?>
								</td>
								
								<td>
									<a href="#" data-product_option_id="<?php echo $productOption['product_option_id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a>
									<a href="#" data-product_id ="<?php echo $productOption['product_id']?>" data-product_option_id ="<?php echo $productOption['product_option_id']?>" data-quantity ="<?php echo $productOption['sell_price']?>" data-expire_date ="<?php echo $productOption['expire_date']?>"  class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
							
						</tbody>