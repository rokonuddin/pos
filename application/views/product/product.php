<style>
	.alert-success{
		display: none;
	}
	.alert-default{
		display: none;
	}
	.alert-danger{
		display: none;
	}
	
</style>
<?php

if(!empty($number['product_code']))
{
	$productnumber=$number['product_code']+1;
	
}else{
	$productnumber='10001';
}


?>
<div class="col-md-12" style="margin-top: 5%;width: 95%;margin-left: 1%; margin-bottom: 1%;">
			<div class="bs-example">
				    <!-- Modal HTML -->
				   
				    <div id="myModal1" class="modal fade">
				        <div class="modal-dialog">
				            <div class="modal-content" style="width: 70%;">
				            	<div class="modal-header">
				                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                    <h3 class="modal-title title" style="text-align: center;color:green;">Product</h3>
				                </div>
				                <div class="modal-header">
				                	<input id="productid" type="hidden" />
				                	<h4 class="modal-title title" style="text-align: center;color:green;">Are You Sure delete this Product?</h4><br />
				                 <a  style="margin-left: 130px;" class="btn btn-danger yes" data-dismiss="modal">Yes</a>
				                 <a  style="" class="btn btn-success cancel" data-dismiss="modal">Cancel</a>
				                </div>
				                
				         
				            </div>
				        </div>
				    </div>
				</div>
			
			<div class="col-md-12">
				<h3 class="text-center text-success" style="margin-top: -1%;background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;height: 50px;">
						 <b style="margin-top: 50px;">Product</b> 
					</h3>
			</div><br /><br />
			<div class="alert alert-success save_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been added to the list..
			</div>
			<div class="alert alert-default  edit_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been edit to the list..
			</div>
			<div class="alert alert-danger delete_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been delete to the list..
			</div>
			<div class="col-md-12">	
				<div class="col-md-6" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 40%;">
					<h3 class="text-center text-success">
						ADD Product
					</h3>
					<form class="form-horizontal" role="form" id="product_form">
						<input id="product_id" type="hidden" />
					<div class="form-group" style="margin-top: 15px;">	
						<input id="product_pre_code" type="hidden" />						 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Product Code :
							</label>
							<div class="col-sm-8">
								<input id="product_code" readonly="readonly" name="product_code" value="<?= $number?>" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Product Code">
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Product Name :
							</label>
							<div class="col-sm-8">
								<input id="product_name" name="product_name" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Product Name">
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Manufacture:
							</label>
							<div class="col-sm-8">
								<select class="form-control" name="manufacture_id" id="manufacture">
								<option>---select--- </option>
								<?php foreach($menufactures as $menufacture):?> 
								<option value="<?= $menufacture['menufacture_id']?>"><?= $menufacture['name']?></option>
								<?php endforeach;?>
								
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Category:
							</label>
							<div class="col-sm-8">
								<select class="form-control" name="catagory_id" id="catagory">
								<option>---select----</option>
								<?php foreach($catagorys as $catagory):?>
								<option value="<?= $catagory['catagory_id']?>"><?= $catagory['catagory_name']?> </option>
								<?php endforeach;?>
								
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Pack Size :
							</label>
							<div class="col-sm-8">
								<input id="pack_size" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Pack Size">
							</div>
						</div>		
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Re-Order Level :
							</label>
							<div class="col-sm-8">
								<input id="re_order_level" name="re_order_level" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Re-Order Level">
							</div>
						</div>		
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Purchase Price :
							</label>
							<div class="col-sm-8">
								<input id="purchase_price" name="purchase_price" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Purchase Price">
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Whole Sale Price :
							</label>
							<div class="col-sm-8">
								<input id="whole_sale_price"  name="whole_sale_price" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Whole Sale Price">
							</div>
						</div>						
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Sell Price :
							</label>
							<div class="col-sm-8">
								<input id="sell_price" name="sell_price"  style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Sell Price">
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Due Sale Price :
							</label>
							<div class="col-sm-8">
								<input id="due_sale_price" name="due_sale_price" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Due Sale Price">
							</div>
						</div>
						
						<div class="form-group save">								 
							<button  class="btn btn-success" style="float: right; margin-right: 40%;">
								Save
							</button>
						</div>
						<div class="form-group edit_button" >								 
							<a  class="btn btn-success editform" style="float: right; margin-right: 40%;">
								edit
							</a>
						</div>	
					</form>						
				</div>			
				<div class="col-md-6" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 58%;margin-left: 15px;">
					<h3 class="text-center text-success">
						Product List
					</h3>
					<table class="table table-bordered table-condensed subject_table" style="margin-top: 5%;">
						<thead>
							<tr>
								<th>
									Product Code
								</th>
								<th>
									Product Name
								</th>
								
								<th>
									Pack Size
								</th>
								<th>
									Manufacture
								</th>
								<th>
									Price
								</th>
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($products as $product):?>
							<tr>
								<td>
									<?= $product['product_code']?>
								</td>
								<td>
									<?= $product['product_name']?>
								</td>
								
								<td>
									<?= $product['pack_size']?>
								</td>
								<td>
									<?= $product['manufacture_id']?>
								</td>
								<td>
									<?= $product['purchase_price']?>
								</td>
								<td>
									<!-- <a href="#" data-product_id="<?php echo $product['product_id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a> -->
									<a href="#" data-due_sale_price ="<?php echo $product['due_sale_price']?>" data-sell_price ="<?php echo $product['sell_price']?>" data-whole_sale_price ="<?php echo $product['whole_sale_price']?>"  data-purchase_price="<?php echo $product['purchase_price']?>" data-re_order_level="<?php echo $product['re_order_level']?>"  data-product_id="<?php echo $product['product_id']?>" data-pack_size="<?php echo $product['pack_size']?>" data-category_id="<?php echo $product['catagory_id']?>" data-manufacture_id="<?php echo $product['manufacture_id']?>"  data-product_name="<?php echo $product['product_name']?>" data-product_code="<?php echo $product['product_code']?>" class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
							
						</tbody>
					</table>						
				</div>
			</div>	
								
				
			</div>	
			<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
 <script src="<?= base_url('dist/js/formValidation.js')?>"></script>
 <script src="<?= base_url('dist/js/framework/bootstrap.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>
<script type="text/javascript">

           function autoproduct_number()
           {
           	
           	    $.ajax({
					
					type:'post',
						
					url:'<?= site_url('settings/autoproduct_number')?>',
					success : function(result){
					
					$('#product_code').val(result);
					
					 
				     }
				});
           	
           	
           }

            $(function () {
                $('#datetimepicker1').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('#datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
				$('.delete_success').hide();
				$('.save_success').hide();
				$('.edit_success').hide();
				$('#product_form').formValidation({
				message: 'This value is not valid',
		        icon: {
		            valid: 'glyphicon glyphicon-ok',
		            invalid: 'glyphicon glyphicon-remove',
		            validating: 'glyphicon glyphicon-refresh'
		        },
		        excluded: ':disabled', 
		        fields: {
		        	product_name: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The Product Name field is required'
		                    }
		                }
		        	},
		        	product_code: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The Product Code field is required'
		                    }
		                }
		        	},
		        	due_sale_price: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The Due Sale Price field is required'
		                    }
		                }
		        	},
		        	sell_price: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The Sell Price field is required'
		                    }
		                }
		        	},
		        	whole_sale_price: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The Whole Sale Price field is required'
		                    }
		                }
		        	},
		        	purchase_price: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The Purchase  Price field is required'
		                    }
		                }
		        	},
		        	re_order_level: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The Reorder level field is required'
		                    }
		                }
		        	}
		        	,manufacture_id: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The manufacture field Selected First'
		                    }
		                }
		        	}
		        	,catagory_id: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The Customer field Selected First'
		                    }
		                }
		        	}
		        }
		       }).on('success.form.fv', function(e) {
		       	
		       	var product_name=$('#product_name').val();
					var product_code=$('#product_code').val();
					var due_sale_price=$('#due_sale_price').val();
					var sell_price=$('#sell_price').val();
					var whole_sale_price=$('#whole_sale_price').val();
					var purchase_price=$('#purchase_price').val();
					var re_order_level=$('#re_order_level').val();
					var pack_size=$('#pack_size').val();
					var manufacture_id=$('#manufacture').val();
					var catagory_id=$('#catagory').val();
					//alert(manufacture_id);
					$.ajax({
					
					type:'post',
					data:{'catagory_id':catagory_id,'manufacture_id':manufacture_id,'pack_size':pack_size,'re_order_level':re_order_level,'purchase_price':purchase_price,'product_name':product_name,'product_code':product_code,'due_sale_price':due_sale_price,'sell_price':sell_price,'whole_sale_price':whole_sale_price},
					url:'<?= site_url('settings/ajaxsaveproduct')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					 $('#product_name').val('');
					$('#product_code').val('');
					$('#due_sale_price').val('');
					$('#sell_price').val('');
					$('#whole_sale_price').val('');
					$('#purchase_price').val('');
					$('#re_order_level').val('');
					$('#pack_size').val('');
					$('#manufacture').val('');
					$('#catagory').val('');
					$(".alert-success").slideDown("slow");
					$(".alert-success").delay(1500);
					$(".alert-success").slideUp("slow");
					 autoproduct_number();
					 
					    $('.form-group').each(function () { $(this).removeClass('has-success'); });
						$('.form-group').each(function () { $(this).removeClass('has-error'); });
						$('.form-group').each(function () { $(this).removeClass('has-feedback'); });
						$('.help-block').each(function () { $(this).remove(); });
						$('.form-control-feedback').each(function () { $(this).remove(); });
					 
				     }
					
					});
		       	
		       });
				
				
				
				$('.edit_button').hide();
				$('body').on('click','.add',function(){
					
					var product_name=$('#product_name').val();
					var product_code=$('#product_code').val();
					var due_sale_price=$('#due_sale_price').val();
					var sell_price=$('#sell_price').val();
					var whole_sale_price=$('#whole_sale_price').val();
					var purchase_price=$('#purchase_price').val();
					var re_order_level=$('#re_order_level').val();
					var pack_size=$('#pack_size').val();
					var manufacture_id=$('#manufacture').val();
					var catagory_id=$('#catagory').val();
					//alert(manufacture_id);
					$.ajax({
					
					type:'post',
					data:{'catagory_id':catagory_id,'manufacture_id':manufacture_id,'pack_size':pack_size,'re_order_level':re_order_level,'purchase_price':purchase_price,'product_name':product_name,'product_code':product_code,'due_sale_price':due_sale_price,'sell_price':sell_price,'whole_sale_price':whole_sale_price},
					url:'<?= site_url('settings/ajaxsaveproduct')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					
					 
				     }
					
					});
				});
				
				
				
				$('body').on('click','.edit',function(){
					$('.edit_button').show();
					$('.save').hide();
					var product_id=$(this).data('product_id');
					var product_name=$(this).data('product_name');
					var product_code=$(this).data('product_code');
					var due_sale_price=$(this).data('due_sale_price');
					var sell_price=$(this).data('sell_price');
					var whole_sale_price=$(this).data('whole_sale_price');
					var purchase_price=$(this).data('purchase_price');
					var re_order_level=$(this).data('re_order_level');
					var pack_size=$(this).data('pack_size');
					var manufacture_id=$(this).data('manufacture_id');
					var catagory_id=$(this).data('category_id');
			      var pro=		$('#product_code').val();
			      
					$('#product_id').val(product_id);
					$('#product_pre_code').val(pro);
					$('#product_name').val(product_name);
					$('#product_code').val(product_code);
					$('#due_sale_price').val(due_sale_price);
					$('#sell_price').val(sell_price);
					$('#whole_sale_price').val(whole_sale_price);
					$('#purchase_price').val(purchase_price);
					$('#re_order_level').val(re_order_level);
					$('#pack_size').val(pack_size);
					$('#manufacture').val(manufacture_id);
					$('#catagory').val(catagory_id);
					
				//var menufacture=$(this).data('menufacture');
				 // var name=$(this).data('name');
					//var contact_person=$(this).data('contact_person');
					//var address=$(this).data('address');
					//var phone=$(this).data('phone');
					//var description=$(this).data('description');
				});
					
				
				
				
				$('body').on('click','.delete',function(){
				    
				    $('#myModal1').modal('show');
					var product_id=$(this).data('product_id');
					
					$('#productid').val(product_id);
				});
				
				$('body').on('click','.yes',function(){
					var product_id=$('#productid').val();
					
					$.ajax({
					
					type:'post',
						data:{'product_id':product_id},
					url:'<?= site_url('settings/ajaxdeleteProduct')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$(".alert-danger").slideDown("slow");
					$(".alert-danger").delay(1500);
					$(".alert-danger").slideUp("slow");
					 
				     }
				});
					
					
				});
				
				
				
				
				
				$('body').on('click','.editform',function(){
					
					
					var product_id=$('#product_id').val();
					var product_name=$('#product_name').val();
					var product_code=$('#product_code').val();
					var due_sale_price=$('#due_sale_price').val();
					var sell_price=$('#sell_price').val();
					var whole_sale_price=$('#whole_sale_price').val();
					var purchase_price=$('#purchase_price').val();
					var re_order_level=$('#re_order_level').val();
					var pack_size=$('#pack_size').val();
					var manufacture_id=$('#manufacture').val();
					var catagory_id=$('#catagory').val();
					var pro=$('#product_pre_code').val();
					
					$.ajax({
					
					type:'post',
					data:{'product_id':product_id,'catagory_id':catagory_id,'manufacture_id':manufacture_id,'pack_size':pack_size,'re_order_level':re_order_level,'purchase_price':purchase_price,'product_name':product_name,'product_code':product_code,'due_sale_price':due_sale_price,'sell_price':sell_price,'whole_sale_price':whole_sale_price},
					url:'<?= site_url('settings/ajaxeditProduct')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$('#product_id').val('');
					$('#product_code').val(pro);
					$('.save').show();
					$('#product_name').val('');
					//$('#product_code').val('');
					$('#due_sale_price').val('');
					$('#sell_price').val('');
					$('#whole_sale_price').val('');
					$('#purchase_price').val('');
					$('#re_order_level').val('');
					$('#pack_size').val('');
					$('#manufacture').val('');
					$('#catagory').val('');
					 $('.edit_button').hide();
					 $(".alert-default").slideDown("slow");
					$(".alert-default").delay(1000);
					$(".alert-default").slideUp("slow");
					 
				     }
					
					});
				
					
				});
			});
     </script>