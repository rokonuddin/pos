<thead>
							<tr>
								<th>
									Product Code
								</th>
								<th>
									Product Name
								</th>
								
								<th>
									Pack Size
								</th>
								<th>
									Manufacture
								</th>
								<th>
									Price
								</th>
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($products as $product):?>
							<tr>
								<td>
									<?= $product['product_code']?>
								</td>
								<td>
									<?= $product['product_name']?>
								</td>
								
								<td>
									<?= $product['pack_size']?>
								</td>
								<td>
									<?= $product['manufacture_id']?>
								</td>
								<td>
									<?= $product['purchase_price']?>
								</td>
								<td>
									<a href="#" data-product_id="<?php echo $product['product_id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a>
									<a href="#" data-due_sale_price ="<?php echo $product['due_sale_price']?>" data-sell_price ="<?php echo $product['sell_price']?>" data-whole_sale_price ="<?php echo $product['whole_sale_price']?>"  data-purchase_price="<?php echo $product['purchase_price']?>" data-re_order_level="<?php echo $product['re_order_level']?>"  data-product_id="<?php echo $product['product_id']?>" data-pack_size="<?php echo $product['pack_size']?>" data-category_id="<?php echo $product['catagory_id']?>" data-manufacture_id="<?php echo $product['manufacture_id']?>"  data-product_name="<?php echo $product['product_name']?>" data-product_code="<?php echo $product['product_code']?>" class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
							
						</tbody>