

<div class="col-md-12" >
				<div class="col-md-12" style="margin-top: 1%; border: 1px solid #449D44; border-radius: 5px; margin-bottom: 1%;">
					<h3 style="text-align: center;color: green;">Menufacture</h3>
					<table class="table table-bordered table-condensed" style="margin-top: 1%;">
						<?php foreach($menufactures as $menufacture):?>
							<tr>
								<th colspan="5">
									<?= $menufacture['name']?>
								</th>
							</tr>
						<thead>
							<tr>
								<th class="text-center">
									Transaction Date
								</th>
								<th class="text-center">
									Invoice Number
								</th>
								<th class="text-center">
									Invoice Amount
								</th>							
								<th class="text-center">
									Credit
								</th>
								
								<th class="text-center">
									 Debit
								</th>							
								
							</tr>
						</thead>
						<tbody >
							    
							<?php $debit=0;$credit=0; $i=0; $total_purchase=0;$total_due=0; foreach($reprots[$menufacture['menufacture_id']] as $report):?>
								<tr>
									<td style="text-align: center"><?= date("jS  F Y",strtotime($report['invoice_date'])) ?></td>
									<td style="text-align: center"><?= $report['invoice_number'] ?></td>
									<td style="text-align: center"><?php $total_purchase=$total_purchase+$report['invoice_total']; echo $report['invoice_total']; ?></td>
									<td style="text-align: center"><?php $credit=$credit+$report['due_amount']; echo $report['due_amount'] ?></td>
									
									<td style="text-align: center"><?php $debit=$debit+$report['pay_amount']; echo $report['pay_amount']; ?></td>
								</tr>
								
							<?php $i++; endforeach;?>
							<tr>
								<td colspan="2" style="text-align: center;">Total Purchases</td>
								<td style="text-align: center;"><?php echo  $total_purchase; ?></td>
									<td style="text-align: center;"><?php echo $credit; ?></td>
									<td style="text-align: center;"><?=$debit?></td>
									
									
							</tr> 
						</tbody>
						<?php endforeach;?>
					</table>
				</div>
				
				</div>
				