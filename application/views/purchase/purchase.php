
<style>
	#addp{
		cursor: pointer;
		padding: 10px 19px;
	}
	.category_delete{
		cursor: pointer;
		padding: 10px 19px;
	}
	/*.category{
		display:none;
	}
	.vendorform{
		display:none;
	}
	.addpurchase{
		display:none;
	}*/
</style>
		<div class="col-md-12 new_invoice" style="margin-top: 5%;border: 1px solid #449D44; border-radius: 5px;width: 98%;margin-left: 1%; margin-bottom: 1%;">
			<form class="form-horizontal" role="form" id="all_purchase">			
			<div class="col-md-12" style="margin-bottom: 1%; text-align: center; margin-top: 1%;width: 99%;">
				<h4>Purchase View</h4><input type="hidden" id="invoice_id" name="invoice_id" value="0">
			</div>
				<div class="col-md-12" style="margin-top: 1%; border: 1px solid #449D44; border-radius: 5px; margin-bottom: 1%;">
					<div class="col-md-3">
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-12 control-label" style="text-align: left;">
								Supplier:
								</label>
								<div class="col-sm-8">
								<select class="form-control" name="supplier_id" id="supplier_id">
								<option value="0">---select--- </option>
									<?php foreach($suppliers as $supplier):?> 
									<option  value="<?= $supplier['id']?>"><?= $supplier['supplier_name']?></option>
									<?php endforeach;?>
								</select>
								</div>
						</div>	
										
					</div>
					<div class="col-md-3">
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-12 control-label" style="text-align: left;">
								Invoice Number:
								</label>
								<div class="col-sm-8">
									<input id="invoice_number" name="invoice_number" value="<?= $invoice_number?>" readonly="readonly" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="name" type="text" placeholder="Name">
								</div>
						</div>	
										
					</div>
					<div class="col-md-3">
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-12 control-label" style="text-align: left;">
								Receive Date:
								</label>
								<div class="col-sm-8">
									<input  name="receive_date" id="receive_date" value="<?= date('Y-m-d')?>"  style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control datetimepicker2" id="datetimepicker2" type="text" placeholder="Purchase Date">
								</div>
						</div>	
										
					</div>
					<div class="col-md-3">
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" id="p_due_text" class="col-sm-12 control-label" style="text-align: left;">
								Previous due:
								</label>
								<div class="col-sm-8">
									<input id="p_due_value" value="0" name="name" readonly="readonly" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control"  type="text" placeholder="Previous due">
								</div>
						</div>			
					</div>				
							
				</div>			
				<div class="col-md-12 addpurchase" style="margin-top: 1%; border: 1px solid #449D44; border-radius: 5px; margin-bottom: 1%;">
					<table class="table table-bordered table-condensed" style="margin-top: 1%;">
						<thead>
							<tr>
								<th class="text-center">
									SL
								</th>
								
								<th class="text-center">
									Product Name
								</th>							
								<th class="text-center">
									Product Code
								</th>
								<th class="text-center">
									Pack Size
								</th>
								<th class="text-center">
									Exp Date
								</th>							
								<th class="text-center">
									Quantity
								</th>
								<th class="text-center">
									Purchase Price 
								</th>
								
								<th class="text-center">
									Total Price 
								</th>
								<th class="text-center">
									Action
								</th>
								
							</tr>
						</thead>
						<tbody >
							
							
							<tr>
								<td class="text-center"> # </td>
								<td class="text-center">
									<input id="product_name" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;"  class="col-md-1 form-control" type="text"/>
									<input type="hidden"  id="items_id" />
								</td>
								
								<td class="text-center" >
									<input id="product_code" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;"  class="col-md-1 form-control" type="text"/>
								</td>
								<td class="text-center" id="pack_size_text">
									<input id="pack_size" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;"  class="col-md-1 form-control" type="text"/>
								</td>
								<td class="text-center">
									<input id="exp_date" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;"  class="col-md-1 form-control ExpDate exp_date" type="text"/>
								</td>
								<td class="text-center">

									<input   style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;"  class="col-md-1 form-control" type="text" id="unit" />
								</td>
								<td class="text-center">
									<input   type="text" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;"  class="col-md-1 form-control" id="purchase_price" />
								</td>
								
								<td class="text-center">
									<input   type="text" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;"  class="col-md-1 form-control" id="product_amount" />
									
								</td>
								<td id="addp">
									<span style="display: inline-block;"><a class="addp"  style="width: 70px;color: #A94442;"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></a></span>
								</td>
								
							</tr>
							
						</tbody>
					</table>
				</div>
				<div class="col-md-12 addpurchase" style="margin-top: 1%; border: 1px solid #449D44; border-radius: 5px; margin-bottom: 1%;">
					<table class="table table-bordered table-condensed" style="margin-top: 1%;">
						<thead>
							<tr>
								<th class="text-center">
									SL
								</th>
								
								<th class="text-center">
									Product Code
								</th>							
								<th class="text-center">
									Name
								</th>
								<th class="text-center">
									Pack Size
								</th>
								<th class="text-center">
									Exp Date
								</th>							
								<th class="text-center">
									Quantity
								</th>
								<th class="text-center">
									Purchase Price 
								</th>
								
								<th class="text-center">
									Total Price 
								</th>
								<th class="text-center">
									Remove
								</th>
							</tr>
						</thead>
						<tbody class="addproduct">
							
							
						</tbody>
					</table>
				</div>
				<div class="col-md-12" style="text-align: right;width: 99%;">
					
					<div class="col-md-9" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 61%;margin-right: 15px; margin-top:10px; float:right;">
					<div class="col-md-4">

						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-9 control-label">
									Sub Total :
								</label>
								<label for="inputEmail3" class="control-label">
								<span id="sub"></span><input type="hidden" id="sub_total" value="0" />  $$
								</label>
						</div>	
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-9 control-label">
									Invoice Total :
								</label>
								<label for="inputEmail3" class="control-label">
								<span id="invoice_total" style="color: green;"></span><input type="hidden" value="0" id="invoicetotal" /> $$
								</label>
						</div>	
					</div>
					<div class="col-md-5">
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label" style="float:left;">
									Discount :
								</label>
								<div class="col-sm-6">
								<input style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="discount" type="text" >
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label" style="float:left;">
									Transport Cost :
								</label>
								<div class="col-sm-6">
								<input style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="transport_cost" type="text" >
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label" style="float:left;">
									Other Cost :
								</label>
								<div class="col-sm-6">
								<input style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="other_cost" type="text" >
							</div>
						</div>	
						<!--div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail6" class="col-sm-12 control-label"style="float:left;">
									Cash: <input type="checkbox" class="cash" id="cash"  /> Due: <input type="checkbox" class="due" id="due" />
								</label>
								
						</div-->					
						<div class="form-group cash_pay" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label"style="float:left;">
									Cash Pay:
								</label>
								<div class="col-sm-6">
								<input style=" background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="cash_pay" type="text" >
							</div>
						</div>	
						
					</div>
					<div class="col-md-3">
						<div class="form-group">								 
							<button  class="btn btn-success" style="float: left; margin-right: 40%; margin-top:7%;">
								Save
							</button>
						</div>		
						
						<div class="form-group">								 
							<button type="submit" class="btn btn-success" style="float: left; margin-right: 40%;">
								Save & Print
							</button>
						</div>
						<div class="form-group">								 
							<button type="submit" class="btn btn-warning " style="float: left; margin-right: 40%;">
								Refresh 
							</button>
						</div>						
					</div>
				</div>
					
					
				</div>
				
				
			</div>
		</form>
		<div class="col-md-1">
		</div>
	</div>
		
		<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
<script src="<?= base_url('dist/js/formValidation.js')?>"></script>
 <script src="<?= base_url('dist/js/framework/bootstrap.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>autocom/jquery.auto-complete.css">
<script src="<?php echo base_url(); ?>autocom/jquery.auto-complete.js"></script>
<script type="text/javascript">
        var count=0;
            $(function () {

                $('.ExpDate').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('.datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
				$('#product_name').autoComplete({
                        source: function (term, response) {
                            $.getJSON('<?php echo site_url('purchase/searchproduct') ?>', {q: term}, function (data) {
                                response(data);
                            });
                        },
                        minChars: 3

                });
				
				$('#all_purchase').on('keyup keypress', function(e) {
					  var keyCode = e.keyCode || e.which;
					  if (keyCode === 13) { 
					    e.preventDefault();
					    return false;
					  }
				});
				
				
				
				
				
				$('#all_purchase').formValidation({
				message: 'This value is not valid',
		        icon: {
		            valid: 'glyphicon glyphicon-ok',
		            invalid: 'glyphicon glyphicon-remove',
		            validating: 'glyphicon glyphicon-refresh'
		        },
		        fields: {
		        	name: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The Name field is required'
		                    }
		                }
		        	}	
		        }
		        }).on('success.form.fv', function(e) {
		        	
		        	save();
		        	 
		        	
		        });
				
				
				
				
				$('body').on('click','#addp',function(){
					var p=0;
					var items_id=$('#items_id').val();
					var product_name=$('#product_name').val();
					var product_code=$('#product_code').val();
					var pack_size=$('#pack_size').val();
					var exp_date=$('#exp_date').val();
					var unit=$('#unit').val();
					var purchase_price=$('#purchase_price').val();
					var product_amount=$('#product_amount').val();
                    $(".tr_table").each(function() {
					    var rowid = $(this).attr("id");
					    var pid=$('#productid'+rowid).val();
					    if(pid==items_id){
					    	p++;
					    }
					});
					if(p==0){
						ajax_search_items_for_price(items_id,product_name,product_code,pack_size,exp_date,unit,purchase_price,product_amount);
						$('#items_id').val('');
						$('#product_name').val('');
						$('#product_code').val('');
						$('#pack_size').val('');
						$('#exp_date').val('');
						$('#unit').val('');
						$('#purchase_price').val('');
						$('#product_amount').val('');
					}else{
						alert("Already Add This Product!");
						$('#items_id').val('');
						$('#product_name').val('');
						$('#product_code').val('');
						$('#pack_size').val('');
						$('#exp_date').val('');
						$('#unit').val('');
						$('#purchase_price').val('');
						$('#product_amount').val('');
					}
					
				});
				
				$('body').on('click','.category_delete',function(){
					
					var rowTotal=0;
					$(this).closest('tr').remove();
					var i=1;
					var total=0;
					 $(".tr_table").each(function() {
					 	var rowid = $(this).attr("id");
					 	$('#textcount'+rowid).html(i++);
					 	var amount=$('#product_amount'+rowid).val();
					 	total=+total+ +amount;
					});
					$('#invoicetotal').val(total);
					$('#invoice_total').html(total);
				    $('#sub_total').val(total);
					$('#sub').html(total);
					$('#discount').val('');
					$('#transport_cost').val('');
					$('#other_cost').val('');
					count--;
			    });
			    
			    
			    
			    
			$('body').on('change','#unit', function(){
				var unitquantity = $('#unit').val();
				var rate = $("#purchase_price").val();
				var total = unitquantity*rate;
				$("#product_amount").val(total);
			    
				
			});
			
			
			$('body').on('change','#amount', function(e){
				e.preventDefault();
				rowTotal=0;
				//var unitquantity = $(this).data('items_id');
				var unitquantity = $(this).data('unit');
				
				//var row_id = $(this).data('row_id');
				var unit = $(this).val();
				
				var rate = $(".unit_"+unitquantity).val();
				
				var total = unit*rate;
				$("#items_rate_"+unitquantity).html(rate);
			    $("#unit_total_price_"+unitquantity).html(total);
			    $("#unit_value_"+unitquantity).val(total);
			    var s = $("#unit_total_price_"+unitquantity).html();
			    var totalUnitAmount = $("#unit_value_"+unitquantity).val();
			      $(".row_total").each(function() {
					    rowTotal += parseInt($(this).val());
					    $('#sub').html(rowTotal);
			    		$('#sub_total').val(rowTotal);
			    		$('#invoice_total').html(rowTotal);
			            $('#invoicetotal').val(rowTotal);
					});
				
			});
			
			$('body').on('change','#discount', function(e){
			  
			   var discount=$('#discount').val();
			   var subtotal= $('#sub_total').val();
			   
			   
			    var transport_cost=$('#transport_cost').val();
			   var other_cost= $('#other_cost').val();
			    var total=+subtotal+ +transport_cost+ +other_cost;
			    var total=total-discount
			   $('#invoice_total').html(total);
			   $('#invoicetotal').val(total);
			});
			$('body').on('change','#transport_cost', function(e){
			
			    var discount=$('#discount').val();
			   var subtotal= $('#sub_total').val();
			    var transport_cost=$('#transport_cost').val();
			   var other_cost= $('#other_cost').val();
			   var total=+subtotal+ +transport_cost+ +other_cost;
			    var total=total-discount
			   $('#invoice_total').html(total);
			   $('#invoicetotal').val(total);
			});
			$('body').on('change','#other_cost', function(e){
			
			   var discount=$('#discount').val();
			   var subtotal= $('#sub_total').val();
			    var transport_cost=$('#transport_cost').val();
			   var other_cost= $('#other_cost').val();
			    var total=+subtotal+ +transport_cost+ +other_cost;
			    var total=total-discount
			   $('#invoice_total').html(total);
			   $('#invoicetotal').val(total);
			
			});
			$('body').on('change','#cash_pay', function(e){
			
			   var total=$('#invoicetotal').val();
			   var cash_pay= $('#cash_pay').val();
			   
			   
			   
			   
			
			});
			    
			});
			function save()
			{
				
		        	
		        	$('.save_all').hide();
		        	ordersObj = [];
		        	var k=0;
				   $(".tr_table").each(function() {
				    var rowid = $(this).attr("id");
				    k++;
				    var itemid=$('#productid'+rowid).val();

				    var unit=$('#unit'+rowid).val();
				    var exp_date=$('#exp_date'+rowid).val();
				    var unit_price=$('#purchase_price'+rowid).val();
				    var product_amount=$('#product_amount'+rowid).val();
				    
							
					
                	
                	item = {}
       				item ["itemid"] = itemid;
        			item ["unit"] = unit;
        			item ["unit_price"] = unit_price;
        			item ["exp_date"] = exp_date;
        			item ["product_amount"] = product_amount;
        			
        			
                	ordersObj.push(item);
                					  
				   });
					orders = JSON.stringify(ordersObj);
					
					var invoice_id = $('#invoice_id').val();
					var supplier_id = $('#supplier_id').val();
					var invoice_number = $('#invoice_number').val();
					var receive_date = $('#receive_date').val();
					var p_due_value = $('#p_due_value').val();
				
				 
				 
					var subtotal = $('#sub_total').val();
					var invoicetotal = $('#invoicetotal').val();
					var discount = $('#discount').val();
					var transport_cost = $('#transport_cost').val();
					var other_cost = $('#other_cost').val();
					var cash_pay = $('#cash_pay').val();
					var url = "<?= site_url('purchase/save_invoice');?>";
					if(k!=0 && supplier_id!=0){
						$.ajax({
						type : "post",
						data : {'invoice_id': invoice_id, 'supplier_id': supplier_id, 'p_due_value':p_due_value,
						'subtotal' : subtotal, 'discount' : discount, 'transport_cost':transport_cost,'invoice_number':invoice_number,
						'invoicetotal' : invoicetotal,'cash_pay' : cash_pay,  'orders':orders,'other_cost': other_cost,'cash_pay': cash_pay},
						async : false,
						dataType : "json",
						url : url,
					    success : function(result){
					    	      
									 
										 $('#sub_total').val('');
										$('#invoicetotal').val('');
										$('#discount').val('');
										$('#transport_cost').val('');
										$('#other_cost').val('');
										 $('#cash_pay').val('');
					    	
					         window.location.assign('purchase/purchasePrint/'+result);
					    }
						
						});
					}else{
						if(k==0){
							alert('Please Add Your Product!');
						}else{
							alert('Select Your Supplier!');
						}
						
					}
		        	
		        	
			}
			function ajax_search_items_for_price(items_id,product_name,product_code,pack_size,exp_date,unit,purchase_price,product_amount)
			{
				
					count++;
					$.ajax({
						type:'post',
						data:{'count':count,'items_id':items_id,'product_name':product_name,'product_code':product_code,'pack_size':pack_size,'exp_date':exp_date,'unit':unit,'purchase_price':purchase_price,'product_amount':product_amount},
						url:'<?php echo site_url('purchase/addproduct')?>',
						success: function(result){
							
							$('.addproduct').append(result);

							var invoicetotal=$('#invoicetotal').val();
							var sub_total=$('#sub_total').val();
							$('#invoicetotal').val(+invoicetotal+ +product_amount);
							$('#invoice_total').html(+invoicetotal+ +product_amount);
							$('#sub_total').val(+sub_total+ +product_amount);
							$('#sub').html(+sub_total+ +product_amount);
						}
					});
			
			}
     </script>

   	
	