<?php 
$user = $this->ion_auth->user()->row();
   		$user_id = $user->id;
        $year=date('Y');
        $month=date('m');
        $checkInvoice=$this->db->select('invoice_number')->where('created_by',$user->id)->order_by('purchase_id','desc')->get('purchase')->row_array();
        
        if(!empty($checkInvoice))
        {
                     $invoicearray=(explode("-", $checkInvoice['invoice_number']));
                       $newstring=intval($invoicearray[2]);
                     $newstring=$newstring+1;
                      $invoiceNumber=date('Y').'-'.$user->id.'-'.$newstring;
        }else{
            $invoiceNumber=date('Y').'-'.$user->id.'-'.'1';
        }


?>


<form class="form-horizontal" role="form">			
			<div class="col-md-12" style="margin-bottom: 1%; text-align: right; margin-top: 1%;width: 99%;">
				<a class="btn btn-success addvendor" style="">New</a><a class="btn btn-danger" style="margin-left: 1%;">Close</a>
			</div>
				<div class="col-md-12" style="border: 1px solid #449D44; border-radius: 5px;width: 97%;margin-left: 15px;">
					<div class="col-md-6" style="border-right: 1px solid #449D44;">
						<div class="form-group form-inline" style="margin-top: 15px; margin-left: 3%;">
							<input class="form-control" type="search" role="search" placeholder="Name/Phone">
							<button type="submit" class="btn btn-primary" style="display: inline;">
									Search
								</button>						
						</div>
						<div class="col-md-12">
							<table class="table table-bordered table-condensed">
								<thead>
									<tr>
										<th>
											#
										</th>
										<th>
										 Manufactrue Name
										</th>
										
										<th>
											Phone Number
										</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=0; foreach($menufactures as $menufacture):?>
									<tr>
										<td>
											<?php echo $i++;?>
										</td>
										<td>
											<?php echo  $menufacture['name'];?>
										</td>
										<td>
											<?php echo  $menufacture['phone'];?>
										</td>
										<td> <a data-menufacture_id="<?=$menufacture['menufacture_id']?>" data-address="<?=$menufacture['address']?>"  data-phone="<?=$menufacture['phone']?>" data-name="<?=$menufacture['name']?>" class="btn btn-success purchase" >Add</a></td>
									</tr>
									<?php endforeach;?>
									
									
								</tbody>
							</table>
						</div>					
					</div>					
					<div class="col-md-6 vendorform" >
						<!--form class="form-horizontal" role="form"-->
						<input type="hidden" name="menufacture_id" id="menufacture_id"  />
							<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-4 control-label">
								Manufacture Name:
								</label>
								<div class="col-sm-6">
									<input id="name" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="name" type="text" placeholder="Name">
								</div>
							</div>		
							<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-4 control-label">
								 	Phone :
								</label>
								<div class="col-sm-6">
									<input id="phone" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="phone" type="text" placeholder="Phone">
								</div>
							</div>	
							<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-4 control-label">
									Address :
								</label>
								<div class="col-sm-6">
									<input id="address" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="address" type="text" placeholder="Address">
								</div>
							</div>	
								
                          <a class="btn btn-success addvendor" style="margin-left:225px;">save</a>							
						<!--/form-->				
					</div>			
				</div>			
				<div class="col-md-12 category" style="width: 97%;border: 1px solid #449D44; border-radius: 5px;margin-left: 1%;margin-top: 1%;margin-bottom: 1%;">
					<div class="col-md-12" style="margin-top: 1%; border: 1px solid #449D44;border-radius: 5px;">
						<div class="col-md-6 category" style="margin-left: -15px; border-right: 1px solid #449D44;">
							<h3 class="text-center text-success">
								CATEGORY
							</h3>
							<div class="form-group form-inline" style="margin-top: 15px; margin-left: 3%;">
								<input class="form-control" type="search" role="search" placeholder="category">
								<button type="submit" class="btn btn-primary" style="display: inline;">
									Search
								</button>						
							</div>
							<div class="col-md-12" style="margin-left: -15px;width: 103%;margin-left: -2%">
								<table class="table table-bordered table-condensed" style="margin-top: 5%;">
									<thead>
										<tr>
											<th>
												#
											</th>
											<th>
												Name
											</th>
											
										</tr>
									</thead>
									<tbody>
										<?php $i=1; foreach($catagorys as $catagory):?>
										<tr>
											<td>
												<?=$i++;?>
											</td>
											<td data-catagory_id="<?= $catagory['catagory_id']?>" class="additems">
												<?php echo $catagory['catagory_name']?>
											</td>
											
										</tr>
										<?php endforeach;?>
										
										
								</table>								
							</div>											
						</div> 
						<div class="col-md-6" style="margin-left: 5px; border-right: 1px solid #449D44">
							<h3 class="text-center text-success">
								Product
							</h3>						
							<div class="form-group form-inline" style="margin-top: 15px; margin-left: 3%;">
								<input class="form-control" type="search" role="search" placeholder="items">
								<button type="submit" class="btn btn-primary" style="display: inline;">
									Search
								</button>						
							</div>
							<div class="col-md-12" style="width: 103%;margin-left: -2%">
								
								<table class="table table-bordered table-condensed items" style="margin-top: 5%;">
									
									
									</table>
							</div>
						</div>
						
						
					</div>
				<div class="col-md-12 addpurchase" style="margin-top: 1%; border: 1px solid #449D44; border-radius: 5px; margin-bottom: 1%;">
					<table class="table table-bordered table-condensed" style="margin-top: 1%;">
						<thead>
							<tr>
								<th class="text-center">
									SL
								</th>
								
								<th class="text-center">
									Product Code
								</th>							
								<th class="text-center">
									Name
								</th>
								<th class="text-center">
									Pack Size
								</th>
								<th class="text-center">
									Exp Date
								</th>							
								<th class="text-center">
									Quantity
								</th>
								<th class="text-center">
									Purchase Price 
								</th>
								
								<th class="text-center">
									Total Price 
								</th>
								<th class="text-center">
									Remove
								</th>
							</tr>
						</thead>
						<tbody class="addproduct">
							
							
						</tbody>
					</table>
				</div>
				<div class="col-md-12" style="text-align: right;width: 99%;">
					<div class="col-md-9" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 61%;margin-right: 15px; margin-top:10px; float:right;">
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-3 control-label">
									Invoice Number :
								</label>
								<label for="inputEmail3" class="control-label" style="margin-right: 320px;">
								<span style="color: red;"><?= $invoiceNumber?></span>
								</label>
						</div>	
						
					</div>
					<div class="col-md-9" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 61%;margin-right: 15px; margin-top:10px; float:right;">
					<div class="col-md-4">

						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-9 control-label">
									Sub Total :
								</label>
								<label for="inputEmail3" class="control-label">
								<span id="sub"></span><input type="hidden" id="sub_total" />  $$
								</label>
						</div>	
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-9 control-label">
									Invoice Total :
								</label>
								<label for="inputEmail3" class="control-label">
								<span id="invoice_total" style="color: green;"></span><input type="hidden" id="invoicetotal" /> $$
								</label>
						</div>	
					</div>
					<div class="col-md-5">
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label" style="float:left;">
									Discount :
								</label>
								<div class="col-sm-6">
								<input style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="discount" type="text" >
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label" style="float:left;">
									Transport Cost :
								</label>
								<div class="col-sm-6">
								<input style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="transport_cost" type="text" >
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label" style="float:left;">
									Other Cost :
								</label>
								<div class="col-sm-6">
								<input style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="other_cost" type="text" >
							</div>
						</div>	
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail6" class="col-sm-12 control-label"style="float:left;">
									Cash: <input type="checkbox" class="cash" id="cash"  /> Due: <input type="checkbox" class="due" id="due" />
								</label>
								
						</div>					
						<div class="form-group cash_pay" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label"style="float:left;">
									Cash Pay:
								</label>
								<div class="col-sm-6">
								<input style=" background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="cash_pay" type="text" >
							</div>
						</div>	
						<div class="form-group change_amount" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label"style="float:left;">
									Change Amount:
								</label>
								<div class="col-sm-6">
								<input readonly="readonly" style=" background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="change" type="text" >
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">								 
							<a  class="btn btn-success save_all" style="float: left; margin-right: 40%; margin-top:7%;">
								Save
							</a>
						</div>		
						
						<div class="form-group">								 
							<button type="submit" class="btn btn-success" style="float: left; margin-right: 40%;">
								Save & Print
							</button>
						</div>
						<div class="form-group">								 
							<button type="submit" class="btn btn-warning " style="float: left; margin-right: 40%;">
								Refresh 
							</button>
						</div>						
					</div>
				</div>
					
					
				</div>
				
				
			</div>
		</form>