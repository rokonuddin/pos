<?php $title=$this->settings_model->getconfig();?>
<style>
	
	@page {
   size: 6in 8in;
       margin: 0mm 5mm 27mm 10mm;
    }
	table{
		font-size:12px;
	}
</style>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

     <title><?php echo $title['company_name'];?></title>
    <link rel="shortcut icon"   href="<?= base_url('./companyLogo/15x15/'.$title['image'])?>"  type="image/x-icon" >

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="<?= base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/bootstrap-datetimepicker.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/style.css')?>" rel="stylesheet">
  </head>
  <body>
    <div class="container-fluid" style="width: 80%;mar">
	
		
		<!--div class="col-md-12">
			
		</div-->
		<div class="col-md-12" >
			<div class="col-md-12" style="width: 100%;">
				<div class="col-md-5">
					<p style="margin-left: 50px;margin-top: 10px;"><img width="50" src="<?= base_url('./companyLogo/15x15/'.$title['image'])?>" /></p>
					
				</div>
				<div class="col-md-7" style="margin-left: 100px;margin-top: -60">
					<p style="text-align: center; height: 8px;"><b style="font-size: 16px;"><?php echo $title['company_name'];?></b></p>
				<p style="text-align: center;height: 8px;"><b style="font-size: 16px;"><?php echo $title['address_line_1'];?></b></p>
				<p style="text-align: center;height: 8px;"><b style="font-size: 16px;">Phone No : <?php echo $title['phone'];?></b></p>
					
				</div>
				
				
				
			</div>
			<b>_________________________________________________________________</b>
            <div class="col-md-12" >
			<div class="col-md-5"  style="width:40%; margin-left: -20px;">
				<table>
					<tr>
						<th style="text-align: left;">Invoice Date</th>
						<th>:</th>
						<th><?php echo $invoiceInfo['invoice_date'];?></th>
					</tr>
					<tr>
						<th style="text-align: left;">InvoiceType</th>
						<th>:</th>
						<th><?php echo $invoiceInfo['pay_status'];?></th>
					</tr>
					<tr>
						<th style="text-align: left;">Invoice ID</th>
						<th>:</th>
						<th><?php echo $invoiceInfo['invoice_number'];?></th>
					</tr>
				</table>
				
				<!--p style="text-align: center;height: 8px;"><b>Customer Name:</b><?php echo $invoiceInfo['customer_name'];?></p>
				<p style="text-align: center;height: 8px;"><b>Address      :</b><?php echo $invoiceInfo['address'];?></p>
				<p style="text-align: center;height: 8px;"><b>Phone No     : </b><?php echo $invoiceInfo['phone'];?></p-->
			</div>
			<div class="col-md-7" style="width:60%; margin-left: 170px;margin-top: -45px;" >
				<table>
					<tr>
						<th style="text-align: right;">Customer Name</th>
						<th>:</th>
						<th style="text-align: right;"><?php echo $invoiceInfo['customer_name'];?></th>
					</tr>
					<tr>
						<th style="text-align: right;">Address</th>
						<th>:</th>
						<th style="text-align: right;"><?php echo $invoiceInfo['address'];?></th>
					</tr>
					<tr>
						<th style="text-align: right;">Phone No</th>
						<th>:</th>
						<th style="text-align: right;"><?php echo $invoiceInfo['phone'];?></th>
					</tr>
				</table>
				<!--p style="text-align: center;height: 8px;"><b>Invoice Date:</b><?php echo $invoiceInfo['invoice_date'];?></p>
				<p style="text-align: center;height: 8px;"><b>InvoiceType :</b><?php echo $invoiceInfo['pay_status'];?></p>
				<p style="text-align: center;height: 8px;"><b>Invoice ID  :</b><?php echo $invoiceInfo['invoice_number'];?></p-->
			</div>
			</div>
			<br />
			<b>_________________________________________________________________</b>
            <div class="col-md-12"  style="width: 100%;">
                    

                        
                        <table class="table table-bordered table-condensed" style="margin-top: 1%;">
                            <thead>
                                <tr>
                                    <th>
                                        SL
                                    </th>
                                    <th>
                                        Code
                                    </th>
                                    <th>
                                    	Product Name
                                    </th>
                                    <th>
                                        Pack Size
                                    </th>
                                    <th>
                                       Qty
                                    </th>
                                    
                                    <th>
                                        Price
                                    </th>
                                    <th>
                                        Net Value
                                    </th>
                                    
                                    
                                </tr>
                            </thead>
                            <tbody>
                            	<?php $i=1; foreach($sales as $sale):?>
                                <tr>
                                    <th>
                                       <?= $i++;?>
                                    </th>
                                    <th>
                                      <?php echo $sale['product_code']?>
                                    </th>
                                    <th>
                                       <?php echo $sale['product_name']?>
                                    </th>
                                    <th>
                                        <?php echo $sale['pack_size']?>
                                    </th>
                                    
                                    <th>
                                        <?php echo $sale['quantity']?>
                                    </th>
                                    <th>
                                      <?php echo $sale['unit_price']?>
                                    </th>
                                    <th>
                                        <?php echo $sale['total_price']?>
                                    </th>
                                    
                                </tr>
                                <?php endforeach;?>
                                
                                 <tr>
                                    <th  colspan="6" >
                                       <b style="float: right">Sub Total:</b>
                                    </th>
                                    <th>
                                        <?= $invoiceInfo['sub_total']?>
                                    </th>
                                    
                                   
                                </tr>
                            </tbody>
                        </table>
                   
                </div>
                <div class="col-md-12" style="margin-top: -22px;" style="width: 100%;">
                    
                       <div class="col-md-8" style="width: 50%;">
                       	
                       	</div>
                        <div class="col-md-4" style="width: 45%;margin-left: 285px;">
                       	<table class="table table-bordered table-condensed" style="margin-top: 1%;">
                            
                            <tbody>
                                <tr>
                                    <th>
                                       Discount
                                    </th>
                                    <th>
                                       <?= $invoiceInfo['discount']?>
                                    </th>
                                   
                                    
                                </tr>
                                <tr >
                                    <th>
                                     Invoice Total
                                    </th>
                                    <th>
                                        <?php $to=0; echo $invoiceInfo['invoice_total']?>
                                    </th>
                                    
                                    
                                </tr>
                                <?php if($advance['advance']==0){
                                	$total_due=$invoiceInfo['total_due']+$invoiceInfo['invoice_total'];
                                	$paid_total=$paid['amount'];
									
                                	?>
                                	
                                	
                                	<tr>
                                    <th>
                                        Previous Due
                                    </th>
                                    <th>
                                        <?php echo $invoiceInfo['total_due']?>
                                    </th>
                                    
                                    
                                </tr>
                                 
                                	<tr>
                                    <th>
                                        Paid
                                    </th>
                                    <th>
                                        <?php echo $paid['amount']?>
                                    </th>
                                    
                                   
                                </tr>
                                <?php if($total_due>$paid_total){?>
                                	<tr>
                                    <th>
                                       Due:
                                    </th>
                                    <th>
                                        <?php echo $total_due-$paid_total?>
                                    </th>
                                    
                                   
                                </tr>
                                	<?php }else{?>
                                		
                                		<tr>
                                    <th>
                                       Advance:
                                    </th>
                                    <th>
                                        <?php echo $paid_total-$total_due;?>
                                    </th>
                                    
                                   
                                </tr>
                                		
                                		
                                		<?php }?>
                                	<?php }else{
                                		
                                		
                                		
                                		
                                		?>
                                		
                                <?php if($invoiceInfo['total_due']!=0){
                                	
                                	if($paid['amount']>($invoiceInfo['total_due']+$invoiceInfo['invoice_total']))
									{
										$total_due=$paid['amount']-($invoiceInfo['total_due']+$invoiceInfo['invoice_total']);
									}else{
										$total_due=($invoiceInfo['total_due']+$invoiceInfo['invoice_total'])-$paid['amount'];
									}
                                	
                                	
                                	?>	
                                <tr>
                                    <th>
                                        Previous due
                                    </th>
                                    <th>
                                        <?php echo $invoiceInfo['total_due']?>
                                    </th>
                                    
                                    
                                </tr>
                                <?php }else{?>
                                	<tr>
                                    <th>
                                        Previous Advance
                                    </th>
                                    <th>
                                        <?php echo $advance['advance']?>
                                    </th>
                                    
                                    
                                </tr>
                                	<?php }?>
                                <tr>
                                    <th>
                                        Paid
                                    </th>
                                    <th>
                                        <?php echo $paid['amount']?>
                                    </th>
                                    
                                   
                                </tr>
                                
                                <?php if($paid['amount']>($invoiceInfo['total_due']+$invoiceInfo['invoice_total'])){?>
                                	<tr>
                                    <th>
                                       Advance:
                                    </th>
                                    <th>
                                        <?php echo $paid['amount']-($invoiceInfo['total_due']+$invoiceInfo['invoice_total'])?>
                                    </th>
                                    
                                   
                                </tr>
                                	<?php }else{?>
                                		
                                		<tr>
                                    <th>
                                       Due:
                                    </th>
                                    <th>
                                        <?php echo ($invoiceInfo['total_due']+$invoiceInfo['invoice_total'])-$paid['amount'];?>
                                    </th>
                                    
                                   
                                </tr>
                                		
                                		
                                		<?php }?>
                                		
                                		
                                		
                                		<?php }?>
                                
                                
                                 
                                
                            </tbody>
                        </table>
                       	</div>
                        
                   
                </div>
                <div class="col-md-12" style="width: 100%;">
                	<div class="col-md-6" style="width:50%; margin-left: -10px;">
                		
                		<b>Authorized By</b>
                		
                		 </div>
                	<div class="col-md-6" style="width:50%; margin-left: 250px;margin-top: -20px;">
                		
                		<b>Customer Signature</b>
                		
                		 </div>
                </div>
           	
			</div>	
		
	<div class="col-md-1">
	</div>
</div>

    	<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
<script src="<?= base_url('dist/js/formValidation.js')?>"></script>
 <script src="<?= base_url('dist/js/framework/bootstrap.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>  	
	<script type="text/javascript">
             $(function () {
                $('#ExpDate').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('#datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
     </script>
  </body>
</html>