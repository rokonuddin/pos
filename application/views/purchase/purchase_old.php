<?php 
$user = $this->ion_auth->user()->row();
   		$user_id = $user->id;
        $year=date('Y');
        $month=date('m');
        $checkInvoice=$this->db->select('invoice_number')->where('created_by',$user->id)->order_by('purchase_id','desc')->get('purchase')->row_array();
        
        if(!empty($checkInvoice))
        {
                     $invoicearray=(explode("-", $checkInvoice['invoice_number']));
                       $newstring=intval($invoicearray[2]);
                     $newstring=$newstring+1;
                      $invoiceNumber=date('Y').'-'.$user->id.'-'.$newstring;
        }else{
            $invoiceNumber=date('Y').'-'.$user->id.'-'.'1';
        }


?>
<style>
	
	.category{
		display:none;
	}
	.vendorform{
		display:none;
	}
	.addpurchase{
		display:none;
	}
</style>
		<div class="col-md-12 new_invoice" style="margin-top: 5%;border: 1px solid #449D44; border-radius: 5px;width: 98%;margin-left: 1%; margin-bottom: 1%;">
			<form class="form-horizontal" role="form" id="all_purchase">			
			<div class="col-md-12" style="margin-bottom: 1%; text-align: right; margin-top: 1%;width: 99%;">
				<a class="btn btn-success addvendor" style="">New</a><a class="btn btn-danger" style="margin-left: 1%;">Close</a>
			</div>
				<div class="col-md-12" style="border: 1px solid #449D44; border-radius: 5px;width: 97%;margin-left: 15px;">
					<div class="col-md-6" style="border-right: 1px solid #449D44;">
						<div class="form-group form-inline search_phone" style="margin-top: 15px; margin-left: 3%;">
							<input class="form-control" id="search_phone" type="search" role="search" placeholder="Phone">
							<button type="submit" class="btn btn-primary" style="display: inline;">
									Search
								</button>						
						</div>
						<div class="col-md-12">
							<table class="table table-bordered table-condensed">
								<thead>
									<tr>
										<th>
											#
										</th>
										<th>
										 Manufactrue Name
										</th>
										
										<th>
											Phone Number
										</th>
										<th>
											Advance
										</th>
										<th>
											Due
										</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody class="menufacture_result">
									<?php $i=1; foreach($menufactures as $menufacture):?>
									<tr>
										<td>
											<?php echo $i++;?>
										</td>
										<td>
											<?php echo  $menufacture['name'];?>
										</td>
										<td>
											<?php echo  $menufacture['phone'];?>
										</td>
										<td>
											<?php echo  $menufacture['advance'];?>
										</td>
										<td>
											<?php echo  $menufacture['total_due'];?>
										</td>
										<td> <a data-menufacture_id="<?=$menufacture['menufacture_id']?>" data-address="<?=$menufacture['address']?>"  data-phone="<?=$menufacture['phone']?>" data-name="<?=$menufacture['name']?>" class="btn btn-success purchase" >Select</a></td>
									</tr>
									<?php endforeach;?>
									
									
								</tbody>
							</table>
						</div>					
					</div>					
					<div class="col-md-6 vendorform" >
						<!--form class="form-horizontal" role="form"-->
						<input type="hidden" name="menufacture_id" id="menufacture_id"  />
							<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-4 control-label">
								Manufacture Name:
								</label>
								<div class="col-sm-6">
									<input id="name" name="name" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="name" type="text" placeholder="Name">
								</div>
							</div>		
							<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-4 control-label">
								 	Phone :
								</label>
								<div class="col-sm-6">
									<input id="phone" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="phone" type="text" placeholder="Phone">
								</div>
							</div>	
							<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-4 control-label">
									Address :
								</label>
								<div class="col-sm-6">
									<input id="address" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="address" type="text" placeholder="Address">
								</div>
							</div>	
								
                          							
						<!--/form-->				
					</div>			
				</div>			
				<div class="col-md-12 category" style="width: 97%;border: 1px solid #449D44; border-radius: 5px;margin-left: 1%;margin-top: 1%;margin-bottom: 1%;">
					<div class="col-md-12" style="margin-top: 1%; border: 1px solid #449D44;border-radius: 5px;">
						<div class="col-md-6 category" style="margin-left: -15px; border-right: 1px solid #449D44;">
							<h3 class="text-center text-success">
								CATEGORY
							</h3>
							<div class="form-group form-inline" style="margin-top: 15px; margin-left: 3%;">
								<input class="form-control search_category"  id="search_category" type="search" role="search" placeholder="category">
								<button class="btn btn-primary" style="display: inline;">
									Search
								</button>						
							</div>
							<div class="col-md-12" style="margin-left: -15px;width: 103%;margin-left: -2%">
								<table class="table table-bordered table-condensed" style="margin-top: 5%;">
									<thead>
										<tr>
											<th>
												#
											</th>
											<th>
												Name
											</th>
											
										</tr>
									</thead>
									<tbody class="category_table">
										<?php $i=1; foreach($catagorys as $catagory):?>
										<tr>
											<td>
												<?=$i++;?>
											</td>
											<td data-catagory_id="<?= $catagory['catagory_id']?>" class="additems">
												<?php echo $catagory['catagory_name']?>
											</td>
											
										</tr>
										<?php endforeach;?>
										
										
								</table>								
							</div>											
						</div> 
						<div class="col-md-6" style="margin-left: 5px; border-right: 1px solid #449D44">
							<h3 class="text-center text-success">
								Product
							</h3>						
							<div class="form-group form-inline" style="margin-top: 15px; margin-left: 3%;">
								<input type="hidden"  id="catagoryid" />
								<input class="form-control search_item" id="search_item" type="search" role="search" placeholder="code">
								<button  class="btn btn-primary" style="display: inline;">
									Search
								</button>						
							</div>
							<div class="col-md-12" style="width: 103%;margin-left: -2%">
								
								<table class="table table-bordered table-condensed items" style="margin-top: 5%;">
									
									
									</table>
							</div>
						</div>
						
						
					</div>
				<div class="col-md-12 addpurchase" style="margin-top: 1%; border: 1px solid #449D44; border-radius: 5px; margin-bottom: 1%;">
					<table class="table table-bordered table-condensed" style="margin-top: 1%;">
						<thead>
							<tr>
								<th class="text-center">
									SL
								</th>
								
								<th class="text-center">
									Product Code
								</th>							
								<th class="text-center">
									Name
								</th>
								<th class="text-center">
									Pack Size
								</th>
								<th class="text-center">
									Exp Date
								</th>							
								<th class="text-center">
									Quantity
								</th>
								<th class="text-center">
									Purchase Price 
								</th>
								
								<th class="text-center">
									Total Price 
								</th>
								<th class="text-center">
									Remove
								</th>
							</tr>
						</thead>
						<tbody class="addproduct">
							
							
						</tbody>
					</table>
				</div>
				<div class="col-md-12" style="text-align: right;width: 99%;">
					<div class="col-md-9" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 61%;margin-right: 15px; margin-top:10px; float:right;">
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-3 control-label">
									Invoice Number :
								</label>
								<label for="inputEmail3" class="control-label" style="margin-right: 320px;">
								<span style="color: red;"><?= $invoiceNumber?></span>
								</label>
						</div>	
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Previous due Payment :
							</label>
							<div class="col-sm-1">
								<input type="checkbox" id="is_due" value="1" />
							</div>
						   </div>
						
					</div>
					<div class="col-md-9" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 61%;margin-right: 15px; margin-top:10px; float:right;">
					<div class="col-md-4">

						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-9 control-label">
									Sub Total :
								</label>
								<label for="inputEmail3" class="control-label">
								<span id="sub"></span><input type="hidden" id="sub_total" />  $$
								</label>
						</div>	
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-9 control-label">
									Invoice Total :
								</label>
								<label for="inputEmail3" class="control-label">
								<span id="invoice_total" style="color: green;"></span><input type="hidden" id="invoicetotal" /> $$
								</label>
						</div>	
					</div>
					<div class="col-md-5">
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label" style="float:left;">
									Discount :
								</label>
								<div class="col-sm-6">
								<input style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="discount" type="text" >
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label" style="float:left;">
									Transport Cost :
								</label>
								<div class="col-sm-6">
								<input style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="transport_cost" type="text" >
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label" style="float:left;">
									Other Cost :
								</label>
								<div class="col-sm-6">
								<input style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="other_cost" type="text" >
							</div>
						</div>	
						<!--div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail6" class="col-sm-12 control-label"style="float:left;">
									Cash: <input type="checkbox" class="cash" id="cash"  /> Due: <input type="checkbox" class="due" id="due" />
								</label>
								
						</div-->					
						<div class="form-group cash_pay" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label"style="float:left;">
									Cash Pay:
								</label>
								<div class="col-sm-6">
								<input style=" background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="cash_pay" type="text" >
							</div>
						</div>	
						
					</div>
					<div class="col-md-3">
						<div class="form-group">								 
							<button  class="btn btn-success" style="float: left; margin-right: 40%; margin-top:7%;">
								Save
							</button>
						</div>		
						
						<div class="form-group">								 
							<button type="submit" class="btn btn-success" style="float: left; margin-right: 40%;">
								Save & Print
							</button>
						</div>
						<div class="form-group">								 
							<button type="submit" class="btn btn-warning " style="float: left; margin-right: 40%;">
								Refresh 
							</button>
						</div>						
					</div>
				</div>
					
					
				</div>
				
				
			</div>
		</form>
		<div class="col-md-1">
		</div>
	</div>
		
		<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
<script src="<?= base_url('dist/js/formValidation.js')?>"></script>
 <script src="<?= base_url('dist/js/framework/bootstrap.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>autocom/jquery.auto-complete.css">
<script src="<?php echo base_url(); ?>autocom/jquery.auto-complete.js"></script>
<script type="text/javascript">
            $(function () {
                $('.ExpDate').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('.datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
				//$('.change_amount').hide();
				//$('.cash_pay').hide();
				
				
				
				$('body').on('input','#search_phone',function(){
					
				var	search_phone=$('#search_phone').val();
				
				   var url = "<?= site_url('purchase/search_phone');?>";
				   $.ajax({
						type:'post',
						data:{'search_phone':search_phone},
						url:url,
						success: function(result){							
							$('.menufacture_result').html(result);
							
						}
					});
					
				});
				
				$('body').on('input','.search_category',function(){
					
				var	category=$('#search_category').val();
				   var url = "<?= site_url('sales/search_category');?>";
				   $.ajax({
						type:'post',
						data:{'category':category},
						url:url,
						success: function(result){							
							$('.category_table').html(result);
							
						}
					});
					
				});
				
				
				$('body').on('input','#search_item',function(){
				var	catagoryid=$('#catagoryid').val();//alert(catagoryid);
				var	item=$('#search_item').val();//alert(item);return false;
				   var url = "<?= site_url('sales/search_item');?>";
				   $.ajax({
						type:'post',
						data:{'item':item,'catagoryid':catagoryid},
						url:url,
						success: function(result){							
							$('.item_table').html(result);
							
						}
					});
					
				});
				
				$('body').on('click','.addvendor',function(){
					
					$('.vendorform').show();
					$('.category').show();
					$('#name').val('');
					$('#phone').val('');
					$('#address').val('');
					$('#menufacture_id').val('');
					$('#name').attr('readonly', false);
					$('#address').attr('readonly', false);
					$('#phone').attr('readonly', false);
				});
				
				$('#all_purchase').formValidation({
				message: 'This value is not valid',
		        icon: {
		            valid: 'glyphicon glyphicon-ok',
		            invalid: 'glyphicon glyphicon-remove',
		            validating: 'glyphicon glyphicon-refresh'
		        },
		        fields: {
		        	name: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The Name field is required'
		                    }
		                }
		        	}	
		        }
		        }).on('success.form.fv', function(e) {
		        	
		        	save();
		        	 
		        	
		        });
				
				
				
				$('body').on('click','.purchase',function(){
					
					var name=$(this).data('name');
					var phone=$(this).data('phone');
					var address=$(this).data('address');
					var menufacture_id=$(this).data('menufacture_id');
					$('#name').val(name);
					$('#phone').val(phone);
					$('#address').val(address);
					$('#menufacture_id').val(menufacture_id);
					$('#name').attr('readonly', true);
					$('#address').attr('readonly', true);
					$('#phone').attr('readonly', true);
					$('.vendorform').show();
					$('.category').show();
					
					
				   var url = "<?= site_url('purchase/search_category_menufacture');?>";
				   $.ajax({
						type:'post',
						data:{'menufacture_id':menufacture_id},
						url:url,
						success: function(result){							
							$('.category_table').html(result);
							
						}
					});
					
					
				});
				$('body').on('click','.additems',function(){
					var catagory_id=$(this).data('catagory_id');
					$('#catagoryid').val(catagory_id);
					$.ajax({
						type:'post',
						data:{'catagory_id':catagory_id},
						url:'<?php echo site_url('purchase/items')?>',
						success: function(result){
							
							$('.items').html(result);
							
						}
					});
				});
				$('body').on('click','.addlist',function(){
					$('.addpurchase').show();
					var product_id=$(this).data('product_id');
					$.ajax({
						type:'post',
						data:{'product_id':product_id},
						url:'<?php echo site_url('purchase/addproduct')?>',
						success: function(result){
							
							//$('.addproduct').html(result);
							
						},
						complete: function(){
						ajax_search_items_for_price(product_id);
					}
					});
				});
				
				$('body').on('click','.category_delete',function(e){
					e.preventDefault();
					var rowTotal=0;
					$(this).closest('tr').remove();
					 $(".row_total").each(function() {
						    rowTotal += parseInt($(this).val());
						   $('#sub').html(rowTotal);
			    		$('#sub_total').val(rowTotal);
			    		$('#invoice_total').html(rowTotal);
			            $('#invoicetotal').val(rowTotal);
					});
					count--;
			    });
			    
			    
			    
			    
			    $('body').on('change','#unit', function(e){
				e.preventDefault();
				rowTotal=0;
				//var unitquantity = $(this).data('items_id');
				var unitquantity = $(this).data('unit');
				
				//var row_id = $(this).data('row_id');
				var unit = $(this).val();
				
				var rate = $(".amount_"+unitquantity).val();
				
				var total = unit*rate;
				$("#items_rate_"+unitquantity).html(rate);
			    $("#unit_total_price_"+unitquantity).html(total);
			    $("#unit_value_"+unitquantity).val(total);
			    var s = $("#unit_total_price_"+unitquantity).html();
			    var totalUnitAmount = $("#unit_value_"+unitquantity).val();
			      $(".row_total").each(function() {
					    rowTotal += parseInt($(this).val());
					    $('#sub').html(rowTotal);
			    		$('#sub_total').val(rowTotal);
			    		$('#invoice_total').html(rowTotal);
			            $('#invoicetotal').val(rowTotal);
					});
				
			});
			
			
			$('body').on('change','#amount', function(e){
				e.preventDefault();
				rowTotal=0;
				//var unitquantity = $(this).data('items_id');
				var unitquantity = $(this).data('unit');
				
				//var row_id = $(this).data('row_id');
				var unit = $(this).val();
				
				var rate = $(".unit_"+unitquantity).val();
				alert(rate);
				var total = unit*rate;
				$("#items_rate_"+unitquantity).html(rate);
			    $("#unit_total_price_"+unitquantity).html(total);
			    $("#unit_value_"+unitquantity).val(total);
			    var s = $("#unit_total_price_"+unitquantity).html();
			    var totalUnitAmount = $("#unit_value_"+unitquantity).val();
			      $(".row_total").each(function() {
					    rowTotal += parseInt($(this).val());
					    $('#sub').html(rowTotal);
			    		$('#sub_total').val(rowTotal);
			    		$('#invoice_total').html(rowTotal);
			            $('#invoicetotal').val(rowTotal);
					});
				
			});
			
			$('body').on('change','#discount', function(e){
			  
			   var discount=$('#discount').val();
			   var subtotal= $('#sub_total').val();
			   
			   
			    var transport_cost=$('#transport_cost').val();
			   var other_cost= $('#other_cost').val();
			    var total=+subtotal+ +transport_cost+ +other_cost;
			    var total=total-discount
			   $('#invoice_total').html(total);
			   $('#invoicetotal').val(total);
			});
			$('body').on('change','#transport_cost', function(e){
			
			    var discount=$('#discount').val();
			   var subtotal= $('#sub_total').val();
			    var transport_cost=$('#transport_cost').val();
			   var other_cost= $('#other_cost').val();
			   var total=+subtotal+ +transport_cost+ +other_cost;
			    var total=total-discount
			   $('#invoice_total').html(total);
			   $('#invoicetotal').val(total);
			});
			$('body').on('change','#other_cost', function(e){
			
			   var discount=$('#discount').val();
			   var subtotal= $('#sub_total').val();
			    var transport_cost=$('#transport_cost').val();
			   var other_cost= $('#other_cost').val();
			    var total=+subtotal+ +transport_cost+ +other_cost;
			    var total=total-discount
			   $('#invoice_total').html(total);
			   $('#invoicetotal').val(total);
			
			});
			$('body').on('change','#cash_pay', function(e){
			
			   var total=$('#invoicetotal').val();
			   var cash_pay= $('#cash_pay').val();
			   
			   
			   
			   
			
			});
			    
			});
			function save()
			{
				
		        	
		        	$('.save_all').hide();
		        	ordersObj = [];
				   $(".tr_table").each(function() {
				    var rowid = $(this).attr("id");
				    
				    var itemid=$('input[data-itemsid="'+rowid+'"]').val();
				   
				    var exp_date=$('input[data-exp_date="'+rowid+'"]').val();
				     var unit=$('input[data-unit="'+rowid+'"]').val();
				     
				    var unit_price=$('input[data-unit_price="'+rowid+'"]').val();
				    
				   
				    var row_total=$('input[data-row_total="'+rowid+'"]').val();
				   
				    
							
					
                	
                	item = {}
       				item ["itemid"] = itemid;
        			item ["unit"] = unit;
        			item ["unit_price"] = unit_price;
        			item ["exp_date"] = exp_date;
        			item ["row_total"] = row_total;
        			
        			
                	ordersObj.push(item);
                					  
				   });
					orders = JSON.stringify(ordersObj);
					
					var menufacture_id = $('#menufacture_id').val();
					
				    var name = $('#name').val();
				    var address = $('#address').val();
				    var phone = $('#phone').val();
				  //  var email = $('#email').val();
				//	var invoice_number = $('#invoice_number').val();
				//	var receive_date = $('#receive_date').val();
				//	var deliver_date = $('#deliver_date').val();
				 var is_dues=$('#is_due').prop('checked');
					if(is_dues==true)
					{
					var	is_due=1;
					}else{
					var	is_due=0;
					}
				 
					var subtotal = $('#sub_total').val();
					var invoicetotal = $('#invoicetotal').val();
					var discount = $('#discount').val();
					var transport_cost = $('#transport_cost').val();
					var other_cost = $('#other_cost').val();
					var cash_pay = $('#cash_pay').val();
					var url = "<?= site_url('purchase/save_invoice');?>";
					$.ajax({
					type : "post",
					data : {'name':name,'address':address,'phone':phone,'is_due':is_due, 'menufacture_id': menufacture_id, 
					'subtotal' : subtotal, 'discount' : discount, 'transport_cost':transport_cost,
					'invoicetotal' : invoicetotal,'cash_pay' : cash_pay,  'orders':orders,'other_cost': other_cost,'cash_pay': cash_pay},
					async : false,
					dataType : "json",
					url : url,
				    success : function(result){
				    	      
				    	      
				    	      
				    	      $('#due').prop('checked', false)
				    	      $('#cash').prop('checked', false)
								 
									 $('#sub_total').val('');
									$('#invoicetotal').val('');
									$('#discount').val('');
									$('#transport_cost').val('');
									$('#other_cost').val('');
									 $('#cash_pay').val('');
				    	
				    	$('.category').hide();
				    	
				    	
				        $('.vendorform').hide();
				        
				        
				         window.location.assign('purchase/purchasePrint/'+result);
				    }
					
					});
		        	
		        	
			}
			function ajax_search_items_for_price(product_id)
			{
				
					
					$.ajax({
						type:'post',
						data:{'product_id':product_id},
						url:'<?php echo site_url('purchase/addproduct')?>',
						success: function(result){
							
							$('.addproduct').append(result);
							
						},
						complete: function(){
						$('.addpurchase').show();
					}
					});
			
			}
     </script>

   	
	