<div class="col-md-12" style="margin-top: 5%;width: 95%;margin-left: 1%; margin-bottom: 1%;">
			<div class="col-md-12">
				<h3 class="text-center text-success" style="margin-top: -1%;background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;height: 50px;">
						 <b style="margin-top: 50px;">Payment</b> 
					</h3>
			</div>
			<div class="col-md-12">	
				<div class="col-md-12" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;">
					<h3 class="text-center text-success">
						Customer
					</h3>
					<form class="form-horizontal" method="post" action="<?= site_url('purchase/menufacturePaid')?>"  role="form">
					
						
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Menufacture Name:
							</label>
							<div class="col-sm-4">
								<select class="form-control menufacture" name="menufacture_id">
								<option>--select--</option>
								<?php foreach($menufactures as $menufacture):?>
								<option value="<?= $menufacture['menufacture_id']?>"><?php echo $menufacture['name']?></option>
								<?php endforeach;?>
								
								</select>
							</div>
						</div>
						
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Due amount:
							</label>
							<div class="col-sm-4">
								<input readonly="readonly" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="due" type="text" name="due" placeholder="due">
							</div>
						</div>	
						
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Pay amount:
							</label>
							<div class="col-sm-4">
								<input style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" name="amount" placeholder="amount">
							</div>
						</div>		
						
						
						<div class="form-group">								 
							<button type="submit" class="btn btn-success" style="float: right; margin-right: 40%;">
								Save
							</button>
						</div>	
					</form>						
				</div>			
				
			</div>	
								
				
			</div>
			


<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
<script src="<?= base_url('dist/js/formValidation.js')?>"></script>
 <script src="<?= base_url('dist/js/framework/bootstrap.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>

<script type="text/javascript">
            $(function () {
                $('.datetimepicker1').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('.datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
			
			
			$('body').on('change','.menufacture', function(){
				
				var menufacture_id=$('.menufacture').val();
				
				
				
				
				$.ajax({
						type:'post',
						data:{'menufacture_id':menufacture_id},
						url:'<?php echo site_url('purchase/ajax_menufactureDue')?>',
						success: function(result){
							
							$('#due').val(result);
							
						},
						
					});
				
			  });
				
				
			});
</script>
