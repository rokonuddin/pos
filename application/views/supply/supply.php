
<script src="<?= base_url('assets/js/angular.min.js');?>"></script>
		<div class="col-md-12 new_invoice" style="margin-top: 5%;border: 1px solid #449D44; border-radius: 5px;width: 98%;margin-left: 1%; margin-bottom: 1%;">
			<form class="form-horizontal" role="form" id="all_purchase">			
			<div class="col-md-12" style="margin-bottom: 1%; text-align: right; margin-top: 1%;width: 99%;">
				<a class="btn btn-success addvendor" style="">New</a><a class="btn btn-danger" style="margin-left: 1%;">Close</a>
			</div>
						
				<div class="col-md-12 category" style="width: 97%;border: 1px solid #449D44; border-radius: 5px;margin-left: 1%;margin-top: 1%;margin-bottom: 1%;">
					<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-6 control-label">
								Customer Name:
							</label>
							<div class="col-sm-6">
                                <select class="form-control" name="parent_id" id="parent_id">
                                	
                                    
                                </select>
								
							</div>
						</div>	
					
				<div class="col-md-12 addpurchase" style="margin-top: 1%; border: 1px solid #449D44; border-radius: 5px; margin-bottom: 1%;">
					<table class="table table-bordered table-condensed" style="margin-top: 1%;">
						<thead>
							<tr>
								<th class="text-center">
									SL
								</th>
								
								<th class="text-center">
									Product Code
								</th>							
								<th class="text-center">
									Name
								</th>
								<th class="text-center">
									Pack Size
								</th>
								<th class="text-center">
									Exp Date
								</th>							
								<th class="text-center">
									Quantity
								</th>
								<th class="text-center">
									Purchase Price 
								</th>
								
								<th class="text-center">
									Total Price 
								</th>
								<th class="text-center">
									Remove
								</th>
							</tr>
						</thead>
						<tbody class="addproduct">
							
							
						</tbody>
					</table>
				</div>
				<div class="col-md-12" style="text-align: right;width: 99%;">
					<div class="col-md-9" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 61%;margin-right: 15px; margin-top:10px; float:right;">
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-3 control-label">
									Invoice Number :
								</label>
								<label for="inputEmail3" class="control-label" style="margin-right: 320px;">
								<span style="color: red;"></span>
								</label>
						</div>	
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Previous due Payment :
							</label>
							<div class="col-sm-1">
								<input type="checkbox" id="is_due" value="1" />
							</div>
						   </div>
						
					</div>
					<div class="col-md-9" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 61%;margin-right: 15px; margin-top:10px; float:right;">
					<div class="col-md-4">

						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-9 control-label">
									Sub Total :
								</label>
								<label for="inputEmail3" class="control-label">
								<span id="sub"></span><input type="hidden" id="sub_total" />  $$
								</label>
						</div>	
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-9 control-label">
									Invoice Total :
								</label>
								<label for="inputEmail3" class="control-label">
								<span id="invoice_total" style="color: green;"></span><input type="hidden" id="invoicetotal" /> $$
								</label>
						</div>	
					</div>
					<div class="col-md-5">
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label" style="float:left;">
									Discount :
								</label>
								<div class="col-sm-6">
								<input style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="discount" type="text" >
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label" style="float:left;">
									Transport Cost :
								</label>
								<div class="col-sm-6">
								<input style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="transport_cost" type="text" >
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label" style="float:left;">
									Other Cost :
								</label>
								<div class="col-sm-6">
								<input style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="other_cost" type="text" >
							</div>
						</div>	
						<!--div class="form-group" style="margin-top: 15px;">							 
								<label for="inputEmail6" class="col-sm-12 control-label"style="float:left;">
									Cash: <input type="checkbox" class="cash" id="cash"  /> Due: <input type="checkbox" class="due" id="due" />
								</label>
								
						</div-->					
						<div class="form-group cash_pay" style="margin-top: 15px;">							 
								<label for="inputEmail3" class="col-sm-6 control-label"style="float:left;">
									Cash Pay:
								</label>
								<div class="col-sm-6">
								<input style=" background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="cash_pay" type="text" >
							</div>
						</div>	
						
					</div>
					<div class="col-md-3">
						<div class="form-group">								 
							<button  class="btn btn-success" style="float: left; margin-right: 40%; margin-top:7%;">
								Save
							</button>
						</div>		
						
						<div class="form-group">								 
							<button type="submit" class="btn btn-success" style="float: left; margin-right: 40%;">
								Save & Print
							</button>
						</div>
						<div class="form-group">								 
							<button type="submit" class="btn btn-warning " style="float: left; margin-right: 40%;">
								Refresh 
							</button>
						</div>						
					</div>
				</div>
					
					
				</div>
				
				
			</div>
		</form>
		<div class="col-md-1">
		</div>
	</div>
		
		

   	
	