<thead>
						
							<tr>
								<th>
									#
								</th>
								<th>
									Customer Name
								</th>
								<th>
									Customer Code
								</th>
								<th>
									Address
								</th>
								<th>
									Phone
								</th>
								<th>
									Balance
								</th>
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach($customers as $customer):?>
							<tr>
								<td>
									<?= $i++;?>
								</td>
								<td>
									<?=$customer['customer_name']?>
								</td>
								<td>
									<?=$customer['customer_code']?>
								</td>
								<td>
								<?=$customer['address']?>
									
								</td>
								<td>
								 <?=$customer['phone']?>
								</td>
								<td>
								 <?=$customer['balance']?>
								</td>
								<td>
									<a href="#" data-customer_id="<?php echo $customer['customer_id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a>
									<a href="#" data-customer_id="<?php echo $customer['customer_id']?>" data-balance="<?php echo $customer['balance']?>" data-phone="<?php echo $customer['phone']?>" data-address="<?php echo $customer['address']?>"  data-customer_name="<?php echo $customer['customer_name']?>" data-customer_code="<?php echo $customer['customer_code']?>" class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
							
						</tbody>