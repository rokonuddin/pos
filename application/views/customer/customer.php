<style>
	.alert-success{
		display: none;
	}
	.alert-default{
		display: none;
	}
	.alert-danger{
		display: none;
	}
	
</style>
<?php

if(!empty($number['customer_code']))
{
	$customernumber=$number['customer_code']+1;
	
}else{
	$customernumber='100001';
}


?>
<div class="col-md-12" style="margin-top: 5%;width: 95%;margin-left: 1%; margin-bottom: 1%;">
			<div class="bs-example">
				    <!-- Modal HTML -->
				   
				    <div id="myModal1" class="modal fade">
				        <div class="modal-dialog">
				            <div class="modal-content" style="width: 70%;">
				            	<div class="modal-header">
				                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                    <h3 class="modal-title title" style="text-align: center;color:green;">Customer</h3>
				                </div>
				                <div class="modal-header">
				                	<input id="customerid" type="hidden" />
				                	<h4 class="modal-title title" style="text-align: center;color:green;">Are You Sure delete this?</h4><br />
				                 <a  style="margin-left: 130px;" class="btn btn-danger yes" data-dismiss="modal">Yes</a>
				                 <a  style="" class="btn btn-success cancel" data-dismiss="modal">Cancel</a>
				                </div>
				                
				         
				            </div>
				        </div>
				    </div>
				</div>
			
			
			
			<div class="col-md-12">
				<h3 class="text-center text-success" style="margin-top: -1%;background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;height: 50px;">
						 <b style="margin-top: 50px;">Customer</b> 
					</h3>
			</div><br /><br />
			<div class="alert alert-success save_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been added to the list..
			</div>
			<div class="alert alert-default  edit_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been edit to the list..
			</div>
			<div class="alert alert-danger delete_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been delete to the list..
			</div>
			<div class="col-md-12">	
				<div class="col-md-4" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 35%;">
					<h3 class="text-center text-success">
						ADD Customer
					</h3>
					<form class="form-horizontal" role="form" id="customer_form">
					<input type="hidden" id="customer_id" />
					<input type="hidden" id="code_number" value="0" />
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Customer Code :
							</label>
							<div class="col-sm-8">
								<input readonly="readonly" id="customer_code" value="<?= $number?>"  style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Customer Code">
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Customer Name:
							</label>
							<div class="col-sm-8">
								<input id="customer_name" name="customer_name" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Customer Name">
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Address :
							</label>
							<div class="col-sm-8">
								<input id="address" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Address">
							</div>
						</div>		
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Phone :
							</label>
							<div class="col-sm-8">
								<input id="phone" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Phone">
							</div>
						</div>		
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Balance :
							</label>
							<div class="col-sm-8">
								<input id="balance" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Balance">
							</div>
						</div>		
														
						<div class="form-group save">								 
							<button  class="btn btn-success add" style="float: right; margin-right: 40%;">
								Save
							</button>
						</div>
						<div class="form-group edit_button" >								 
							<a  class="btn btn-success editform" style="float: right; margin-right: 40%;">
								edit
							</a>
						</div>	
					</form>						
				</div>			
				<div class="col-md-8" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 63%;margin-left: 15px;">
					<h3 class="text-center text-success">
						Customer LIST
					</h3>
					<table class="table table-bordered table-condensed subject_table" style="margin-top: 5%;">
						<thead>
						
							<tr>
								<th>
									#
								</th>
								<th>
									Customer Name
								</th>
								<th>
									Customer Code
								</th>
								<th>
									Address
								</th>
								<th>
									Phone
								</th>
								<th>
									Advance
								</th>
								<th>
									due
								</th>
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
						<?php $total_due=0; $total_advance=0; $i=1; foreach($customers as $customer):?>
							<tr>
								<td>
									<?= $i++;?>
								</td>
								<td>
									<?=$customer['customer_name']?>
								</td>
								<td>
									<?=$customer['customer_code']?>
								</td>
								<td>
								<?=$customer['address']?>
									
								</td>
								<td>
								 <?=$customer['phone']?>
								</td>
								<td>
								 <?php $total_advance=$total_advance+$customer['advance'];echo $customer['advance']?>
								</td>
								<td>
								 <?php $total_due=$total_due+$customer['due_total']; echo $customer['due_total']?>
								</td>
								<td>
									
									<a href="#" data-customer_id="<?php echo $customer['customer_id']?>" data-balance="<?php echo $customer['balance']?>" data-phone="<?php echo $customer['phone']?>" data-address="<?php echo $customer['address']?>"  data-customer_name="<?php echo $customer['customer_name']?>" data-customer_code="<?php echo $customer['customer_code']?>" class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
									<a href="#" data-customer_id="<?php echo $customer['customer_id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
							<tr>
								<td colspan="5">Total</td>
								<td><?= $total_advance;?></td>
								<td><?= $total_due;?></td>
								<td></td>
							</tr>
						</tbody>
					</table>						
				</div>
			</div>	
								
				
			</div>
			<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
 <script src="<?= base_url('dist/js/formValidation.js')?>"></script>
 <script src="<?= base_url('dist/js/framework/bootstrap.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('#datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
				$('.delete_success').hide();
				$('.save_success').hide();
				$('.edit_success').hide();
				$('.edit_button').hide();
				$('#customer_form').formValidation({
				message: 'This value is not valid',
		        icon: {
		            valid: 'glyphicon glyphicon-ok',
		            invalid: 'glyphicon glyphicon-remove',
		            validating: 'glyphicon glyphicon-refresh'
		        },
		        fields: {
		        	customer_name: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The customer name field is required'
		                    }
		                }
		        	}	
		        }
		      }).on('success.form.fv', function(e) {
		      	
		      	var customer_name=$('#customer_name').val();
					var customer_code=$('#customer_code').val();
					var address=$('#address').val();
					var phone=$('#phone').val();
					var balance=$('#balance').val();
					
					$.ajax({
					
					type:'post',
					data:{'customer_name':customer_name,'customer_code':customer_code,'address':address,'phone':phone,'balance':balance},
					url:'<?= site_url('settings/ajaxsaveCustomer')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					
					 $('#customer_id').val('');
					$('#customer_name').val('');
					$('#customer_code').val(+customer_code+ +'1');
					$('#address').val('');
					$('#phone').val('');
					$('#balance').val('');
					 $(".alert-success").slideDown("slow");
					$(".alert-success").delay(1500);
					$(".alert-success").slideUp("slow");
				     }
					
					});
		      	
		      });
				
				
				
				
				$('body').on('click','.edit',function(){
					$('.edit_button').show();
					$('.save').hide();
					var code=$('#customer_code').val();
					
				var customer_id=$(this).data('customer_id');
				  var name=$(this).data('customer_name');
					var customer_code=$(this).data('customer_code');
					var address=$(this).data('address');
					var phone=$(this).data('phone');
					var balance=$(this).data('balance');
					
					$('#code_number').val(code);
					$('#customer_id').val(customer_id);
					$('#customer_name').val(name);
					$('#customer_code').val(customer_code);
					$('#address').val(address);
					$('#phone').val(phone);
					$('#balance').val(balance);
				});
				
				
				
				$('body').on('click','.delete',function(){
				    
				    $('#myModal1').modal('show');
					var customer_id=$(this).data('customer_id');
					
					$('#customerid').val(customer_id);
				});
				
				$('body').on('click','.yes',function(){
					var customer_id=$('#customerid').val();
					
					$.ajax({
					
					type:'post',
					data:{'customer_id':customer_id},
					url:'<?= site_url('settings/ajaxdeleteCustomer')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$(".alert-danger").slideDown("slow");
					$(".alert-danger").delay(1500);
					$(".alert-danger").slideUp("slow");
					 
				     }
				});
					
					
				});
				
				
				$('body').on('click','.editform',function(){
					
					
					var code=$('#code_number').val();
					alert(code);
					var customer_id=$('#customer_id').val();
					var customer_name=$('#customer_name').val();
					var customer_code=$('#customer_code').val();
					var address=$('#address').val();
					var phone=$('#phone').val();
					var balance=$('#balance').val();
					
					$.ajax({
					
					type:'post',
					data:{'customer_id':customer_id,'customer_name':customer_name,'customer_code':customer_code,'address':address,'phone':phone,'balance':balance},
					url:'<?= site_url('settings/ajaxeditCustomer')?>',
					success : function(result){
					$('#customer_code').val(code);
					$('.subject_table').html(result);
					$('#customer_id').val('');
					$('#customer_name').val('');
					
					$('#address').val('');
					$('#phone').val('');
					$('#balance').val('');
					
					 $(".alert-default").slideDown("slow");
					$(".alert-default").delay(1000);
					$(".alert-default").slideUp("slow");
					$('.edit_button').hide();
					$('.save').show();
					
					
				     }
					
					});
				
					
				});
			});
     </script>