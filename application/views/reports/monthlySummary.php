<style>
	.alert-success{
		display: none;
	}
	.alert-default{
		display: none;
	}
	.alert-danger{
		display: none;
	}
	.editform{
		display: none;
	}
</style>

<div class="col-md-12" style="margin-top: 5%;width: 95%;margin-left: 1%; margin-bottom: 1%;">
			
			<div class="bs-example">
				    <!-- Modal HTML -->
				   
				    <div id="myModal1" class="modal fade">
				        <div class="modal-dialog">
				            <div class="modal-content" style="width: 70%;">
				            	<div class="modal-header">
				                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                    <h3 class="modal-title title" style="text-align: center;color:green;">Account</h3>
				                </div>
				                <div class="modal-header">
				                	<input id="amount_tranjection_id1" type="hidden" />
				                	<h4 class="modal-title title" style="text-align: center;color:green;">Are You Sure delete this?</h4><br />
				                 <a  style="margin-left: 130px;" class="btn btn-danger yes" data-dismiss="modal">Yes</a>
				                 <a  style="" class="btn btn-success cancel" data-dismiss="modal">Cancel</a>
				                </div>
				                
				         
				            </div>
				        </div>
				    </div>
				</div>
			
			<br />
			<div class="alert alert-success save_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been added to the list..
			</div>
			<div class="alert alert-default  edit_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been edit to the list..
			</div>
			<div class="alert alert-danger delete_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been delete to the list..
			</div>
			
			
			
			<div class="col-md-12">
				<h3 class="text-center text-success" style="margin-top: -1%;background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;height: 50px;">
						 <b style="margin-top: 50px;">Summary</b> 
					</h3>
			</div>
			<div class="col-md-12">	
				<div class="col-md-4" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 35%;">
					<h3 class="text-center text-success">
						Add Month
					</h3>
					<form class="form-horizontal" role="form" id="expanse_form">
						<input type="hidden" id="amount_tranjection_id" value="0" />
						
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Month Name:
							</label>
							<div class="col-sm-8">
                                <select class="form-control" name="month" id="month">
                                	<option >--Select--</option>
                                	
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="11">December</option>
                                    
                                </select>
								
							</div>
						</div>
						
						
												
						<div class="form-group save">								 
							<button  class="btn btn-success" style="float: right; margin-right: 40%;">
								Save
							</button>
						</div>
							
					</form>						
				</div>			
				<div class="col-md-8" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 63%;margin-left: 15px;">
					<h3 class="text-center text-success">
						Summary Head
					</h3>
					<table class="table table-bordered table-condensed subject_table" style="margin-top: 5%;">
						
						
					</table>						
				</div>
			</div>	
								
				
			</div>	
			


<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
 <script src="<?= base_url('dist/js/formValidation.js')?>"></script>
 <script src="<?= base_url('dist/js/framework/bootstrap.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('#datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
				$('.edit_button').hide();
				$('.delete_success').hide();
				$('.save_success').hide();
				$('.edit_success').hide();
				$('#expanse_form').formValidation({
				message: 'This value is not valid',
		        icon: {
		            valid: 'glyphicon glyphicon-ok',
		            invalid: 'glyphicon glyphicon-remove',
		            validating: 'glyphicon glyphicon-refresh'
		        },
		        fields: {
		        	month: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The from name field is required'
		                    }
		                }
		        	},
		        		
		        },
		        
		       
		       }).on('success.form.fv', function(e) {
		       
		       	    var month=$('#month').val();
					
					$.ajax({
					
					type:'post',
					data:{'month':month},
					url:'<?= site_url('reports/monthlySummaryReports')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					
					
					
				     }
					
					});
				
		       	
		       });
				
				
				
				
				
				
				$('body').on('click','.edit',function(){
					$('.edit_button').show();
					$('.save').hide();
					var amount_tranjection_id=$(this).data('amount_tranjection_id');
				    var Description=$(this).data('description');
				    
				   var to_account=$(this).data('to_account');
					var from_account=$(this).data('from_account');
					var amount=$(this).data('amount');
					$('#amount_tranjection_id').val(amount_tranjection_id)
					$('#from').val(from_account);
					$('#to').val(to_account);
					$('#amount').val(amount);
					$('#description').val(Description);
					

				});
				
				
				$('body').on('click','.delete',function(){
				    
				    $('#myModal1').modal('show');
					var amount_tranjection_id=$(this).data('amount_tranjection_id');
					
					$('#amount_tranjection_id1').val(amount_tranjection_id);
				});
				
				$('body').on('click','.yes',function(){
					var amount_tranjection_id=$('#amount_tranjection_id1').val();
					
					$.ajax({
					
					type:'post',
					data:{'amount_tranjection_id':amount_tranjection_id},
					url:'<?= site_url('account/ajaxdeleteIncomeAccount')?>',
					success : function(result){
					$('#amount_tranjection_id1').val('');
					$('.subject_table').html(result);
					$(".alert-danger").slideDown("slow");
					$(".alert-danger").delay(1500);
					$(".alert-danger").slideUp("slow");
					 
				     }
				});
					
					
				});
				
				
				
			});
     </script>