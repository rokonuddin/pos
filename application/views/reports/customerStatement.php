<div class="col-md-12" style="margin-top: 5%;border: 1px solid #449D44; border-radius: 5px;width: 98%;margin-left: 1%; margin-bottom: 1%;">
			<form class="form-horizontal" role="form">			
			<div class="col-md-12" style="margin-bottom: 1%; text-align: right; margin-top: 1%;width: 99%;">
				<h3 class="text-center text-success">
							Sales
				</h3>
			</div>
				<div class="col-md-12" style="border: 1px solid #449D44; border-radius: 5px;width: 97%;margin-left: 15px;">
									
					<div class="col-md-8">
						<div class="col-md-12">
							<div class="col-md-4">
								
							</div>	
							<div class="col-md-8">
								<h3 class="text-center text-success">
									 Sales Startement
								</h3>
								<div class="form-group" style="margin-top: 15px;">							 
									<label for="inputEmail3" class="col-sm-3 control-label">
									Customer Name :
									</label>
									<div class="col-sm-9">
										
                                     <select class="form-control" id="customer_id">
                                     	
										<option>--Select--</option>
										<?php foreach($customers as $customer):?>
										<option value="<?= $customer['customer_id']?>"><?= $customer['customer_name']?></option>
								        <?php endforeach;?>
								     </select>
                                     
									</div>
								</div>		
								<div class="form-group" style="margin-top: 15px;">							 
									<label for="inputEmail3" class="col-sm-3 control-label">
										Strat Date :
									</label>
									<div class="col-sm-9">
										<div class="form-group">
		                			<div class='input-group datetimepicker1'   style="width: 96%;margin-left: 2%;">
		                    			<input type='text' class="form-control" id="start_date" placeholder="Receiving Date"/>
			                    			<span class="input-group-addon">
			                        			<span class="glyphicon glyphicon-calendar"></span>
			                    			</span>
		                			</div>
		           				</div>
									</div>
								</div>	
								<div class="form-group" style="margin-top: 15px;">							 
									<label for="inputEmail3" class="col-sm-3 control-label">
										End Date :
									</label>
									<div class="col-sm-9">
										<div class="form-group">
		                			<div class='input-group datetimepicker2'   style="width: 96%;margin-left: 2%;">
		                    			<input type='text' id="end_date" class="form-control" placeholder="Receiving Date"/>
			                    			<span class="input-group-addon">
			                        			<span class="glyphicon glyphicon-calendar"></span>
			                    			</span>
		                			</div>
		           				</div>
									</div>
								</div>	
								<div class="col-md-12" style="text-align: right;width: 73%;margin-bottom: 1%;">
					               <a class="btn btn-success search_inquery">Inquery</a>
				                </div>	
							</div>
						</div>			
					</div>			
				</div>			
				<div class="col-md-12 results" style="width: 97%;border: 1px solid #449D44; border-radius: 5px;margin-left: 1%;margin-top: 1%;margin-bottom: 1%;">
					
			</div>
		</form>
		<div class="col-md-1">
		</div>
	</div>
		<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>
<script type="text/javascript">
            $(function () {
                $('.datetimepicker1').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('.datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
			
			
			$('body').on('click','.search_inquery', function(){
				
				var customer_id=$('#customer_id').val();
				var start_date=$('#start_date').val();
				var end_date=$('#end_date').val();
				
				alert(end_date);
				
				$.ajax({
						type:'post',
						data:{'customer_id':customer_id,'start_date':start_date,'end_date':end_date},
						url:'<?php echo site_url('reports/inquery_customer')?>',
						success: function(result){
							
							$('.results').html(result);
							
						},
						
					});
				
			  });
				
				
			});
</script>