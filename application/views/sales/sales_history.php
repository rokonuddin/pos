<div class="col-md-12" style="margin-top: 5%;width: 95%;margin-left: 1%; margin-bottom: 1%;">
			<div class="bs-example">
				    <!-- Modal HTML -->
				    <div id="myModal" class="modal fade">
				        <div class="modal-dialog">
				            <div class="modal-content">
				            	<div class="modal-header">
				                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                    <h4 class="modal-title title" style="text-align: center;"></h4>
				                </div>
				 <table class="table table-bordered table-condensed" style="margin-top: 5%;">
						<thead>
							<tr>
								<th>
									Product Name
								</th>
								<th>
									Quantity
								</th>
								
								<th>
									Unit Price
								</th>
								<th>
									Total
								</th>
							</tr>
						</thead>
						<tbody class="results_product">
							
                           
						</tbody>
				</table>
				            </div>
				        </div>
				    </div>
				    <div id="myModal1" class="modal fade">
				        <div class="modal-dialog">
				            <div class="modal-content">
				            	<div class="modal-header">
				                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                    <h4 class="modal-title title" style="text-align: center;">Due Payment</h4>
				                </div>
				                <div class="modal-header">
				                	<input type="hidden" id="refernce_type" />
				                  Payment:   <input type="text" id="amount" class="form-control" id="name" name="name" style="border-radius: .5em;width: 200px;margin-top:-30px; height: 37px;margin-left:120px;"></br><a  style="margin-left: 190px;" class="btn btn-success payment" data-dismiss="modal">Save</a>
				                </div>
				                
				         
				            </div>
				        </div>
				    </div>
				</div>
			<div class="col-md-12">
				<h3 class="text-center text-success" style="margin-top: -1%;background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;height: 50px;">
						 <b style="margin-top: 60px;">SALES HISTORY</b> 
					</h3>
			</div>
			<div class="col-md-12">	
				<div class="col-md-6" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 40%;">
					<h3 class="text-center text-success">
						SALES
					</h3>
					<form class="form-horizontal" role="form">
					
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Status :
							</label>
							<div class="col-sm-8">
								<select class="form-control" id="status">
								<option>--Selected--</option>
								<option value="DUE">DUE</option>
								<option value="PAID">PAID</option>
								<option value="ALL">ALL</option>
								</select>
							</div>
						</div>
                       
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Date :
							</label>
							<div class="col-sm-8">
								<input id="datetimepicker1" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Date">
							</div>
						</div>		
								
						
						
						<div class="form-group">								 
							<a  class="btn btn-success search" style="float: right; margin-right: 40%;">
								Save
							</a>
						</div>	
					</form>						
				</div>			
				<div class="col-md-6" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 58%;margin-left: 15px;">
					<h3 class="text-center text-success">
						SALES Details
					</h3>
					<table class="table table-bordered table-condensed" style="margin-top: 5%;">
						<thead>
							<tr>
								<th>
									Customer Name
								</th>
								<th>
									Invoice Number
								</th>
								
								<th>
									Total Price
								</th>
								<th>
									Pay amount
								</th>
								<th>
									Due amount
								</th>
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody class="results">
							
                           
						</tbody>
					</table>
													
				</div>
			</div>	
								
				
			</div>	
		
<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('#datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
				  $("#myModal").modal('hide');
				  $("#myModal1").modal('hide');
				$('body').on('click','.search',function(){
					
					var status=$('#status').val();
					var date=$('#datetimepicker1').val();
					var url='<?= site_url('sales/search')?>';
					$.ajax({
						type:'post',
						data:{'status':status,'date':date},
						url:url,
						async : false,
						 success : function(result){
				    	      
				    	    $('.results').html(result);
				         }
					});
					
				});
				$('body').on('click','.view_details',function(){
					
				var invoice_number=	$(this).data('invoice_number');
				     $('.title').html(invoice_number);
				     var url='<?= site_url('sales/search_product')?>';
					$.ajax({
						type:'post',
						data:{'invoice_number':invoice_number},
						url:url,
						async : false,
						 success : function(result){
				    	      
				    	    $('.results_product').html(result);
				         }
					});
				     $("#myModal").modal('show');
				});
				$('body').on('click','.view_details1',function(){
					var sales_id =$(this).data('sales_id');
					$('#refernce_type').val(sales_id);
					$("#myModal1").modal('show');
					
				});
				$('body').on('click','.payment',function(){
					
					var sales_id= $('#refernce_type').val();
					var amount= $('#amount').val();
					var url='<?= site_url('sales/pay_amount')?>';
					$.ajax({
						type:'post',
						data:{'sales_id':sales_id,'amount':amount},
						url:url,
						async : false,
						 success : function(result){
				    	      $("#myModal1").modal('hide');
				    	    $('#amount').val('');
				         }
					});
					
					
					
				});
				
				
			});
     </script>