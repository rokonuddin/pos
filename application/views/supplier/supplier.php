<style>
	.alert-success{
		display: none;
	}
	.alert-default{
		display: none;
	}
	.alert-danger{
		display: none;
	}
	#ma_no{
		display: none;
	}
	#ma_phone{
		display: none;
	}
	#ma_address{
		display: none;
	}
	#ma_supplier{
		display: none;
	}
	
</style>

<div class="col-md-12" style="margin-top: 5%;width: 95%;margin-left: 1%; margin-bottom: 1%;">
			<div class="bs-example">
				    <!-- Modal HTML -->
				   
				    <div id="myModal1" class="modal fade">
				        <div class="modal-dialog">
				            <div class="modal-content" style="width: 70%;">
				            	<div class="modal-header">
				                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                    <h3 class="modal-title title" style="text-align: center;color:green;">Supplier</h3>
				                </div>
				                <div class="modal-header">
				                	<input id="supplier_id" type="hidden" />
				                	<h4 class="modal-title title" style="text-align: center;color:green;">Are You Sure delete this?</h4><br />
				                 <a  style="margin-left: 130px;" class="btn btn-danger yes" data-dismiss="modal">Yes</a>
				                 <a  style="" class="btn btn-success cancel" data-dismiss="modal">Cancel</a>
				                </div>
				                
				         
				            </div>
				        </div>
				    </div>
				</div>
			
			
			<div class="col-md-12">
				<h3 class="text-center text-success" style="margin-top: -1%;background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;height: 50px;">
						 <b style="margin-top: 50px;">Supplier</b> 
					</h3>
			</div><br /><br />
			<div class="alert alert-success save_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been added to the list..
			</div>
			<div class="alert alert-default  edit_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been edit to the list..
			</div>
			<div class="alert alert-danger delete_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been delete to the list..
			</div>
			
			<div class="col-md-12">	
				<div class="col-md-4" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 35%;">
					<h3 class="text-center text-success">
						ADD Supplier
					</h3>
					<form class="form-horizontal" role="form" id="supplier">
					<input type="hidden" id="id" value="0" />
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Supplier No :
							</label>
							<div class="col-sm-8">
								<input id="number" name="number" value="<?= $number?>" readonly="readonly" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control"  type="text" placeholder="Supplier No">
								<span id="ma_no" style="color:red;">filed must requied</span>
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Supplier Name:
							</label>
							<div class="col-sm-8">
								<input name="name" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="name" type="text" placeholder="Supplier Name">
								<span id="ma_supplier" style="color:red;">filed must requied</span>
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Address :
							</label>
							<div class="col-sm-8">
								<input id="address" name="address" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control"  type="text" placeholder="Address">
								<span id="ma_address" style="color:red;">filed must requied</span>
							</div>
						</div>		
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Phone :
							</label>
							<div class="col-sm-8">
								<input id="phone" name="phone" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control"  type="text" placeholder="Phone">
								<span id="ma_phone" style="color:red;">filed must requied</span>
							</div>
						</div>		
						
														
						<div class="form-group save">								 
							<button  class="btn btn-success" style="float: right; margin-right: 40%;">
								Save
							</button>
						</div>
						<div class="form-group edit_button" >								 
							<a  class="btn btn-success editform" style="float: right; margin-right: 40%;">
								edit
							</a>
						</div>	
					</form>						
				</div>			
				<div class="col-md-8" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 63%;margin-left: 15px;">
					<h3 class="text-center text-success">
						Manufacture LIST
					</h3>
					<table class="table table-bordered table-condensed subject_table" style="margin-top: 5%;">
						<thead>
						
							<tr>
								<th>
									#
								</th>
								<th>
									Supplier Name
								</th>
								
								<th>
									Supplier No
								</th>
								<th>
									Phone
								</th>
								<th>
									advance
								</th>
								<th>
									due
								</th>
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
						<?php $total_due=0; $total_advance=0;  $i=1; foreach($suppliers as $supplier):?>
							<tr>
								<td>
									<?= $i++;?>
								</td>
								<td>
									<?=$supplier['supplier_name']?>
								</td>
								
								<td>
								<?=$supplier['supplier_no']?>
									
								</td>
								<td>
								<?=$supplier['phone']?>
								</td>
								<td>
								<?php $total_advance=$total_advance+$supplier['advance']; echo $supplier['advance']?>
									
								</td>
								<td>
								<?php $total_due=$total_due+$supplier['due']; echo $supplier['due']?>
								</td>
								<td>
									
									<a href="#" data-name="<?php echo $supplier['supplier_name']?>" data-number="<?php echo $supplier['supplier_no']?>" data-id="<?php echo $supplier['id']?>" data-address="<?php echo $supplier['address']?>" data-phone="<?php echo $supplier['phone']?>"  class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
									<a href="#" data-id="<?php echo $supplier['id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
							<tr>
								<td colspan="4">Total</td>
								<td><?= $total_advance;?></td>
								<td><?= $total_due;?></td>
								<td></td>
							</tr>
							
						</tbody>
					</table>						
				</div>
			</div>	
								
				
			</div>
			<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>


 <script src="<?= base_url('dist/js/formValidation.js')?>"></script>
 <script src="<?= base_url('dist/js/framework/bootstrap.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('#datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
				$('#supplier').formValidation({
				message: 'This value is not valid',
		        icon: {
		            valid: 'glyphicon glyphicon-ok',
		            invalid: 'glyphicon glyphicon-remove',
		            validating: 'glyphicon glyphicon-refresh'
		        },
		        fields: {
		        	name: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The Supplier Name field is required'
		                    }
		                }
		        	},
		        	phone: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The Phone field is required'
		                    }
		                }
		        	}	
		        }
		        }).on('success.form.fv', function(e) {
		        	//alert('im here');return false;
		        	var id=$('#id').val();
		        	var name=$('#name').val();
					var number=$('#number').val();
					var address=$('#address').val();
					var phone=$('#phone').val();				
					$.ajax({					
					type:'post',
					data:{'id':id,'name':name,'number':number,'address':address,'phone':phone},
					url:'<?= site_url('settings/ajaxsavesupplier')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$(".alert-success").slideDown("slow");
					$(".alert-success").delay(1500);
					$(".alert-success").slideUp("slow");
					
					$('#id').val('0');
					$('#name').val('');
					$('#number').val(getNumber());
					$('#address').val('');
					$('#phone').val('');
					 
				     }
					
					});
		        	
		        	
		        	});	
				
				$('.delete_success').hide();
				$('.save_success').hide();
				$('.edit_success').hide();
				$('.edit_button').hide();
				$('#ma_name').hide();
				// $('body').on('click','.add',function(){
					
				// 	var name=$('#name').val();
					
				
						
				// 	$('#ma_name').hide();
				// 	var contact_person=$('#contact_person').val();
				// 	var address=$('#address').val();
				// 	var phone=$('#phone').val();
				// 	var description=$('#description').val();
					
				// 	$.ajax({
					
				// 	type:'post',
				// 	data:{'name':name,'contact_person':contact_person,'address':address,'phone':phone,'description':description},
				// 	url:'<?= site_url('settings/ajaxsave')?>',
				// 	success : function(result){
					
				// 	$('.subject_table').html(result);
				// 	$(".alert-success").slideDown("slow");
				// 	$(".alert-success").delay(1500);
				// 	$(".alert-success").slideUp("slow");
				// 	//$('.delete_success').hide();
				// 	$('#menufacture_id').val('');
				// 	$('#name').val('');
				// 	$('#contact_person').val('');
				// 	$('#address').val('');
				// 	$('#phone').val('');
				// 	$('#description').val('');
					 
				//      }
					
				// 	});
					
					
				// });
				
				
				
				$('body').on('click','.edit',function(){
					$('.edit_button').show();
					$('.save').hide();
				var id=$(this).data('id');
				  var name=$(this).data('name');
					var number=$(this).data('contact_person');
					var address=$(this).data('address');
					var phone=$(this).data('phone');
					
					$('#id').val(id);
					$('#name').val(name);
					$('#number').val(contact_person);
					$('#address').val(address);
					$('#phone').val(phone);
				});
				$('body').on('click','.delete',function(){
				    
				    $('#myModal1').modal('show');
					var id=$(this).data('id');
					$('#supplier_id').val(id);
				});
				
				$('body').on('click','.yes',function(){
					var id=$('#supplier_id').val();
					$.ajax({
					
					type:'post',
					data:{'id':id},
					url:'<?= site_url('settings/ajaxdelete')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$(".alert-danger").slideDown("slow");
					$(".alert-danger").delay(1500);
					$(".alert-danger").slideUp("slow");
					 
				     }
				});
					
					
				});
				
				$('body').on('click','.editform',function(){
					var id=$('#id').val();
					var name=$('#name').val();
					var number=$('#number').val();
					var address=$('#address').val();
					var phone=$('#phone').val();
					
					$.ajax({
					type:'post',
					data:{'id':id,'name':name,'number':number,'address':address,'phone':phone},
					url:'<?= site_url('settings/ajaxedit')?>',
					success : function(result){
						$(".alert-default").slideDown("slow");
					$(".alert-default").delay(1000);
					$(".alert-default").slideUp("slow");
					$('#menufacture_id').val('');
					$('#name').val('');
					$('#contact_person').val('');
					$('#address').val('');
					$('#phone').val('');
					$('#description').val('');
					$('.subject_table').html(result);
					
					 $('.edit_button').hide();
					 $('.save').show();
				     }
					
					});
				     
					
				});
			});
            function getNumber(){
            	$.ajax({
					type:'post',
					url:'<?= site_url('settings/getNumber')?>',
					success : function(result){
						$('#number').val(result);
					}
				});
            }
     </script>