<thead>
						
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								
								<th>
									Supplier No
								</th>
								<th>
									Phone
								</th>
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach($suppliers as $supplier):?>
							<tr>
								<td>
									<?= $i++;?>
								</td>
								<td>
									<?=$supplier['supplier_name']?>
								</td>
								
								<td>
								<?=$supplier['supplier_no']?>
									
								</td>
								<td>
								<?=$supplier['phone']?>
								</td>
								<td>
									<a href="#" data-menufacture="<?php echo $supplier['id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a>
									<a href="#" data-suppliername="<?php echo $supplier['supplier_name']?>" data-id="<?php echo $supplier['id']?>" data-supplierno="<?php echo $supplier['supplier_no']?>" data-phone="<?php echo $supplier['phone']?>" data-address="<?php echo $supplier['address']?>" class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
							
						</tbody>