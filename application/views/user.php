<div class="col-md-12" style="margin-top: 5%;width: 95%;margin-left: 1%; margin-bottom: 1%;">
			<div class="col-md-12">
				<h3 class="text-center text-success" style="margin-top: -1%;background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;height: 50px;">
						 <b style="margin-top: 50px;">USER FORM</b> 
					</h3>
			</div>
			<div class="col-md-12">	
				<div class="col-md-4" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 35%;">
					<h3 class="text-center text-success">
						 USER ADD
					</h3>
					<form class="form-horizontal" role="form">
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-3 control-label">
								First Name :
							</label>
							<div class="col-sm-9">
								<input id="first_name" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Name">
							<?php echo form_error('first_name', '<span class="help-inline margin" style="color:red;">', '</span>'); ?>
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-3 control-label">
								Last Name :
							</label>
							<div class="col-sm-9">
								<input id="last_name" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Name">
							<?php echo form_error('last_name', '<span class="help-inline margin" style="color:red;">', '</span>'); ?>
							
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-3 control-label">
								Email :
							</label>
							<div class="col-sm-9">
								<input id="email" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="email" placeholder="Email">
							<?php echo form_error('email', '<span class="help-inline margin" style="color:red;">', '</span>'); ?>
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-3 control-label">
								Address :
							</label>
							<div class="col-sm-9">
								<input id="address" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Address">
							<?php echo form_error('last_name', '<span class="help-inline margin" style="color:red;">', '</span>'); ?>
							</div>
						</div>		
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-3 control-label">
								Phone :
							</label>
							<div class="col-sm-9">
								<input id="phone" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Phone">
							<?php echo form_error('phone', '<span class="help-inline margin" style="color:red;">', '</span>'); ?>
							</div>
						</div>		
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-3 control-label">
								password :
							</label>
							<div class="col-sm-9">
								<input id="password" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="password" placeholder="">
							<?php echo form_error('password', '<span class="help-inline margin" style="color:red;">', '</span>'); ?>
							</div>
						</div>		
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-3 control-label">
								Confirm Password :
							</label>
							<div class="col-sm-9">
								<input id="confirm_password" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="password" placeholder="">
							<?php echo form_error('password_confirm', '<span class="help-inline" style="color:red; margin-left:17%;">', '</span>'); ?>
							</div>
						</div>										
						<div class="form-group save">								 
							<a class="btn btn-success add" style="float: right; margin-right: 40%;">
								Save
							</a>
						</div>
						<div class="form-group edit_button">								 
							<a class="btn btn-success editform" style="float: right; margin-right: 40%;">
								Edit
							</a>
						</div>	
					</form>						
				</div>			
				<div class="col-md-8" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 63%;margin-left: 15px;">
					<h3 class="text-center text-success">
						USER LIST
					</h3>
					<table class="table table-bordered table-condensed" style="margin-top: 5%;">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								<th>
									Email
								</th>
								<th>
									Address
								</th>
								<th>
									Phone
								</th>
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									1
								</td>
								<td>
									Abul
								</td>
								<td>
									abul@abul.com
								</td>
								<td>
									Address
								</td>
								<td>
									01710000000
								</td>
								<td>
								
								<a href="#" >
								<span class="glyphicon glyphicon-remove"></span>
								
								</a>
								
								</td>
							</tr>
							<tr class="active">
								<td>
									2
								</td>
								<td>
									Babul
								</td>
								<td>
									babul@babul.com
								</td>
								<td>
									Address
								</td>
								<td>
									01710000000
								</td>
							</tr>
							<tr class="success">
								<td>
									3
								</td>
								<td>
									Chowdory
								</td>
								<td>
									chowdory@chowdory.com
								</td>
								<td>
									Address
								</td>
								<td>
									01710000000
								</td>
							</tr>
							<tr class="warning">
								<td>
									4
								</td>
								<td>
									Dalim
								</td>
								<td>
									dalim@dalim.com
								</td>
								<td>
									Address
								</td>
								<td>
									01710000000
								</td>
							</tr>
							<tr class="danger">
								<td>
									5
								</td>
								<td>
									Enimi
								</td>
								<td>
									enimi@enimi.com
								</td>
								<td>
									Address
								</td>
								<td>
									01710000000
								</td>
							</tr>
						</tbody>
					</table>						
				</div>
			</div>	
		</div>
		<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('#datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
				$('.edit_button').hide();
				$('body').on('click','.add',function(){
					
					var first_name=$('#first_name').val();
					
					
					var last_name=$('#last_name').val();
					
					var email=$('#email').val();
					
					var address=$('#address').val();
					
					var phone=$('#phone').val();
					
					var password=$('#password').val();
					
					var confirm_password=$('#confirm_password').val();
					
					
					$.ajax({
					
					type:'post',
					data:{'first_name':first_name,'last_name':last_name,'email':email,'address':address,'phone':phone,'password':password,'confirm_password':confirm_password},
					url:'<?= site_url('auth/create_user')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					
					 
				     }
					
					});
				});
				
				
				
				$('body').on('click','.edit',function(){
					$('.edit_button').show();
					$('.save').hide();
					var first_name=$('#first_name').val();
					
					
					var last_name=$('#last_name').val();
					
					var email=$('#email').val();
					
					var address=$('#address').val();
					
					var phone=$('#phone').val();
					
					var password=$('#password').val();
					
					var confirm_password=$('#confirm_password').val();
					
					$('#catagory_id').val(catagory_id);
					$('#catagory_name').val(catagory_name);
					
					$('#description').val(description);
				});
				$('body').on('click','.delete',function(){
				
					var catagory_id=$(this).data('catagory_id');
					$.ajax({
					
					type:'post',
					data:{'catagory_id':catagory_id},
					url:'<?= site_url('settings/ajaxdeleteCatagory')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					
					 
				     }
				});
				});
				$('body').on('click','.editform',function(){
					
					
					
					var catagory_id=$('#catagory_id').val();
					var name=$('#catagory_name').val();
					
					var description=$('#description').val();
					
					$.ajax({
					
					type:'post',
					data:{'catagory_id':catagory_id,'name':name,'description':description},
					url:'<?= site_url('settings/ajaxeditcatagory')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$('#catagory_name').val('');
					$('#catagory_id').val('');
					
					$('#description').val('');
					 
				     }
					
					});
				
					
				});
			});
     </script>