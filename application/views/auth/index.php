<style>
	input[type=text], textarea {
		-webkit-transition: all 0.30s ease-in-out;
		-moz-transition: all 0.30s ease-in-out;
		-ms-transition: all 0.30s ease-in-out;
		-o-transition: all 0.30s ease-in-out;
		outline: none;
		padding: 3px 0px 3px 3px;
		margin: 5px 1px 3px 0px;
		border: 1px solid #DDDDDD;
		display: inline-block
	}

	input[type=text]:focus, textarea:focus {
		box-shadow: 0 0 5px rgba(81, 203, 238, 1);
		padding: 3px 0px 3px 3px;
		margin: 5px 1px 3px 0px;
		border: 1px solid rgba(81, 203, 238, 1);
	}
	
	fieldset.scheduler-border {
	    border: 1px groove #000000 !important;
	    width:300px;
	    box-sizing: border-box;
	    float:left;
	    padding: 0 1.4em 1.4em 1.4em !important;
	    -webkit-box-shadow:  0px 0px 0px 0px #000;
	            box-shadow:  0px 0px 0px 0px #000;
	}

    legend.scheduler-border {
        font-size: 1.2em !important;
        font-weight: bold !important;
        text-align: left !important;
        width:auto;
        padding:0 10px;
        border-bottom:none;
        margin-top:17px;
    }
   .help-block {
	    color: #737373;
	    display: block;
	    margin-bottom: 10px;
	    margin-left: 184px;
	    margin-top: -12px;
	}

	.form-control-feedback {
	    display: block;
	    height: 34px;
	    line-height: 34px;
	    margin-top: 17px;
	    pointer-events: none;
	    position: absolute;
	    right: 0;
	    text-align: center;
	    top: 0;
	    width: 34px;
	    z-index: 2;
	    margin-right: -50px;
	}
	
	.has-feedback .form-control {
  	  padding-right: 1.5px;
	}
   .alert-success{
   	display:none;
   }
      .alert-info{
   	display:none;
   }
      .alert-danger{
   	display:none;
   }   
</style>

<div class="container" style="margin-top: -33px">

	<div class="well well-sm" style="margin-top: 16px;">
		<h1 style="color: #5E5E58;text-align: center">All Users List</h1>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-12 col-xs-12">
					
			<a class="btn btn-success" href="<?php echo  site_url('auth/create_user');?> "> Add User</a>
		
			<!--a class="btn btn-success" href="<?php echo  site_url('controller_users/load_campain_group');?> "> Add Group</a-->
		<br />	
		<div class="table-responsive" style="width: 100%; float: left;background-color: #F9F9F9;border: 2px solid #E7E7E7;border-radius: 1em;margin-top: 10px;">

		
					<table class="table table-bordered">
						<thead>
							<tr>
								<th style="width: 50px">#</th>
								<th><?php echo lang('index_fname_th');?></th>
								<th><?php echo lang('index_lname_th');?></th>
								<th><?php echo lang('index_email_th');?></th>
								<th><?php echo lang('index_groups_th');?></th>
								<th><?php echo lang('index_status_th');?></th>
								<th><?php echo lang('index_action_th');?></th>
							</tr>
						</thead>
						<tbody class="subject_table">
							<?php $counter=1;foreach($users as $user):?>
								<tr class="odd">
									<td><?php echo $counter;?></td>
									 <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
            						<td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
            						<td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
									<td>
										<?php foreach ($user->groups as $group):?>
											<?php echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
						                <?php endforeach?>
									</td>
									<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'));?></td>
									<td>
									<span class="btn btn-info" style="display: inline-block;"><a href="<?php echo site_url('auth/edit_user/'.$user->id)?>"  
											
											style="width: 70px;color: #204D74;">Edit</span>
										</a>
									</span>	 
									</td>
								</tr>	
							<?php $counter++;endforeach;?>
						</tbody>
					</table>
					<a class="btn btn-sm btn-danger" style="text-align: center; margin-left: 42%;" id="backButton" >Back To Data Table</a>
				</div>		
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('.hideinput').hide();
		$('#backButton').hide();
		var deleteID=0;
		$('#updateButton').hide();
		$('#cancelButton').hide();
		$('.alert-success').hide();
		$('.alert-info').hide();
		$('.alert-danger').hide();
		$('#defaultForm').formValidation({
			message: 'This value is not valid',
	        icon: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {
	        	name: {
	        		validators: {
	                    notEmpty: {
	                        message: 'The Name field is required'
	                    }
	                }
	        	},
	             phone: {
	                message: 'The code is not valid',
	                validators: {
	                    notEmpty: {
	                        message: 'Code is required'
	                    },
	                    regexp: {
	                        regexp: /^[0-9.]+$/,
	                        message: 'Input only Decimal'
	                    }
	                }
	            },
	        }
	        }).on('success.form.fv', function(e) {
	            // Prevent form submission
	            e.preventDefault();
	            var $form = $(e.target),
	            fv = $form.data('formValidation');
	            
	            var check_id_empty = $('#customer_id').val();
	            if(check_id_empty == ''){
	            	var url = "<?= site_url('controller_customer/ajxSaveCustomer')?>";
	            	$.ajax({
	                url: url,
	                type: 'POST',
	                data: $form.serialize(),
	                success: function(result) {
	                    $(".alert-success").slideDown("slow");
						$(".alert-success").delay(1500);
						$(".alert-success").slideUp("slow");
						reset();
		 				$form.data('formValidation').resetForm();
		 				$('.subject_table').html(result);
		 				$('#backButton').hide();
	                }
	            });
	            }else{
	            	$("#myModalUpdate").modal('show');
	            	$('body').on('click','.confirmUpdate',function(){
	            	var url = "<?= site_url('controller_customer/ajxUpdateCustomer')?>";
	            	$.ajax({
	                url: url,
	                type: 'POST',
	                data: $form.serialize(),
	                success: function(result) {
	                    $(".alert-info").slideDown("slow");
						$(".alert-info").delay(1500);
						$(".alert-info").slideUp("slow");
						$('#updateButton').hide();
						$('#deleteButton').hide();
						$('#cancelButton').hide();
						$('#addButton').show();
						reset();
		 				$form.data('formValidation').resetForm();
		 				$('.subject_table').html(result);
		 				$('#backButton').hide();
		 				$("#myModalUpdate").modal('hide');
		 				
	                }
	            });
	            });
	            }
	        });
	        
/*--------------------------------------------Start Editing Grading Entry Form---------------------------------------*/
		$('body').on('click','.cus_edit',function(){
			$('.hideinput').show();
			$('#updateButton').show();
			$('#cancelButton').show();
			$('#addButton').hide();
			var customer = $(this).data('cus_id');
			$('#customer_id').val(customer);
			$('#name').val($(this).data('cus_name'));
			$('#email').val($(this).data('cus_email'));
			$('#phone').val($(this).data('cus_phone'));
			//$('#gender').val($(this).data('cus_gender'));
			var gender = $(this).data('cus_gender');
			$('.gender').val(gender);		
			$('#occupation').val($(this).data('cus_occupation'));
			$('#company').val($(this).data('cus_company'));
			$('#remark').val($(this).data('cus_remark'));
		});
/*---------------------------------------------------------End--------------------------------------------------------*/


/*-------------------------------------------------Start Delete Grading---------------------------------------------*/

		$('body').on('click','.cus_delete',function(e){
			deleteID = $(this).data("cus_id");
			$('.confirmDelete').val(deleteID);
			$("#myModal").modal('show');
		});
		
		$('body').on('click','.confirmDelete',function(){
			var deleteID = $(this).val();
			var url = "<?= site_url('controller_customer/ajxDeleteCustomer'); ?>" ;
			$.ajax({
				type : "post",
				data : { 'customer_id' : deleteID},
				async : false,
				dataType : "html",
				url : url,
				success : function(result){
					$("#myModal").modal('hide');
					$('.subject_table').html(result);
					$(".alert-danger").slideDown("slow");
					$(".alert-danger").delay(1500);
					reset();
				},
				complete: function(){
					$(".alert-danger").slideUp("slow");
				}
			});
			});

/*---------------------------------------------------------End------------------------------------------------------------*/ 

/*-----------------------------------------------------Cancel Update--------------------------------------------------*/
		$('body').on('click','#cancelButton',function(e){
			reset();
			$('.hideinput').hide();
			$('#updateButton').hide();
			$('#cancelButton').hide();
			$('#addButton').show();
			var $form = $(e.target);
	        $('#defaultForm').formValidation().resetForm();
		});
/*---------------------------------------------------------End--------------------------------------------------------*/			
/*------------------------------------------------------form reset-------------------------------------------------------*/
			function reset(){
				$('#customer_id').val('');
 				$('#name').val('');
 				$('#email').val('');
 				$('#phone').val('');
 				$('.gender').val('');
 				$('#occupation').val('');
 				$('#company').val('');
 				$('#remark').val('');
			}
/*---------------------------------------------------------End--------------------------------------------------------*/

/*---------------------------------------------------Get Back to Datatable---------------------------------------------*/
		$('body').on('click','#backButton',function(e){
			$.ajax({
				url : "<?= site_url('Controller_subject/ajxgetDataTable'); ?>",
				success : function(result){
					$('.subject_table').html(result);
					$('#backButton').hide();
					reset();
				}
			});
		});
/*--------------------------------------------------------End----------------------------------------------------------*/

	});
	
</script>

