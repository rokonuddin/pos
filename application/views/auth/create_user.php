<div class="col-md-12" style="margin-top: 5%;width: 95%;margin-left: 1%; margin-bottom: 1%;">
			<div class="col-md-12">
				<h3 class="text-center text-success" style="margin-top: -1%;background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;height: 50px;">
						 <b style="margin-top: 50px;">USER FORM</b> 
					</h3>
			</div>
			<div class="col-md-12">	
				<div class="col-md-12" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;">
					<h3 class="text-center text-success">
						 USER ADD
					</h3>
					<form class="form-horizontal" method="post" style="text-align: center;margin-left: 150px;" action="<?php echo (isset($user->first_name))?site_url('settings/edit_user'):site_url('auth/create_user')?>"  >
						<input name="id" type="hidden" value="<?= (isset($user->id))?$user->id:0?>" />
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-3 control-label">
								Client Name :
							</label>
							<div class="col-sm-4">
								<select class="form-control" name="config_id" id="config_id">
								<option>---select--- </option>
								<?php foreach($clients as $client):?> 
								<option <?= (isset($user->client_code) && $user->client_code==$client['code_number'])?'selected="selected"':''?> value="<?= $client['code_number']?>"><?= $client['company_name']?></option>
								<?php endforeach;?>
								</select>
							</div>
							
							
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-3 control-label">
								First Name :
							</label>
							<div class="col-sm-4">
								<input id="first_name" name="first_name" value="<?= (isset($user->first_name))?$user->first_name:''?>" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control"  type="text" placeholder="Name">
							<?php echo form_error('first_name', '<span class="help-inline margin" style="color:red;">', '</span>'); ?>
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-3 control-label">
								Last Name :
							</label>
							<div class="col-sm-4">
								<input id="last_name" value="<?= (isset($user->last_name))?$user->last_name:''?>" name="last_name" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control"  type="text" placeholder="Name">
							<?php echo form_error('last_name', '<span class="help-inline margin" style="color:red;">', '</span>'); ?>
							
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-3 control-label">
								Email :
							</label>
							<div class="col-sm-4">
								<input id="email" name="email" value="<?= (isset($user->email))?$user->email:''?>" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control"  type="email" placeholder="Email">
							<?php echo form_error('email', '<span class="help-inline margin" style="color:red;">', '</span>'); ?>
							</div>
						</div>
							
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-3 control-label">
								Phone :
							</label>
							<div class="col-sm-4">
								<input id="phone" name="phone" value="<?= (isset($user->phone))?$user->phone:''?>" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control"  type="text" placeholder="Phone">
							<?php echo form_error('phone', '<span class="help-inline margin" style="color:red;">', '</span>'); ?>
							</div>
						</div>	
						<?php if(isset($user)){?>
						<?php }else{?>	
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-3 control-label">
								password :
							</label>
							<div class="col-sm-4">
								<input id="password" name="password" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control"  type="password" placeholder="">
							<?php echo form_error('password', '<span class="help-inline margin" style="color:red;">', '</span>'); ?>
							</div>
						</div>		
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-3 control-label">
								Confirm Password :
							</label>
							<div class="col-sm-4">
								<input id="password_confirm" name="password_confirm" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control"  type="password" placeholder="">
							<?php echo form_error('password_confirm', '<span class="help-inline" style="color:red; margin-left:17%;">', '</span>'); ?>
							</div>
						</div>	
						<?php }?>									
						<div class="form-group">								 
							<button type="submit" class="btn btn-success" style="float: right; margin-right: 40%;">
								Save
							</button>
						</div>
							
					</form>						
				</div>			
				
			</div>	
		</div>
		<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?= base_url('dist/js/formValidation.js'); ?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>