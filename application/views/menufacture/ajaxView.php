<thead>
						
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								
								<th>
									Address
								</th>
								<th>
									Phone
								</th>
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach($menufactures as $menufacture):?>
							<tr>
								<td>
									<?= $i++;?>
								</td>
								<td>
									<?=$menufacture['name']?>
								</td>
								
								<td>
								<?=$menufacture['address']?>
									
								</td>
								<td>
								<?=$menufacture['phone']?>
								</td>
								<td>
									<a href="#" data-menufacture="<?php echo $menufacture['menufacture_id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a>
									<a href="#" data-contact_person="<?php echo $menufacture['contact_person']?>" data-description="<?php echo $menufacture['description']?>" data-menufacture="<?php echo $menufacture['menufacture_id']?>" data-name="<?php echo $menufacture['name']?>" data-phone="<?php echo $menufacture['phone']?>" data-address="<?php echo $menufacture['address']?>" class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
							
						</tbody>