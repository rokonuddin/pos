<style>
	.alert-success{
		display: none;
	}
	.alert-default{
		display: none;
	}
	.alert-danger{
		display: none;
	}
	
</style>

<div class="col-md-12" style="margin-top: 5%;width: 95%;margin-left: 1%; margin-bottom: 1%;">
			<div class="bs-example">
				    <!-- Modal HTML -->
				   
				    <div id="myModal1" class="modal fade">
				        <div class="modal-dialog">
				            <div class="modal-content" style="width: 70%;">
				            	<div class="modal-header">
				                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                    <h3 class="modal-title title" style="text-align: center;color:green;">Menufacture</h3>
				                </div>
				                <div class="modal-header">
				                	<input id="menufactureid" type="hidden" />
				                	<h4 class="modal-title title" style="text-align: center;color:green;">Are You Sure delete this?</h4><br />
				                 <a  style="margin-left: 130px;" class="btn btn-danger yes" data-dismiss="modal">Yes</a>
				                 <a  style="" class="btn btn-success cancel" data-dismiss="modal">Cancel</a>
				                </div>
				                
				         
				            </div>
				        </div>
				    </div>
				</div>
			
			
			<div class="col-md-12">
				<h3 class="text-center text-success" style="margin-top: -1%;background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;height: 50px;">
						 <b style="margin-top: 50px;">Manufacture</b> 
					</h3>
			</div><br /><br />
			<div class="alert alert-success save_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been added to the list..
			</div>
			<div class="alert alert-default  edit_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been edit to the list..
			</div>
			<div class="alert alert-danger delete_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been delete to the list..
			</div>
			
			<div class="col-md-12">	
				<div class="col-md-4" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 35%;">
					<h3 class="text-center text-success">
						ADD Manufacture
					</h3>
					<form class="form-horizontal" role="form" id="menufacture">
					<input type="hidden" id="menufacture_id" />
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Name :
							</label>
							<div class="col-sm-8">
								<input id="name" name="name" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Name">
								<span id="ma_name" style="color:red;">filed must requied</span>
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Contact Person:
							</label>
							<div class="col-sm-8">
								<input id="contact_person" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Contact Person Name">
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Address :
							</label>
							<div class="col-sm-8">
								<input id="address" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Address">
							</div>
						</div>		
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Phone :
							</label>
							<div class="col-sm-8">
								<input id="phone" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Phone">
							</div>
						</div>		
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Description :
							</label>
							<div class="col-sm-8">
								<textarea id="description" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" ></textarea>
							</div>
						</div>		
														
						<div class="form-group save">								 
							<button  class="btn btn-success" style="float: right; margin-right: 40%;">
								Save
							</button>
						</div>
						<div class="form-group edit_button" >								 
							<a  class="btn btn-success editform" style="float: right; margin-right: 40%;">
								edit
							</a>
						</div>	
					</form>						
				</div>			
				<div class="col-md-8" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 63%;margin-left: 15px;">
					<h3 class="text-center text-success">
						Manufacture LIST
					</h3>
					<table class="table table-bordered table-condensed subject_table" style="margin-top: 5%;">
						<thead>
						
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								
								<th>
									Address
								</th>
								<th>
									Phone
								</th>
								<th>
									advance
								</th>
								<th>
									due
								</th>
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
						<?php $total_due=0; $total_advance=0;  $i=1; foreach($menufactures as $menufacture):?>
							<tr>
								<td>
									<?= $i++;?>
								</td>
								<td>
									<?=$menufacture['name']?>
								</td>
								
								<td>
								<?=$menufacture['address']?>
									
								</td>
								<td>
								<?=$menufacture['phone']?>
								</td>
								<td>
								<?php $total_advance=$total_advance+$menufacture['advance']; echo $menufacture['advance']?>
									
								</td>
								<td>
								<?php $total_due=$total_due+$menufacture['total_due']; echo $menufacture['total_due']?>
								</td>
								<td>
									
									<a href="#" data-contact_person="<?php echo $menufacture['contact_person']?>" data-description="<?php echo $menufacture['description']?>" data-menufacture="<?php echo $menufacture['menufacture_id']?>" data-name="<?php echo $menufacture['name']?>" data-phone="<?php echo $menufacture['phone']?>" data-address="<?php echo $menufacture['address']?>" class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
									<a href="#" data-menufacture="<?php echo $menufacture['menufacture_id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
							<tr>
								<td colspan="4">Total</td>
								<td><?= $total_advance;?></td>
								<td><?= $total_due;?></td>
								<td></td>
							</tr>
							
						</tbody>
					</table>						
				</div>
			</div>	
								
				
			</div>
			<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>


 <script src="<?= base_url('dist/js/formValidation.js')?>"></script>
 <script src="<?= base_url('dist/js/framework/bootstrap.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('#datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
				$('#menufacture').formValidation({
				message: 'This value is not valid',
		        icon: {
		            valid: 'glyphicon glyphicon-ok',
		            invalid: 'glyphicon glyphicon-remove',
		            validating: 'glyphicon glyphicon-refresh'
		        },
		        fields: {
		        	name: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The Name field is required'
		                    }
		                }
		        	}	
		        }
		        }).on('success.form.fv', function(e) {
		        	//alert('im here');return false;
		        	var name=$('#name').val();alert(name);
					var contact_person=$('#contact_person').val();
					var address=$('#address').val();
					var phone=$('#phone').val();
					var description=$('#description').val();					
					$.ajax({					
					type:'post',
					data:{'name':name,'contact_person':contact_person,'address':address,'phone':phone,'description':description},
					url:'<?= site_url('settings/ajaxsave')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$(".alert-success").slideDown("slow");
					$(".alert-success").delay(1500);
					$(".alert-success").slideUp("slow");
					//$('.delete_success').hide();
					$('#menufacture_id').val('');
					$('#name').val('');
					$('#contact_person').val('');
					$('#address').val('');
					$('#phone').val('');
					$('#description').val('');
					 
				     }
					
					});
		        	
		        	
		        	});	
				
				$('.delete_success').hide();
				$('.save_success').hide();
				$('.edit_success').hide();
				$('.edit_button').hide();
				$('#ma_name').hide();
				$('body').on('click','.add',function(){
					
					var name=$('#name').val();
					
				
						
					$('#ma_name').hide();
					var contact_person=$('#contact_person').val();
					var address=$('#address').val();
					var phone=$('#phone').val();
					var description=$('#description').val();
					
					$.ajax({
					
					type:'post',
					data:{'name':name,'contact_person':contact_person,'address':address,'phone':phone,'description':description},
					url:'<?= site_url('settings/ajaxsave')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$(".alert-success").slideDown("slow");
					$(".alert-success").delay(1500);
					$(".alert-success").slideUp("slow");
					//$('.delete_success').hide();
					$('#menufacture_id').val('');
					$('#name').val('');
					$('#contact_person').val('');
					$('#address').val('');
					$('#phone').val('');
					$('#description').val('');
					 
				     }
					
					});
					
					
				});
				
				
				
				$('body').on('click','.edit',function(){
					$('.edit_button').show();
					$('.save').hide();
				var menufacture=$(this).data('menufacture');
				  var name=$(this).data('name');
					var contact_person=$(this).data('contact_person');
					var address=$(this).data('address');
					var phone=$(this).data('phone');
					var description=$(this).data('description');
					
					$('#menufacture_id').val(menufacture);
					$('#name').val(name);
					$('#contact_person').val(contact_person);
					$('#address').val(address);
					$('#phone').val(phone);
					$('#description').val(description);
				});
				$('body').on('click','.delete',function(){
				    
				    $('#myModal1').modal('show');
					var menufacture=$(this).data('menufacture');
					$('#menufactureid').val(menufacture);
				});
				
				$('body').on('click','.yes',function(){
					var menufacture=$('#menufactureid').val();
					$.ajax({
					
					type:'post',
					data:{'menufacture_id':menufacture},
					url:'<?= site_url('settings/ajaxdelete')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$(".alert-danger").slideDown("slow");
					$(".alert-danger").delay(1500);
					$(".alert-danger").slideUp("slow");
					 
				     }
				});
					
					
				});
				
				$('body').on('click','.editform',function(){
					
					
					
					var menufacture_id=$('#menufacture_id').val();
					var name=$('#name').val();
					var contact_person=$('#contact_person').val();
					var address=$('#address').val();
					var phone=$('#phone').val();
					var description=$('#description').val();
					
					$.ajax({
					
					type:'post',
					data:{'menufacture_id':menufacture_id,'name':name,'contact_person':contact_person,'address':address,'phone':phone,'description':description},
					url:'<?= site_url('settings/ajaxedit')?>',
					success : function(result){
						$(".alert-default").slideDown("slow");
					$(".alert-default").delay(1000);
					$(".alert-default").slideUp("slow");
					$('#menufacture_id').val('');
					$('#name').val('');
					$('#contact_person').val('');
					$('#address').val('');
					$('#phone').val('');
					$('#description').val('');
					$('.subject_table').html(result);
					
					 $('.edit_button').hide();
					 $('.save').show();
				     }
					
					});
				
					
				});
			});
     </script>