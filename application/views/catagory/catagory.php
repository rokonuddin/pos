<style>
	.alert-success{
		display: none;
	}
	.alert-default{
		display: none;
	}
	.alert-danger{
		display: none;
	}
	
</style>

<div class="col-md-12" style="margin-top: 5%;width: 95%;margin-left: 1%; margin-bottom: 1%;">
			<div class="bs-example">
				    <!-- Modal HTML -->
				   
				    <div id="myModal1" class="modal fade">
				        <div class="modal-dialog">
				            <div class="modal-content" style="width: 70%;">
				            	<div class="modal-header">
				                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                    <h3 class="modal-title title" style="text-align: center;color:green;">Catagory</h3>
				                </div>
				                <div class="modal-header">
				                	<input id="catagoryid" type="hidden" />
				                	<h4 class="modal-title title" style="text-align: center;color:green;">Are You Sure delete this?</h4><br />
				                 <a  style="margin-left: 130px;" class="btn btn-danger yes" data-dismiss="modal">Yes</a>
				                 <a  style="" class="btn btn-success cancel" data-dismiss="modal">Cancel</a>
				                </div>
				                
				         
				            </div>
				        </div>
				    </div>
				</div>
			
			<div class="col-md-12">
				<h3 class="text-center text-success" style="margin-top: -1%;background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;height: 50px;">
						 <b style="margin-top: 50px;">Product Catagory</b> 
					</h3>
			</div><br /><br />
			<div class="alert alert-success save_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been added to the list..
			</div>
			<div class="alert alert-default  edit_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been edit to the list..
			</div>
			<div class="alert alert-danger delete_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been delete to the list..
			</div>
			<div class="col-md-12">	
				<div class="col-md-4" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 35%;">
					<h3 class="text-center text-success">
						ADD Catagory
					</h3>
					<form class="form-horizontal" role="form" id="cataogry_form">
					<input type="hidden" id="catagory_id" />
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
							Catagory Name :
							</label>
							<div class="col-sm-8">
								<input id="catagory_name" name="catagory_name" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Name">
							</div>
						</div>
						
								
							
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Description :
							</label>
							<div class="col-sm-8">
								<textarea id="description" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" ></textarea>
							</div>
						</div>		
														
						<div class="form-group save">								 
							<button  class="btn btn-success" style="float: right; margin-right: 40%;">
								Save
							</button>
						</div>
						<div class="form-group edit_button" >								 
							<a  class="btn btn-success editform" style="float: right; margin-right: 40%;">
								edit
							</a>
						</div>	
					</form>						
				</div>			
				<!--div class="col-md-8" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 63%;margin-left: 15px;">
					<h3 class="text-center text-success">
						Catagory LIST
					</h3>
					<table class="table table-bordered table-condensed subject_table" style="margin-top: 5%;">
						<thead>
						
							<tr>
								<th>
									#
								</th>
								<th>
								Catagory Name
								</th>
								
								<th>
									Description
								</th>
								
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach($catagorys as $catagory):?>
							<tr>
								<td>
									<?= $i++;?>
								</td>
								<td>
									<?=$catagory['catagory_name']?>
								</td>
								
								<td>
								<?=$catagory['description']?>
									
								</td>
								
								<td>
									<a href="#" data-catagory_id="<?php echo $catagory['catagory_id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a>
									<a href="#" data-catagory_id="<?php echo $catagory['catagory_id']?>" data-catagory_name="<?php echo $menufacture['catagory_name']?>" data-description="<?php echo $menufacture['description']?>"  class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
							
						</tbody>
					</table>						
				</div-->
				<div class="col-md-8" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 63%;margin-left: 15px;">
					<h3 class="text-center text-success">
						Product Category LIST
					</h3>
					<table class="table table-bordered table-condensed subject_table" style="margin-top: 5%;">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Name
								</th>
								
								<th>
									Description
								</th>
								
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach($catagorys as $catagory):?>
							<tr>
								<td>
									<?php echo $i++;?>
								</td>
								<td>
									<?= $catagory['catagory_name']?>
								</td>
								
								<td>
								<?= $catagory['description']?>
									
								</td>
								
								<td>
									<a href="#" data-catagory_id="<?php echo $catagory['catagory_id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a>
									<a href="#" data-catagory_id="<?php echo $catagory['catagory_id']?>" data-catagory_name="<?php echo $catagory['catagory_name']?>" data-description="<?php echo $catagory['description']?>"  class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
						</tbody>
					</table>						
				</div>
			</div>	
								
				
			</div>
			<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
 <script src="<?= base_url('dist/js/formValidation.js')?>"></script>
 <script src="<?= base_url('dist/js/framework/bootstrap.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('#datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
				$('.edit_button').hide();
				$('.delete_success').hide();
				$('.save_success').hide();
				$('.edit_success').hide();
				$('#cataogry_form').formValidation({
				message: 'This value is not valid',
		        icon: {
		            valid: 'glyphicon glyphicon-ok',
		            invalid: 'glyphicon glyphicon-remove',
		            validating: 'glyphicon glyphicon-refresh'
		        },
		        fields: {
		        	catagory_name: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The catagory name field is required'
		                    }
		                }
		        	}	
		        }
		       }).on('success.form.fv', function(e) {
		       
		       	var name=$('#catagory_name').val();
					
					
					var description=$('#description').val();
					
					$.ajax({
					
					type:'post',
					data:{'name':name,'description':description},
					url:'<?= site_url('settings/ajaxsaveCatagory')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$('#catagory_id').val('');
					$('#catagory_name').val('');
					 $('#description').val('');
					 $(".alert-success").slideDown("slow");
					$(".alert-success").delay(1500);
					$(".alert-success").slideUp("slow");
				     }
					
					});
				
		       	
		       });
				
				
				
				$('body').on('click','.add',function(){
					
					var name=$('#catagory_name').val();
					
					
					var description=$('#description').val();
					
					$.ajax({
					
					type:'post',
					data:{'name':name,'description':description},
					url:'<?= site_url('settings/ajaxsaveCatagory')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$('#catagory_id').val('');
					$('#catagory_name').val('');
					 $('#description').val('');
				     }
					
					});
				});
				
				
				
				$('body').on('click','.edit',function(){
					$('.edit_button').show();
					$('.save').hide();
					var catagory_id=$(this).data('catagory_id');
				    var catagory_name=$(this).data('catagory_name');
				   
					var description=$(this).data('description');
					$('#catagory_id').val(catagory_id);
					$('#catagory_name').val(catagory_name);
					
					$('#description').val(description);
				});
				
				
				$('body').on('click','.delete',function(){
				    
				    $('#myModal1').modal('show');
					var catagory_id=$(this).data('catagory_id');
					
					$('#catagoryid').val(catagory_id);
				});
				
				$('body').on('click','.yes',function(){
					var catagory_id=$('#catagoryid').val();
					
					$.ajax({
					
					type:'post',
					data:{'catagory_id':catagory_id},
					url:'<?= site_url('settings/ajaxdeleteCatagory')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$(".alert-danger").slideDown("slow");
					$(".alert-danger").delay(1500);
					$(".alert-danger").slideUp("slow");
					 
				     }
				});
					
					
				});
				
				
				$('body').on('click','.editform',function(){
					
					
					
					var catagory_id=$('#catagory_id').val();
					var name=$('#catagory_name').val();
					
					var description=$('#description').val();
					
					$.ajax({
					
					type:'post',
					data:{'catagory_id':catagory_id,'name':name,'description':description},
					url:'<?= site_url('settings/ajaxeditcatagory')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$('#catagory_name').val('');
					$('#catagory_id').val('');
					
					$('#description').val('');
					$(".alert-default").slideDown("slow");
					$(".alert-default").delay(1000);
					$(".alert-default").slideUp("slow");
					 
				     }
					
					});
				
					
				});
			});
     </script>