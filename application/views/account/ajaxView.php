<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Head Name
								</th>
								
								<th>
									Type
								</th>
								
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1; foreach($accounts as $account):?>
							<tr>
								<td>
									<?php echo $i++;?>
								</td>
								<td>
									<?php echo $account['AccountName']?>
								</td>
								
								<td>
									<?php echo $account['AccountsType']?>
								</td>
								
								<td>
									<a href="#" data-account_id="<?php echo $account['account_id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a>
									<a href="#" data-account_id="<?php echo $account['account_id']?>" data-account_name="<?php echo $account['AccountName']?>" data-account_type="<?php echo $account['AccountsType']?>"  class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
						</tbody>