<style>
	.alert-success{
		display: none;
	}
	.alert-default{
		display: none;
	}
	.alert-danger{
		display: none;
	}
	.editform{
		display: none;
	}
</style>

<div class="col-md-12" style="margin-top: 5%;width: 95%;margin-left: 1%; margin-bottom: 1%;">
			
			<div class="bs-example">
				    <!-- Modal HTML -->
				   
				    <div id="myModal1" class="modal fade">
				        <div class="modal-dialog">
				            <div class="modal-content" style="width: 70%;">
				            	<div class="modal-header">
				                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                    <h3 class="modal-title title" style="text-align: center;color:green;">Account</h3>
				                </div>
				                <div class="modal-header">
				                	<input id="account_id1" type="hidden" />
				                	<h4 class="modal-title title" style="text-align: center;color:green;">Are You Sure delete this?</h4><br />
				                 <a  style="margin-left: 130px;" class="btn btn-danger yes" data-dismiss="modal">Yes</a>
				                 <a  style="" class="btn btn-success cancel" data-dismiss="modal">Cancel</a>
				                </div>
				                
				         
				            </div>
				        </div>
				    </div>
				</div>
			
			<div class="col-md-12">
				<h3 class="text-center text-success" style="margin-top: -1%;background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;height: 50px;">
						 <b style="margin-top: 50px;">Product Catagory</b> 
					</h3>
			</div><br /><br />
			<div class="alert alert-success save_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been added to the list..
			</div>
			<div class="alert alert-default  edit_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been edit to the list..
			</div>
			<div class="alert alert-danger delete_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been delete to the list..
			</div>
			
			
			
			<div class="col-md-12">
				<h3 class="text-center text-success" style="margin-top: -1%;background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;height: 50px;">
						 <b style="margin-top: 50px;">Accounts Heads</b> 
					</h3>
			</div>
			<div class="col-md-12">	
				<div class="col-md-4" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 35%;">
					<h3 class="text-center text-success">
						Add Accounts Heads
					</h3>
					<form class="form-horizontal" role="form" id="account_form">
						<input type="hidden" id="account_id" value="0" />
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
                                Head Name :
							</label>
							<div class="col-sm-8">
								<input id="head_name" name="head_name" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control" id="inputEmail3" type="text" placeholder="Head Name">
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Accounts Type:
							</label>
							<div class="col-sm-8">
                                <select class="form-control" name="account_type" id="account_type">
                                	<option >--Select--</option>
                                    <option value="Expense">Expense</option>
                                    <option value="Income">Income</option>
                                    <option value="SuperCash">Super Cash</option>
                                    <option value="SuperBank">Super Bank</option>
                                </select>
								
							</div>
						</div>
								
														
						<div class="form-group save">								 
							<button  class="btn btn-success" style="float: right; margin-right: 40%;">
								Save
							</button>
						</div>
						<div class="form-group edit_button" >								 
							<button  class="btn btn-success" style="float: right; margin-right: 40%;">
								edit
							</button>
						</div>	
					</form>						
				</div>			
				<div class="col-md-8" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 63%;margin-left: 15px;">
					<h3 class="text-center text-success">
						Accounts Heads List
					</h3>
					<table class="table table-bordered table-condensed subject_table" style="margin-top: 5%;">
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									Head Name
								</th>
								
								<th>
									Type
								</th>
								
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $i=1; foreach($accounts as $account):?>
							<tr>
								<td>
									<?php echo $i++;?>
								</td>
								<td>
									<?php echo $account['AccountName']?>
								</td>
								
								<td>
									<?php echo $account['AccountsType']?>
								</td>
								
								<td>
									<a href="#" data-account_id="<?php echo $account['account_id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a>
									<a href="#" data-account_id="<?php echo $account['account_id']?>" data-account_name="<?php echo $account['AccountName']?>" data-account_type="<?php echo $account['AccountsType']?>"  class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
                            
						</tbody>
					</table>						
				</div>
			</div>	
								
				
			</div>	
			


<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
 <script src="<?= base_url('dist/js/formValidation.js')?>"></script>
 <script src="<?= base_url('dist/js/framework/bootstrap.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('#datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
				$('.edit_button').hide();
				$('.delete_success').hide();
				$('.save_success').hide();
				$('.edit_success').hide();
				$('#account_form').formValidation({
				message: 'This value is not valid',
		        icon: {
		            valid: 'glyphicon glyphicon-ok',
		            invalid: 'glyphicon glyphicon-remove',
		            validating: 'glyphicon glyphicon-refresh'
		        },
		        fields: {
		        	head_name: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The account name field is required'
		                    }
		                }
		        	}	
		        }
		       }).on('success.form.fv', function(e) {
		       
		       	var name=$('#head_name').val();
					
					
					var account_type=$('#account_type').val();
					var account_id=$('#account_id').val();
					$.ajax({
					
					type:'post',
					data:{'account_id':account_id,'name':name,'account_type':account_type},
					url:'<?= site_url('account/ajaxsaveAccount')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$('#catagory_id').val('');
					$('#catagory_name').val('');
					 $('#description').val('');
					 $(".alert-success").slideDown("slow");
					$(".alert-success").delay(1500);
					$(".alert-success").slideUp("slow");
				     }
					
					});
				
		       	
		       });
				
				
				
				$('body').on('click','.add',function(){
					
					var name=$('#catagory_name').val();
					
					
					var description=$('#description').val();
					
					$.ajax({
					
					type:'post',
					data:{'name':name,'description':description},
					url:'<?= site_url('settings/ajaxsaveCatagory')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$('#catagory_id').val('');
					$('#catagory_name').val('');
					 $('#description').val('');
				     }
					
					});
				});
				
				
				
				$('body').on('click','.edit',function(){
					$('.edit_button').show();
					$('.save').hide();
					var account_id=$(this).data('account_id');
				    var name=$(this).data('account_name');
				   
					var account_type=$(this).data('account_type');
					$('#account_id').val(account_id);
					$('#head_name').val(name);
					
					$('#account_type').val(account_type);
				});
				
				
				$('body').on('click','.delete',function(){
				    
				    $('#myModal1').modal('show');
					var account_id=$(this).data('account_id');
					
					$('#account_id1').val(account_id);
				});
				
				$('body').on('click','.yes',function(){
					var account_id=$('#account_id1').val();
					
					$.ajax({
					
					type:'post',
					data:{'account_id':account_id},
					url:'<?= site_url('account/ajaxdeleteAccount')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$(".alert-danger").slideDown("slow");
					$(".alert-danger").delay(1500);
					$(".alert-danger").slideUp("slow");
					 
				     }
				});
					
					
				});
				
				
				
			});
     </script>