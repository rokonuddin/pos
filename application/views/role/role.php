<style>
	.alert-success{
		display: none;
	}
	.alert-default{
		display: none;
	}
	.alert-danger{
		display: none;
	}
	
</style>

<div class="col-md-12" style="margin-top: 5%;width: 95%;margin-left: 1%; margin-bottom: 1%;">
			<div class="bs-example">
				    <!-- Modal HTML -->
				   
				    <div id="myModal1" class="modal fade">
				        <div class="modal-dialog">
				            <div class="modal-content" style="width: 70%;">
				            	<div class="modal-header">
				                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                    <h3 class="modal-title title" style="text-align: center;color:green;">Product Option</h3>
				                </div>
				                <div class="modal-header">
				                	<input id="id1" type="hidden" />
				                	<h4 class="modal-title title" style="text-align: center;color:green;">Are You Sure delete this?</h4><br />
				                 <a  style="margin-left: 130px;" class="btn btn-danger yes" data-dismiss="modal">Yes</a>
				                 <a  style="" class="btn btn-success cancel" data-dismiss="modal">Cancel</a>
				                </div>
				                
				         
				            </div>
				        </div>
				    </div>
				</div>
			
			<div class="col-md-12">
				<h3 class="text-center text-success" style="margin-top: -1%;background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;height: 50px;">
						 <b style="margin-top: 50px;">Role</b> 
					</h3>
			</div><br /><br />
			<div class="alert alert-success save_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been added to the list..
			</div>
			<div class="alert alert-default  edit_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been edit to the list..
			</div>
			<div class="alert alert-danger delete_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been delete to the list..
			</div>
			<div class="col-md-12">	
				<div class="col-md-6" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 40%;">
					<h3 class="text-center text-success">
						ADD Role
					</h3>
					<form class="form-horizontal" role="form" id="role_form">
						<input id="id" type="hidden" />
					
						
						
						
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Role :
							</label>
							<div class="col-sm-8">
								<input id="role" name="role" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control"  type="text" placeholder="role">
							</div>
						</div>		
						
						
						<div class="form-group save">								 
							<button  class="btn btn-success" style="float: right; margin-right: 40%;">
								Save
							</button>
						</div>
						<div class="form-group edit_button" >								 
							<button  class="btn btn-success" style="float: right; margin-right: 40%;">
								edit
							</button>
						</div>	
					</form>						
				</div>			
				<div class="col-md-6" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 58%;margin-left: 15px;">
					<h3 class="text-center text-success">
						Role List
					</h3>
					<table class="table table-bordered table-condensed subject_table" style="margin-top: 5%;">
						<thead>
							<tr>
								
								<th>
									Role
								</th>
								
								
								
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($roles as $role):?>
							<tr>
								<td>
									<?= $role['name']?>
								</td>
								<td>
									<a href="#" data-id="<?php echo $role['id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a>
									<a href="#" data-id="<?php echo $role['id']?>"  data-name="<?php echo $role['name']?>"    class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
							
						</tbody>
					</table>						
				</div>
			</div>	
								
				
			</div>	
			<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
<script src="<?= base_url('dist/js/formValidation.js')?>"></script>
 <script src="<?= base_url('dist/js/framework/bootstrap.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>
<script type="text/javascript">
            $(function () {
                $('#expire_date').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('#datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
				$('.edit_button').hide();
				$('.delete_success').hide();
				$('.save_success').hide();
				$('.edit_success').hide();
				$('#role_form').formValidation({
				message: 'This value is not valid',
		        icon: {
		            valid: 'glyphicon glyphicon-ok',
		            invalid: 'glyphicon glyphicon-remove',
		            validating: 'glyphicon glyphicon-refresh'
		        },
		        fields: {
		        	role: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The Product filed  is required'
		                    }
		                }
		        	},
		        	
		        }
		        }).on('success.form.fv', function(e) {
		        	
		        	
		        	var role=$('#role').val();
					var id=$('#id').val();
					
					
					//alert(manufacture_id);
					$.ajax({
					
					type:'post',
					data:{'id':id,'role':role},
					url:'<?= site_url('settings/ajaxsaveRole')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					
					$(".alert-success").slideDown("slow");
					$(".alert-success").delay(1500);
					$(".alert-success").slideUp("slow");
					 $('#id').val('');
					 $('#id1').val('');
					$('#role').val('');
					
				     }
					
					});
		        	
		        	
		        });
				
				
				
				
				
				$('body').on('click','.edit',function(){
					$('.edit_button').show();
					$('.save').hide();
					
					
					
					var id=$(this).data('id');
					var name=$(this).data('name');
					
					
					
					$('#id').val(id);
					
					$('#role').val(name);
					
					
				
				});
					
				
				
				$('body').on('click','.delete',function(){
				    
				    $('#myModal1').modal('show');
					var id=$(this).data('id');
					
					$('#id1').val(id);
				});
				
				$('body').on('click','.yes',function(){
					var id=$('#id1').val();
					
					$.ajax({
					
					type:'post',
					data:{'id':id},
					url:'<?= site_url('settings/ajaxDeleteRole')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$(".alert-danger").slideDown("slow");
					$(".alert-danger").delay(1500);
					$(".alert-danger").slideUp("slow");
					 
				     }
				});
					
					
				});
				
				
				
				
				
			});
     </script>