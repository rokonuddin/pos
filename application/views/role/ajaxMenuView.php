<thead>
							<tr>
								
								<th>
									Memu
								</th>
								
								<th>
									Order
								</th>
								<th>
									Url
								</th>
								
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($menus as $menu):?>
							<tr>
								<td>
									<?= $menu['menu_name']?>
								</td>
								<td>
									<?= $menu['menu_order']?>
								</td>
								<td>
									<?= $menu['menu_url']?>
								</td>
								<td>
									<a href="#" data-id="<?php echo $menu['menu_id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a>
									<a href="#" data-id="<?php echo $menu['menu_id']?>"  data-menu_name="<?php echo $menu['menu_name']?>" data-menu_url="<?php echo $menu['menu_url']?>"  data-menu_order="<?php echo $menu['menu_order']?>"  class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
							
						</tbody>