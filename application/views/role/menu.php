<style>
	.alert-success{
		display: none;
	}
	.alert-default{
		display: none;
	}
	.alert-danger{
		display: none;
	}
	
</style>

<div class="col-md-12" style="margin-top: 5%;width: 95%;margin-left: 1%; margin-bottom: 1%;">
			<div class="bs-example">
				    <!-- Modal HTML -->
				   
				    <div id="myModal1" class="modal fade">
				        <div class="modal-dialog">
				            <div class="modal-content" style="width: 70%;">
				            	<div class="modal-header">
				                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                    <h3 class="modal-title title" style="text-align: center;color:green;">Product Option</h3>
				                </div>
				                <div class="modal-header">
				                	<input id="id1" type="hidden" />
				                	<h4 class="modal-title title" style="text-align: center;color:green;">Are You Sure delete this?</h4><br />
				                 <a  style="margin-left: 130px;" class="btn btn-danger yes" data-dismiss="modal">Yes</a>
				                 <a  style="" class="btn btn-success cancel" data-dismiss="modal">Cancel</a>
				                </div>
				                
				         
				            </div>
				        </div>
				    </div>
				</div>
			
			<div class="col-md-12">
				<h3 class="text-center text-success" style="margin-top: -1%;background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 99%;height: 50px;">
						 <b style="margin-top: 50px;">Menu</b> 
					</h3>
			</div><br /><br />
			<div class="alert alert-success save_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been added to the list..
			</div>
			<div class="alert alert-default  edit_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been edit to the list..
			</div>
			<div class="alert alert-danger delete_success" id="success-alert" style="width: 100%;border-radius: 1em">
		    		<button type="button" class="close">x</button>
		    		<strong>Success! </strong>
		    		 Informations has been delete to the list..
			</div>
			<div class="col-md-12">	
				<div class="col-md-6" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em;width: 40%;">
					<h3 class="text-center text-success">
						ADD Menu
					</h3>
					<form class="form-horizontal" role="form" id="role_form">
						<input id="menu_id_edit" type="hidden" />
					
						
						
						
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Menu Name :
							</label>
							<div class="col-sm-8">
								<input id="menu_name" name="menu_name" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control"  type="text" placeholder="role">
							</div>
						</div>	
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Parent:
							</label>
							<div class="col-sm-8">
                                <select class="form-control" name="parent_id" id="parent_id">
                                	<option value="0">--Select--</option>
                                	<?php foreach($menu1s as $menu1):?>
                                    <option value="<?=$menu1['menu_id']?>"><?=$menu1['menu_name']?></option>
                                    <?php endforeach;?>
                                    
                                </select>
								
							</div>
						</div>	
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Menu Order :
							</label>
							<div class="col-sm-8">
								<input id="menu_order" name="menu_order" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control"  type="text" placeholder="role">
							</div>
						</div>	
						<div class="form-group" style="margin-top: 15px;">							 
							<label for="inputEmail3" class="col-sm-4 control-label">
								Url :
							</label>
							<div class="col-sm-8">
								<input id="menu_url" name="menu_url" style="background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" class="form-control"  type="text" placeholder="role">
							</div>
						</div>
						<div class="form-group save">								 
							<button  class="btn btn-success" style="float: right; margin-right: 40%;">
								Save
							</button>
						</div>
						<div class="form-group edit_button" >								 
							<button  class="btn btn-success" style="float: right; margin-right: 40%;">
								edit
							</button>
						</div>	
					</form>						
				</div>			
				<div class="col-md-6" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 58%;margin-left: 15px;">
					<h3 class="text-center text-success">
						Role List
					</h3>
					<table class="table table-bordered table-condensed subject_table" style="margin-top: 5%;">
						<thead>
							<tr>
								
								<th>
									Memu
								</th>
								
								<th>
									Order
								</th>
								<th>
									Url
								</th>
								
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($menus as $menu):?>
							<tr>
								<td>
									<?= $menu['menu_name']?>
								</td>
								<td>
									<?= $menu['menu_order']?>
								</td>
								
								<td>
									<?= $menu['menu_url']?>
								</td>
								<td>
									<a href="#" data-menu_id="<?php echo $menu['menu_id']?>" class="btn btn-link active delete">
									<span class="glyphicon glyphicon-remove"></span>
									</a>
									<a href="#" data-menu_id="<?php echo $menu['menu_id']?>"  data-menu_name="<?php echo $menu['menu_name']?>" data-menu_url="<?php echo $menu['menu_url']?>" data-parent_id="<?php echo $menu['parent_id']?>"  data-menu_order="<?php echo $menu['menu_order']?>"  class="btn btn-link active edit">
									<span class="glyphicon glyphicon-pencil"></span>
									</a>
								</td>
							</tr>
							<?php endforeach;?>
							
						</tbody>
					</table>						
				</div>
			</div>	
								
				
			</div>	
			<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
<script src="<?= base_url('dist/js/formValidation.js')?>"></script>
 <script src="<?= base_url('dist/js/framework/bootstrap.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>
<script type="text/javascript">
            $(function () {
                $('#expire_date').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('#datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
				$('.edit_button').hide();
				$('.delete_success').hide();
				$('.save_success').hide();
				$('.edit_success').hide();
				$('#role_form').formValidation({
				message: 'This value is not valid',
		        icon: {
		            valid: 'glyphicon glyphicon-ok',
		            invalid: 'glyphicon glyphicon-remove',
		            validating: 'glyphicon glyphicon-refresh'
		        },
		        fields: {
		        	menu_name: {
		        		validators: {
		                    notEmpty: {
		                        message: 'The Product filed  is required'
		                    }
		                }
		        	},
		        	
		        }
		        }).on('success.form.fv', function(e) {
		        	
		        	
		        	var menu_name=$('#menu_name').val();
					var parent_id=$('#parent_id').val();
					var menu_id_edit=$('#menu_id_edit').val();
				    var menu_order=$('#menu_order').val();
					var menu_url=$('#menu_url').val();
					var menu_id_edit=$('#menu_id_edit').val();
					//alert(manufacture_id);
					$.ajax({
					
					type:'post',
					data:{'menu_id_edit':menu_id_edit,'menu_name':menu_name,'parent_id':parent_id,'menu_order':menu_order,'menu_url':menu_url},
					url:'<?= site_url('settings/ajaxsaveMenu')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					
					$(".alert-success").slideDown("slow");
					$(".alert-success").delay(1500);
					$(".alert-success").slideUp("slow");
					 $('#menu_name').val('');
					 $('#menu_id').val('');
					$('#menu_order').val('');
					$('#menu_url').val('');
					$('#parent_id').val('');
				     }
					
					});
		        	
		        	
		        });
				
				
				
				
				
				$('body').on('click','.edit',function(){
					$('.edit_button').show();
					$('.save').hide();
					
					
					
					var menu_id=$(this).data('menu_id');
					var menu_name=$(this).data('menu_name');
					var menu_order=$(this).data('menu_order');
					var menu_url=$(this).data('menu_url');
					var parent_id=$(this).data('parent_id');
					
					
					$('#menu_name').val(menu_name);
					$('#parent_id').val(parent_id);
					$('#menu_id_edit').val(menu_id);
				    $('#menu_order').val(menu_order);
					$('#menu_url').val(menu_url);
					
					
					
					
					
				
				});
					
				
				
				$('body').on('click','.delete',function(){
				    
				    $('#myModal1').modal('show');
					var menu_id=$(this).data('menu_id');
					
					$('#id1').val(menu_id);
				});
				
				$('body').on('click','.yes',function(){
					var menu_id=$('#id1').val();
					
					$.ajax({
					
					type:'post',
					data:{'menu_id':menu_id},
					url:'<?= site_url('settings/ajaxDeleteMenu')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$(".alert-danger").slideDown("slow");
					$(".alert-danger").delay(1500);
					$(".alert-danger").slideUp("slow");
					 
				     }
				});
					
					
				});
				
				
				
				
				
			});
     </script>