<style>
	.alert-success{
		display: none;
	}
	.alert-default{
		display: none;
	}
	.alert-danger{
		display: none;
	}
	
</style>
<?php 
      $result[][]='';
        foreach($assignLists as $value)
		{
				$result[$value['role_id']][$value['menu_id']]=1;						   	 
											   	  
		 }									 

?>
<div class="col-md-12" style="margin-top: 5%;width: 95%;margin-left: 1%; margin-bottom: 1%;">
			
			
			
			<div class="col-md-12">	
							
				<div class="col-md-12" style="background-color: #F9F9F9;border: 1px solid #E7E7E7;border-radius: 1em; width: 98%;margin-left: 15px;">
					<h3 class="text-center text-success">
						Role Assign List
					</h3>
					<table class="table table-bordered table-condensed subject_table" style="margin-top: 5%;">
						<thead>
							<tr>
								
								<th style="text-align: center;">
									Select
								</th>
								
								
								<?php foreach($roles as $role):?>
								<th style="text-align: center;">
									<?= $role['name']?>
								</th>
								<?php endforeach;?>
							</tr>
						</thead>
						<tbody>
							<?php $i=1; foreach($menu1s as $menu):?>
							<tr>
								<td style="text-align: left;">
									<b style="margin-left: 20px;"><?= $menu['menu_name']?></b>
								</td>
								<?php foreach($roles as $role):
									
									$menu_id=$menu['menu_id'];
									$role_id=$role['id'];
									?>
									
								<td style="text-align: center;">
									<input <?php  echo (isset($result[$role_id][$menu_id]))?($result[$role_id][$menu_id]=='1')?'checked="checked"':'':'';?> data-role_id="<?= $role_id?>"  data-menu_id="<?= $menu_id?>" type="checkbox" class="assign_role <?= $role_id.''.$menu_id?>" id="<?= $role_id.''.$menu_id?>" />
								</td>
								<?php endforeach;?>
							</tr>
							<?php $childs=$this->settings_model->getChild($menu['menu_id']);
							if(!empty($childs)){
								
								foreach($childs as $child){
							?>
							
							<tr>
								<td style="text-align: center;">
									<?= $child['menu_name']?>
								</td>
								<?php foreach($roles as $role):
								  $menu_id=$child['menu_id'];
									$role_id=$role['id'];
									
									?>
									
								<td style="text-align: center;">
								<input <?php  echo (isset($result[$role_id][$menu_id]))?($result[$role_id][$menu_id]=='1')?'checked="checked"':'':'';?> data-role_id="<?= $role_id?>"  data-menu_id="<?= $menu_id?>" class="assign_role <?= $role_id.''.$menu_id?>" type="checkbox" id="<?= $role_id.''.$menu_id?>"/>
								</td>
								<?php endforeach;?>
							</tr>
							
							<?php } }?>
							
							<?php  endforeach;?>
							
						</tbody-->
					</table>						
				</div>
			</div>	
								
				
			</div>	
			<script src="<?= base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/js/Moment.js')?>"></script>
<script src="<?= base_url('dist/js/formValidation.js')?>"></script>
 <script src="<?= base_url('dist/js/framework/bootstrap.js')?>"></script>
 <script src="<?= base_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>
<script src="<?= base_url('assets/js/scripts.js');?>"></script>
<script type="text/javascript">
            $(function () {
                $('#expire_date').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
                $('#datetimepicker2').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
			$(document).ready(function(){
				$('.edit_button').hide();
				$('.delete_success').hide();
				$('.save_success').hide();
				$('.edit_success').hide();
				
				
				
				
				
				
				$('body').on('click','.assign_role',function(){
					
					
					var menu_id=$(this).data('menu_id');
					var role_id=$(this).data('role_id');
					
					var vlaue="" +role_id +menu_id;
					
					///var check=$('#'+vlaue).val();
					//;
					if ($('#'+vlaue).is(":checked"))
					{
					  var check=1;
					}else{
						var check=0;
					}
					
					$.ajax({
					
					type:'post',
					data:{'menu_id':menu_id,'role_id':role_id,'check':check},
					url:'<?= site_url('settings/ajaxSaveRoleAssign')?>',
					success : function(result){
					
					
					 
				     }
				});
					
				
				});
					
				
				
				$('body').on('click','.delete',function(){
				    
				    $('#myModal1').modal('show');
					var id=$(this).data('id');
					
					$('#id1').val(id);
				});
				
				$('body').on('click','.yes',function(){
					var id=$('#id1').val();
					
					$.ajax({
					
					type:'post',
					data:{'id':id},
					url:'<?= site_url('settings/ajaxDeleteRole')?>',
					success : function(result){
					
					$('.subject_table').html(result);
					$(".alert-danger").slideDown("slow");
					$(".alert-danger").delay(1500);
					$(".alert-danger").slideUp("slow");
					 
				     }
				});
					
					
				});
				
				
				
				
				
			});
     </script>